# playground3

![screenshot](https://i.imgur.com/7rQwX69.png)

## What the...

This is the 3rd installment of "dump everything I've tried so far" playground.

It is dedicated to checking out the Vulkan API that promises to liberate us from legacy
learning materials and left-handed coordinate systems.

## Prerequisites

* Install Vulkan SDK from LunarG: https://vulkan.lunarg.com/sdk/home

## What's inside

The project is semi-organized in the following major subtrees:

* `App` - getting from launch to a window.
* `Game` - this is the part that you should look at then adapt to your will.
* `Geometry` - staging ground for things not yet made into the [geomancy](https://gitlab.com/dpwiz/geomancy) package.
* `Render` - mini-framework and example rendering pipelines. Nothing gets drawn without one of those.
* `Resource` - ready to use file formats.
* `Vulkan` - bits and pieces to set up Vulkan basics to `Render` with.

## Execute

* Run `stack exec -- playground3-exe` to see "We're inside the application!"
* With `stack exec -- playground3-exe --verbose` you will see the same message, with more logging.

## Run tests

`stack test`

## Running with nix

Switch nix.enable to true in stack.yaml.
Then run

```bash
nix-shell -p cachix --run "cachix use all-hies"
```

to add nix cache with HIE (this should be ran once). Then run

```bash
nix-shell
stack run --nix
```

## Running in Windows (broken until GHC 8.10.2)

Have GHC-8.6.5 installed:

    stack setup --stack-yaml stack-windows.yaml

Update msys2 and install SDL devel dependencies:

    stack exec -- pacman -Syu
    stack exec -- pacman -S mingw-w64-x86_64-pkg-config mingw-w64-x86_64-SDL2 mingw-w64-x86_64-SDL2_image

Build:

    stack --stack-yaml stack-windows.yaml build

Run:

    stack --stack-yaml stack-windows.yaml run

> Included VS Code task is set up to run with `stack-windows.yaml`.

## Texture compression

Many (>98%) desktop GPUs support `textureCompressionBC`.
However the conversion and usage is still awkward.
I found [basisu](https://github.com/BinomialLLC/basis_universal/releases) tool producing good enough assets in reasonable time.

Alpha is botched in the process and unusable sprites and e.g. bitmap fonts.
Large photographic textures are okay.

Compression is a two-stage procedure:

1. Generate Basis Unioversal from PNG image.
2. Transcode into KTX-wrapped BC7 compressed texture.
3. Delete extra PNG and KTX you aren't gonna need.

The spell for good quality is:

```bash
find . -name '*.png' | xargs -n1 basisu -mipmap -uastc -uastc_level 3
```

For cubemaps:

```bash
basisu -tex_type cubemap -uastc -uastc_level 3 right.png left.png top.png bottom.png front.png back.png
```
