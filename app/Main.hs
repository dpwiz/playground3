{-# LANGUAGE TemplateHaskell #-}

module Main (main) where

import Import

import Data.Acquire (withAcquire)
import Options.Applicative.Simple
import RIO.Orphans (withResourceMap)

import qualified Prelude

import App.Setup (setup)
import Run (run)

import qualified Paths_playground3

main :: IO ()
main = do
  (options, ()) <- simpleOptions
    $(simpleVersion Paths_playground3.version)
    "Header for command line arguments"
    "Program description, also for command line arguments"
    parseOptions
    empty

  withResourceMap \resourceMap ->
    withAcquire (setup options resourceMap) \app ->
      runRIO app run

parseOptions :: Parser Options
parseOptions = do
  optionsVerbose <- switch $ mconcat
    [ long "verbose"
    ]

  optionsFullscreen <- switch $ mconcat
    [ long "fullscreen"
    ]

  optionsWindowSize <- optional . option readSize $ mconcat
    [ long "window-size"
    ]

  optionsValidation <- switch $ mconcat
    [ long "validation"
    , help "Enable LunarG validation layers."
    ]

  optionsRenderdoc <- switch $ mconcat
    [ long "renderdoc"
    , help "Enable RenderDoc capture layer."
    ]

  optionsFrameGC <- fmap not . switch $ mconcat
    [ long "no-frame-gc"
    , help "Disable forced GC after each frame"
    ]

  optionsFrameDelay <- fmap not . switch $ mconcat
    [ long "no-frame-delay"
    , help "Disable FPS limit"
    ]

  pure Options{..}

readSize :: ReadM (V2 Int)
readSize = eitherReader \val ->
  case break (flip elem ['x', '-', ':']) val of
    (w, h) ->
      V2 <$> semiAuto w <*> semiAuto (drop 1 h)
  where
    semiAuto arg =
      case Prelude.reads arg of
        [(r, "")] -> pure r
        _         -> Left $ "cannot parse value `" ++ arg ++ "'"
