-- | JSON font loader for bitmaps and SDFs
--
-- Generator: https://evanw.github.io/font-texture-generator/
-- Usage (WebGL): https://evanw.github.io/font-texture-generator/example-webgl/

module Resource.Font.Evanw where

import RIO

import Data.Aeson (FromJSON, eitherDecodeFileStrict')
import Geomancy.Transform (Transform)
import Linear (V2(..), V4(..))
import Vulkan.NamedType ((:::))

import qualified Geomancy.Mat4 as Mat4
import qualified Geomancy.Transform as Transform
import qualified RIO.HashMap as HashMap
import qualified RIO.Text as Text

import Geometry.Types (Origin(..), Bitmap(..))

data FontError = FontError Text
  deriving (Eq, Ord, Show, Generic)

instance Exception FontError

data Container = Container
  { name       :: Text
  , size       :: Float
  , bold       :: Bool
  , italic     :: Bool
  , width      :: Float
  , height     :: Float
  , characters :: HashMap Char Character
  }
  deriving (Eq, Ord, Show, Generic)

data Character = Character
  { x       :: Float
  , y       :: Float
  , width   :: Float
  , height  :: Float
  , originX :: Float
  , originY :: Float
  , advance :: Float
  }
  deriving (Eq, Ord, Show, Generic)

instance FromJSON Container

instance FromJSON Character

load :: FilePath -> IO Container
load fp = eitherDecodeFileStrict' fp >>= \case
  Left err ->
    throwIO $ FontError $ Text.pack err
  Right res ->
    pure res

-- | Compose a string of screen/texture transforms
putChars
  :: "XYWH"      ::: V4 Float
  -> "Alignment" ::: (Origin, Origin)
  -> "Size"      ::: Float
  -> "Font"      ::: Container
  -> "Atlas"     ::: Bitmap
  -> "Line"      ::: [Char]
  -> [(Transform, Bitmap)]
putChars (V4 cw ch cx cy) (halign, valign) targetSize font fontTexture =
  extract . foldl' f (V2 0 0, [])
  where
    Container
      { size   = fontSize
      , width  = atlasWidth
      , height = atlasHeight
      , characters
      } = font

    sizeScale = targetSize / fontSize

    extract (V2 offX _offY, bits) = do
      (transform, texture) <- bits
      let
        ax = case halign of
          Begin  -> -cw / 2
          Middle -> - offX * sizeScale / 2
          End    -> cw / 2 - offX * sizeScale

        ay = case valign of
          Begin  -> ch / 2 - targetSize
          Middle -> -targetSize / 2
          End    -> -ch / 2

      pure
        ( transform
            <> Transform.scaleXY sizeScale sizeScale
            <> Transform.translate (cx + ax) (cy + ay) 0
        , texture
        )

    f (V2 offX offY, acc) ' ' =
      ( V2 (offX + fontSize / 2) offY
      , acc
      )

    f (V2 offX offY, acc) char =
      case HashMap.lookup char characters <|> HashMap.lookup '?' characters of
        Nothing ->
          (V2 offX offY, acc)
        Just Character{..} ->
          ( V2 (offX + advance) offY
          , (transform, texture) : acc
          )
          where
            transform = Mat4.rowMajor
              width 0      0 0
              0     height 0 0
              0     0      1 0
              ox    oy     0 1

            ox = offX + width / 2 - originX
            oy = offY - height / 2 + originY

            texture = fontTexture
              { _tOffset = V2 (x / atlasWidth) (y / atlasHeight)
              , _tScale  = V2 (width / atlasWidth) (height / atlasHeight)
              }
