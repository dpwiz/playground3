-- | MSDF font loader
--
-- Source: https://github.com/Itorius/Proxima/tree/master/Proxima/Assets/Fonts

module Resource.Font.Proxima
  ( load
  , Container(..)
  , Vertex
  , toVertices
  , indices
  , toGlyphs
  ) where

import RIO

import Data.Aeson (FromJSON(..), eitherDecodeFileStrict', withObject, (.:))
import Linear (V4(..))

import qualified RIO.HashMap as HashMap
import qualified RIO.Text as Text

newtype FontError = FontError Text
  deriving (Eq, Ord, Show)

instance Exception FontError

data Container = Container
  { characters :: HashMap Char (Float, [Vertex])
  }

type Vertex = V4 Float

data Entry = Entry
  { character :: Char
  , uvs       :: XYWH
  , advanceX  :: Float
  , bearing   :: XY
  , size      :: XY
  }
  deriving (Eq, Ord, Show)

instance FromJSON Entry where
  parseJSON = withObject "Entry" \o -> do
    character <- o .: "Character"
    uvs       <- o .: "UVs"
    advanceX  <- o .: "AdvanceX"
    bearing   <- o .: "Bearing"
    size      <- o .: "Size"
    pure Entry{..}

data XY = XY Float Float
  deriving (Eq, Ord, Show)

instance FromJSON XY where
  parseJSON = withObject "XY" \o -> XY
    <$> o .: "X"
    <*> o .: "Y"

data XYWH = XYWH Float Float Float Float
  deriving (Eq, Ord, Show)

instance FromJSON XYWH where
  parseJSON = withObject "XYWH" \o -> XYWH
    <$> o .: "X"
    <*> o .: "Y"
    <*> o .: "Width"
    <*> o .: "Height"

load :: FilePath -> IO Container
load fp = eitherDecodeFileStrict' fp >>= \case
  Left err ->
    throwIO $ FontError $ Text.pack err
  Right res ->
    pure Container
      { characters =
          HashMap.fromList do
            ch <- res
            pure
              ( character ch
              , ( advanceX ch
                , toVertices ch
                )
              )
      }

toGlyphs :: Maybe Char -> Container -> Text -> [Either Float (Float, [Vertex])]
toGlyphs fallback Container{characters} = Text.foldr f []
  where
    f ch acc =
      case ch of
        ' ' ->
          Left spaceAdvance : acc
        '\t' ->
          Left (spaceAdvance * 8) : acc
        '\n' ->
          Left spaceAdvance : acc -- TODO: line breaks
        _ ->
          case HashMap.lookup ch characters <|> unknownGlyph of
            Nothing ->
              acc
            Just character ->
              Right character : acc

    unknownGlyph = do
      ch <- fallback
      HashMap.lookup ch characters

    spaceMeasure = '-'

    spaceAdvance =
      case HashMap.lookup spaceMeasure characters of
        Nothing ->
          error $ unwords
            [ "assert:"
            , show spaceMeasure
            , "is present in font"
            ]
        Just (advance, _indexed) ->
          advance

toVertices :: Entry -> [Vertex]
toVertices Entry{..} =
  [ V4 bearingX           (-bearingY - sizeY) uvX0 uvY0
  , V4 (bearingX + sizeX) (-bearingY - sizeY) uvX1 uvY0
  , V4 (bearingX + sizeX) (-bearingY)         uvX1 uvY1
  , V4 bearingX           (-bearingY)         uvX0 uvY1
  ]

  where
    XYWH uvX0 uvY1 uvX1 uvY0 = uvs
    XY bearingX bearingY     = bearing
    XY sizeX sizeY           = size

indices :: [Word32]
indices =
  [ 0, 1, 2
  , 2, 3, 0
  ]
