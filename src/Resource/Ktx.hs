module Resource.Ktx where

import Import

import Data.Acquire (Acquire, mkAcquire)
import Data.ByteString.Unsafe (unsafeUseAsCStringLen)

import qualified Codec.Ktx as Ktx1
import qualified Foreign
import qualified RIO.Text as Text
import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector (last)
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Utils.FromGL as FromGL
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Buffer.Command (oneshot)

import qualified Vulkan.Buffer.Image as Image

-- XXX: move to codec
data LoadError = LoadError Int64 Text.Text
  deriving (Eq, Ord, Show, Generic)

instance Exception LoadError

acquireImageView
  :: VulkanDevice
  -> Vk.Image
  -> Vk.Format
  -> "mipLevels" ::: Word32
  -> "faces" ::: Word32
  -> Acquire Vk.ImageView
acquireImageView VulkanDevice{..} image format mipLevels faces =
  Vk.withImageView _vdLogical imageViewCI Nothing mkAcquire
  where
    viewType =
      if faces == 6 then
        Vk.IMAGE_VIEW_TYPE_CUBE
      else
        Vk.IMAGE_VIEW_TYPE_2D

    imageViewCI = zero
      { Vk.image            = image
      , Vk.viewType         = viewType
      , Vk.format           = format
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = Vk.ImageSubresourceRange
      { aspectMask  = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = 0
      , levelCount     = mipLevels -- XXX: including base
      , baseArrayLayer = 0
      , layerCount     = faces
      }

acquireImage
  :: VulkanDevice
  -> FilePath
  -> Acquire (Vk.Image, Vk.Format, "mipLevels" ::: Word32, "faces" ::: Word32)
acquireImage vd path = do
  (image, format, mipLevels, faces, _alloc) <- mkAcquire (createImage vd path) (destroyImage vd)
  pure (image, format, mipLevels, faces)

createImage
  :: VulkanDevice
  -> FilePath
  -> IO (Vk.Image, Vk.Format, "mipLevels" ::: Word32, "faces" ::: Word32, VMA.Allocation)
createImage vd@VulkanDevice{..} path =
  Ktx1.fromFile path >>= \case
    Left (offset, err) ->
      throwIO $ LoadError offset (Text.pack err)
    Right Ktx1.Ktx{header=Ktx1.Header{..}, images} -> do
      -- XXX: https://github.com/KhronosGroup/KTX-Software/blob/bf849b7f/lib/vk_format.h#L676
      format <- case glInternalFormat of
        -- XXX: force SRGB, for now
        36492 ->
          -- GL_COMPRESSED_RGBA_BPTC_UNORM
          -- pure Vk.FORMAT_BC7_UNORM_BLOCK
          pure Vk.FORMAT_BC7_SRGB_BLOCK
        other ->
          case FromGL.internalFormat other of
            Nothing ->
              error $ "Unexpected glInternalFormat: " <> show glInternalFormat -- TODO: throwIo
            Just fmt ->
              -- XXX: going in blind
              pure fmt

      -- XXX: https://github.com/KhronosGroup/KTX-Software/blob/bf849b7f/lib/vkloader.c#L552
      let
        extent = Vk.Extent3D pixelWidth pixelHeight (max 1 pixelDepth)
        arrayLayers = max 1 numberOfArrayElements

      let
        mipSizes = fmap ((*) numberOfFaces . Ktx1.imageSize) images
        offsets = Vector.scanl' (+) 0 mipSizes
        totalSize = Vector.last offsets

      traceShowM (path, format, (Vector.length images, arrayLayers, numberOfFaces))
      traceShowM (offsets, Vector.length offsets, totalSize, extent)

      (image, allocation, _info) <- VMA.createImage
        _vdAllocator
        (imageCI format extent numberOfMipmapLevels arrayLayers numberOfFaces)
        imageAllocationCI

      Image.transitionImageLayout
        vd
        image
        numberOfMipmapLevels
        numberOfFaces -- XXX: arrayLayers is always 0 for now
        format
        Vk.IMAGE_LAYOUT_UNDEFINED
        Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL

      traceShowM (image, allocation)

      unless (numberOfArrayElements == 0) $
        error $ "Unexpected numberOfArrayElements: " <> show numberOfArrayElements

      case numberOfFaces of
        1 ->
          pure ()
        6 ->
          pure ()
        _rest ->
          error $ "Unexpected numberOfFaces: " <> show numberOfFaces

      VMA.withBuffer _vdAllocator (stageBufferCI totalSize) stageAllocationCI bracket \(staging, stage, stageInfo) -> do
        Vector.forM_ (Vector.zip offsets images) \(offset, Ktx1.MipLevel{imageSize, arrayElements}) ->
          Vector.forM_ arrayElements \Ktx1.ArrayElement{faces} -> do
            let ixFaces = Vector.zip (Vector.fromList [0..]) faces
            Vector.forM_ ixFaces \(faceIx, Ktx1.Face{zSlices}) ->
              Vector.forM_ zSlices \Ktx1.ZSlice{block} ->
                unsafeUseAsCStringLen block \(pixelsPtr, pixelBytes) -> do
                  let blockOffset = offset + faceIx * imageSize
                  let sectionPtr = Foreign.plusPtr (VMA.mappedData stageInfo) (fromIntegral blockOffset)
                  traceM $ "Poking z-slice of face " <> tshow (faceIx, faceIx * imageSize)
                  traceShowM (offset, imageSize, sectionPtr, pixelBytes)
                  Foreign.copyBytes sectionPtr (Foreign.castPtr pixelsPtr) pixelBytes

        VMA.flushAllocation _vdAllocator stage 0 Vk.WHOLE_SIZE

        copyBufferToImage vd staging image extent numberOfMipmapLevels numberOfFaces -- XXX: arrayLayers is always 0 for now

        oneshot vd \cmd ->
          cmdBarrier cmd Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT (barrierBase image)
            { Vk.subresourceRange = subr numberOfMipmapLevels numberOfFaces -- XXX: arrayLayers is always 0 for now
            , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
            , Vk.newLayout        = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
            , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
            , Vk.dstAccessMask    = Vk.ACCESS_SHADER_READ_BIT
            }

      pure (image, format, numberOfMipmapLevels, numberOfFaces, allocation)
  where
    imageCI format extent mipLevels arrayLayers faces = zero
      { Vk.flags         = flags
      , Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = format
      , Vk.extent        = extent
      , Vk.mipLevels     = mipLevels
      , Vk.arrayLayers   = if isCube then 6 else arrayLayers
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = usage
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = Vk.SAMPLE_COUNT_1_BIT -- XXX: no multisampling here
      }
      where
        isCube =
          faces == 6

        usage =
          Vk.IMAGE_USAGE_SAMPLED_BIT .|.  -- Sampler
          Vk.IMAGE_USAGE_TRANSFER_DST_BIT -- Staging

        flags =
          if isCube then
            Vk.IMAGE_CREATE_CUBE_COMPATIBLE_BIT
          else
            zero

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageBufferCI :: Integral a => a -> Vk.BufferCreateInfo '[]
    stageBufferCI pixelBytes = zero
      { Vk.size        = fromIntegral pixelBytes
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.flags         = VMA.ALLOCATION_CREATE_MAPPED_BIT
      , VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

    -- XXX: copypasta from CubeMap extended to multiple mipLevels
    cmdBarrier buf dstStage barrier = Vk.cmdPipelineBarrier
      buf
      Vk.PIPELINE_STAGE_TRANSFER_BIT
      dstStage
      zero
      mempty
      mempty
      (Vector.singleton $ SomeStruct barrier)

    barrierBase image = zero
      { Vk.srcQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.dstQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.image               = image
      }

    subr levels layers = Vk.ImageSubresourceRange
      { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = 0
      , baseArrayLayer = 0
      , levelCount     = levels
      , layerCount     = layers
      }

destroyImage
  :: VulkanDevice
  -> (Vk.Image, Vk.Format, "mipLevels" ::: Word32, "faces" ::: Word32, VMA.Allocation)
  -> IO ()
destroyImage vw (image, _fmt, _mipLevels, _faces, allocation) =
  VMA.destroyImage (_vdAllocator vw) image allocation

copyBufferToImage
  :: (MonadUnliftIO m)
  => VulkanDevice
  -> Vk.Buffer
  -> Vk.Image
  -> Vk.Extent3D
  -> Word32
  -> Word32
  -> m ()
copyBufferToImage vw src dst Vk.Extent3D{..} levels layers =
  oneshot vw \cmd ->
    Vk.cmdCopyBufferToImage cmd src dst Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL $
      Vector.generate (fromIntegral levels) \level -> zero
        { Vk.imageSubresource = Vk.ImageSubresourceLayers
            { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
            , mipLevel       = fromIntegral level
            , baseArrayLayer = 0
            , layerCount     = layers
            }
        , Vk.imageExtent = Vk.Extent3D
            { width  = max 1 $ width `shiftR` level
            , height = max 1 $ height `shiftR` level
            , depth  = depth
            }
        }
