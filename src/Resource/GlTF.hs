module Resource.GlTF where

import Import

import RIO.FilePath (takeDirectory, (</>))

import qualified Foreign
import qualified Data.ByteString as ByteString
import qualified Data.ByteString.Unsafe as ByteString
import qualified RIO.HashMap as HashMap
import qualified RIO.Vector as Vector
import qualified RIO.List as List
import qualified Linear.V3 as V3

import qualified Codec.GlTF as GlTF
import qualified Codec.GlTF.Accessor as Accessor
import qualified Codec.GlTF.Buffer as Buffer
import qualified Codec.GlTF.BufferView as BufferView
-- import qualified Codec.GlTF.Image as Image
import qualified Codec.GlTF.Mesh as Mesh
import qualified Codec.GlTF.Root as Root
import qualified Codec.GlTF.URI as URI

import qualified Render.Simple.Vertex as Simple

load :: FilePath -> IO (Indexed Simple.Vertex)
load fp = do
  GlTF.fromFile fp >>= \case
    Left err ->
      fail $ "glTF load error: " <> err
    Right root -> do
      let loader path = Right <$> ByteString.readFile (takeDirectory fp </> path)

      buffers <- case Root.buffers root of
        Nothing ->
          pure mempty
        Just buffers ->
          for buffers \case
            Buffer.Buffer{uri=Nothing} ->
              fail $ "Empty buffer URI in " <> show fp -- XXX: not loading GLB, are we?
            Buffer.Buffer{uri=Just uri} ->
              URI.loadURI loader uri >>= \case
                Left err ->
                  fail $ "Buffer load failed for " <> show uri <> ": " <> err
                Right bs ->
                  pure bs
      let
        getBuffer bix = case buffers Vector.!? Buffer.unBufferIx bix of
          Nothing ->
            fail $ show bix <> " not present in " <> show fp
          Just buffer ->
            pure buffer

      getAccessor <- case Root.accessors root of
        Nothing ->
          fail $ "No accessors in " <> fp
        Just accessors ->
          pure \aix ->
            case accessors Vector.!? Accessor.unAccessorIx aix of
              Nothing ->
                fail $ show aix <> " not present in " <> show fp
              Just accessor ->
                pure accessor

      getBufferView <- case Root.bufferViews root of
        Nothing ->
          fail $ "No buffer views in " <> fp
        Just bufferViews ->
          pure \bvix ->
            case bufferViews Vector.!? BufferView.unBufferViewIx bvix of
              Nothing ->
                fail $ show bvix <> " not present in " <> show fp
              Just bufferView ->
                pure bufferView

      let
        bufferElems :: Storable a => Accessor.AttributeType -> Accessor.ComponentType -> Accessor.AccessorIx -> IO [a]
        bufferElems = accessBuffer getAccessor getBufferView getBuffer

      meshes <- case Root.meshes root of
        Nothing ->
          fail $ "No meshes in " <> fp
        Just meshes ->
          for meshes \mesh -> do
            traceShowIO $ Mesh.name mesh

            for (Mesh.primitives mesh) \prim -> do
              case Mesh.mode prim of
                Mesh.TRIANGLES ->
                  pure ()
                mode ->
                  fail $ "Can't load anything but TRIANGLES, got " <> show mode

              indicesCCW <- case Mesh.indices prim of
                Nothing ->
                  fail "No indices for mesh primitive"
                Just aix ->
                  -- bufferElems @Word16 Accessor.SCALAR Accessor.UNSIGNED_SHORT aix
                  bufferElems @Word32 Accessor.SCALAR Accessor.UNSIGNED_INT aix

              -- traceShowIO (length indices, "indices" :: Text, indices)

              -- for (HashMap.toList $ Mesh.attributes prim) \(attr, aix) ->
              --   traceShowIO (attr, aix)

              let attrKeys = HashMap.keys $ Mesh.attributes prim

              positions <- case HashMap.lookup "POSITION" (Mesh.attributes prim) of
                Nothing ->
                  fail $ "Mesh primitive without POSITION attribute: " <> show attrKeys
                Just aix ->
                  bufferElems @Vec3 Accessor.VEC3 Accessor.FLOAT aix
              -- traceShowIO ("positions" :: Text, take 10 positions, length positions)

              normals <- case HashMap.lookup "NORMAL" (Mesh.attributes prim) of
                Nothing ->
                  fail $ "Mesh primitive without NORMAL attribute: " <> show attrKeys
                Just aix ->
                  bufferElems @Vec3 Accessor.VEC3 Accessor.FLOAT aix
              -- traceShowIO ("normals" :: Text, take 10 normals, length normals)

              texCoords0 <- case HashMap.lookup "TEXCOORD_0" (Mesh.attributes prim) of
                Nothing ->
                  fail $ "Mesh primitive without TEXCOORD_0 attribute: " <> show attrKeys
                Just aix ->
                  bufferElems @Vec2 Accessor.VEC2 Accessor.FLOAT aix
              -- traceShowIO ("texCoords0" :: Text, take 10 texCoords0, length texCoords0)

              tangents <- case HashMap.lookup "TANGENT" (Mesh.attributes prim) of
                Nothing ->
                  fail $ "Mesh primitive without TANGENT attribute: " <> show attrKeys
                Just aix ->
                  bufferElems @Vec4 Accessor.VEC4 Accessor.FLOAT aix
              traceShowIO ("tangents" :: Text, take 10 tangents, length tangents)

              let
                vertices = do
                  (pos, tc0, norm, tangent') <- List.zip4 positions texCoords0 normals tangents
                  let
                    (tangent, _handedness) = withVec4 tangent' \tx ty tz h -> (V3 tx ty tz, h)
                  pure Simple.Vertex
                    { vPos       = pos
                    , vNormal    = norm
                    , vTexCoord  = tc0
                    , vTangent   = tangent
                    }
              pure $ Indexed vertices indicesCCW

      pure . fold $ join meshes

accessBuffer
  :: forall a . (Storable a)
  => (Accessor.AccessorIx -> IO Accessor.Accessor)
  -> (BufferView.BufferViewIx -> IO BufferView.BufferView)
  -> (Buffer.BufferIx -> IO ByteString)
  -> Accessor.AttributeType
  -> Accessor.ComponentType
  -> Accessor.AccessorIx
  -> IO [a]
accessBuffer getAccessor getBufferView getBuffer expectAttribute expectComponent aix = do
  Accessor.Accessor{bufferView, byteOffset=accOffset, componentType, count, type'} <- getAccessor aix

  bv@BufferView.BufferView{byteOffset=bufOffset, byteLength} <- case bufferView of
    Nothing ->
      fail $ "No bufferView for index accessor " <> show aix
    Just bvix ->
      getBufferView bvix

  buffer <- getBuffer (BufferView.buffer bv)

  unexpected "type" expectAttribute type'
  unexpected "componentType" expectComponent componentType
  let strideSize = Foreign.sizeOf (error "strideSize.sizeOf" :: a)
  case BufferView.byteStride bv of
    Nothing ->
      pure ()
    Just stride
      | stride == strideSize ->
          pure ()
    Just stride ->
      unexpected "buffer view stride" strideSize stride

  let bytes = ByteString.take byteLength $ ByteString.drop (accOffset + bufOffset) buffer
  ByteString.unsafeUseAsCString bytes $
    Foreign.peekArray count . Foreign.castPtr

  where
    unexpected :: (Eq e, Show e) => String -> e -> e -> IO ()
    unexpected label expected got =
      unless (expected == got) $
        fail $ unlines
          [ "Unexpected " <> label <> "."
          , "  Expected " <> show expected <> ", got " <> show got
          , "  At " <> show aix
          ]
