module Resource.Object where

import Import

import Codec.Wavefront (WavefrontOBJ(..))

import qualified Codec.Wavefront as Obj
import qualified RIO.Text as Text
import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector

import qualified Geometry
import qualified Render.Simple.Vertex as Simple

data ResourceObjectError = ResourceObjectError Text
  deriving (Eq, Show)

instance Exception ResourceObjectError

load :: FilePath -> IO (Indexed Simple.Vertex)
load fp =
  Obj.fromFile fp >>= \case
    Left err ->
      throwM $ ResourceObjectError (Text.pack err)
    Right obj ->
      pure $ toIndexed obj
  where
    toIndexed WavefrontOBJ{..} = Geometry.indexed vertices
      where
        vertices = do
          el <- Vector.toList objFaces
          case el of
            Obj.Element{elValue} -> do
              let (faceTangent, face) = fromElement elValue

              (pos, V2 r s, normal) <- face
              pure Simple.Vertex
                { vPos       = pos
                , vTexCoord  = V2 r (1 - s)
                , vNormal    = normal
                , vTangent   = faceTangent
                }

        fromElement = \case
          Obj.Triangle a' b' c' ->
            ( tangent
                (pos1, uv1)
                (pos2, uv2)
                (pos3, uv3)
            , [a, b, c]
            )
            where
              a@(pos1, uv1, _n1) = getVertex a'
              b@(pos2, uv2, _n2) = getVertex b'
              c@(pos3, uv3, _n3) = getVertex c'

          huh ->
            error $ "Unexpected non-triangle element: " <> show huh

        getVertex vertex = (V3 x y z, tc, normal)
          where
            Obj.Location x y z _w = objLocations Vector.! (Obj.faceLocIndex vertex - 1)

            tc = case Obj.faceTexCoordIndex vertex of
              Nothing ->
                0.5
              Just ti ->
                V2 texcoordR texcoordS -- XXX: texcoordT ignored
                where
                  Obj.TexCoord{..} = objTexCoords Vector.! (ti - 1)

            normal = case Obj.faceNorIndex vertex of
              Nothing ->
                error $ "absent normal in " <> show fp -- TODO: ab x bc
              Just ti ->
                V3 norX norY norZ
                where
                  Obj.Normal{..} = objNormals Vector.! (ti - 1)

{-# INLINE tangent #-}
tangent :: (V3 Float, V2 Float) -> (V3 Float, V2 Float) -> (V3 Float, V2 Float) -> V3 Float
tangent (pos1, uv1) (pos2, uv2) (pos3, uv3) = V3
  (f * (deltaUV2y * edge1x - deltaUV1y * edge2x))
  (f * (deltaUV2y * edge1y - deltaUV1y * edge2y))
  (f * (deltaUV2y * edge1z - deltaUV1y * edge2z))
  where
    f = 1 / (deltaUV1x * deltaUV2y - deltaUV2x * deltaUV1y)

    V3 edge1x edge1y edge1z = pos2 - pos1
    V3 edge2x edge2y edge2z = pos3 - pos1
    V2 deltaUV1x deltaUV1y = uv2 - uv1
    V2 deltaUV2x deltaUV2y = uv3 - uv1
