{-# LANGUAGE TemplateHaskell #-}

module Resource.TH where

import RIO

import Data.Char (isDigit, isUpper, toUpper)
import Language.Haskell.TH (Q, Dec)
import Language.Haskell.TH.Syntax (qRunIO)
import RIO.Directory (doesDirectoryExist, doesFileExist, getDirectoryContents)
import RIO.FilePath (joinPath, (</>))
import RIO.State (StateT, evalStateT, get, put)

import qualified RIO.List as List
import qualified Language.Haskell.TH.Syntax as TH
import qualified RIO.Map as M

data Scope
  = Files
  | Dirs
  deriving (Eq, Ord, Show, Enum, Bounded, Generic)

filePaths :: Scope -> FilePath -> Q [Dec]
filePaths = mkDeclsWith mkPattern
  where
    mkPattern fp fs = do
      let name = TH.mkName "paths"

      sigType <- [t| [FilePath] |]

      let
        body = TH.ListE do
          segments <- List.sort fs
          pure $ TH.LitE . TH.StringL $ fp </> joinPath segments

      pure
        [ TH.SigD name sigType
        , TH.FunD name [TH.Clause [] (TH.NormalB body) []]
        ]

filePatterns :: Scope -> FilePath -> Q [Dec]
filePatterns = mkDeclsWith mkPattern
  where
    mkPattern fp fs =
      fmap concat . for fs $ \segments -> do
        let name = TH.mkName . map (replace . toUpper) $ List.intercalate "_" segments
        patType <- [t| FilePath |]
        let pat = TH.LitP . TH.StringL $ fp </> joinPath segments

        pure
          [ TH.PatSynSigD name patType
          , TH.PatSynD name (TH.PrefixPatSyn []) TH.ImplBidir pat
          ]

    replace c =
      if isUpper c || isDigit c then
        c
      else
        '_'

mkDeclsWith
  :: (FilePath -> [[String]] -> Q [Dec])
  -> Scope
  -> FilePath
  -> Q [Dec]
mkDeclsWith mkDecl scope fp =
  qRunIO (getFileListPieces scope fp) >>= mkDecl fp

-- XXX: Sourced from yesod-static
getFileListPieces :: Scope -> FilePath -> IO [[String]]
getFileListPieces scope = flip evalStateT M.empty . flip go id
  where
    go :: String
       -> ([String] -> [String])
       -> StateT (M.Map String String) IO [[String]]
    go fp front = do
      allContents <- liftIO $ filter notHidden `fmap` getDirectoryContents fp
      let fullPath :: String -> String
          fullPath f = fp ++ '/' : f
      files <- liftIO $ filterM (doesFileExist . fullPath) allContents
      dirs <- liftIO $ filterM (doesDirectoryExist . fullPath) allContents
      inner <- mapM (\f -> go (fullPath f) (front . (:) f)) dirs

      current <- case scope of
        Files ->
          mapM dedupe $ map (front . pure) files
        Dirs ->
          mapM dedupe $ map (front . pure) dirs

      pure $ concat $ current : inner

    -- Reuse data buffers for identical strings
    dedupe :: [String] -> StateT (M.Map String String) IO [String]
    dedupe = mapM dedupe'

    dedupe' :: String -> StateT (M.Map String String) IO String
    dedupe' s = do
      m <- get
      case M.lookup s m of
        Just s' -> pure s'
        Nothing -> do
          put $ M.insert s s m
          pure s

    notHidden :: FilePath -> Bool
    notHidden = \case
      "tmp"   -> False
      '.' : _ -> False
      _       -> True
