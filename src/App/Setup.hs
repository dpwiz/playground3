module App.Setup where

import Import

import Data.Acquire (Acquire, mkAcquire)
import RIO.Orphans (ResourceMap)
import RIO.Process

import qualified SDL

import qualified Vulkan.Device as Vulkan

setup :: Options -> ResourceMap -> Acquire (App () ())
setup options@Options{..} resourceMap = do
  logOptions     <- logOptionsHandle stderr optionsVerbose
  (logFunc, _)   <- mkAcquire (newLogFunc logOptions) snd
  processContext <- mkDefaultProcessContext

  sizes <- withSdl
  case reverse (sort sizes) of
    [] -> do
      SDL.showSimpleMessageBox Nothing SDL.Information "LOL" "You have no display"
      exitFailure
    largestWidth : _rest -> do
      sdlWindow <- withSdlWindow options largestWidth

      vulkanDevice <- Vulkan.withDevice options sdlWindow -- TODO: convert DeviceError to SDL messsage

      -- XXX: Actual state resources are tied to context and should be more limited in scope.
      setupState <- newSomeRef ()

      pure App
        { _appOptions        = options
        , _appLogFunc        = logFunc
        , _appProcessContext = processContext
        , _appResourceMap    = resourceMap
        , _appSdlWindow      = sdlWindow
        , _appVulkanDevice   = vulkanDevice
        , _appContext        = ()
        , _appState          = setupState
        }

withSdl :: Acquire [V2 CInt]
withSdl = mkAcquire (SDL.initializeAll >> getDisplaySizes) (const SDL.quit)

withSdlWindow :: Options -> V2 CInt -> Acquire SDL.Window
withSdlWindow Options{..} largest = mkAcquire create SDL.destroyWindow
  where
    create = SDL.createWindow "Haskell Playground 3" config

    config = SDL.defaultWindow
      { SDL.windowGraphicsContext = SDL.VulkanContext
      , SDL.windowMode            = mode
      , SDL.windowInitialSize     = initialSize
      , SDL.windowBorder          = mode /= SDL.Fullscreen
      , SDL.windowResizable       = mode /= SDL.Fullscreen
      }

    initialSize =
      case optionsWindowSize of
        Nothing ->
          if optionsFullscreen then
            largest
          else
            fmap (`div` 2) largest
        Just size ->
          fmap fromIntegral size

    mode =
      if optionsFullscreen then
        SDL.Fullscreen
      else
        SDL.Windowed

getDisplaySizes :: MonadIO m => m [V2 CInt]
getDisplaySizes = do
  displays <- SDL.getDisplays
  pure $ map SDL.displayBoundsSize displays
