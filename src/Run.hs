module Run (run) where

import Import

import Control.Monad.Trans.Resource (release)
import Data.Acquire (withAcquire)
import GHC.Clock (getMonotonicTimeNSec)
import System.Mem (performGC)

import qualified SDL
import qualified Vulkan.Core10 as Vk

import Render.Draw (Draw(..), drawFrame)

import qualified Game.Draw.Shadow as Shadow
import qualified Game.Draw.Solids as Solids
import qualified Game.Draw.UI as UI
import qualified Game.Draw.World as World
import qualified Game.Events as Events
import qualified Game.Resources as Resources
import qualified Game.Scene as Scene
import qualified Game.World as World

run :: RIO AppSetup ()
run = do
  window <- view appSdlWindow

  devName <- views (appVulkanDevice . vdPhysical . pdProperties) Vk.deviceName
  logInfo $ "Running on " <> displayShow devName

  SDL.showWindow window

  vd <- view appVulkanDevice
  withAcquire (Resources.setup vd) \initialState -> do
    Scene.initial initialState
    contextLoop initialState

contextLoop :: GameState -> RIO AppSetup ()
contextLoop previousGameState = do
  window <- view appSdlWindow
  dev <- view appVulkanDevice
  nextGameState <- withAcquire (Resources.acquire window dev previousGameState) \newCtx -> do
    stateRef <- newSomeRef previousGameState
    oldApp <- view id
    let
      appGameLoop = oldApp
        { _appContext = newCtx
        , _appState   = stateRef
        }
    time0 <- liftIO getMonotonicTimeNSec
    runRIO appGameLoop (mainLoop time0 time0)
  case nextGameState of
    Just restart -> do
      logInfo "Restarting contextLoop"
      contextLoop restart
    Nothing ->
      logInfo "Bye!"

mainLoop :: Word64 -> Word64 -> RIO AppGame (Maybe GameState)
mainLoop prevTime startTime = do
  SDL.pollEvents >>= mapM_ Events.handle
  Events.handleTime prevTime startTime
  (shadow, scenes) <- makeFrame

  quitApp <- use gsQuit
  updateContext <- use gsUpdateContext
  if quitApp || updateContext then do
    views appVulkanDevice _vdLogical >>= Vk.deviceWaitIdle

    let DrawCommands transient _cmd = mconcat $ shadow : scenes
    traverse_ release transient

    if quitApp then
      pure Nothing
    else do
      -- XXX: dump final state for new context iteration to pick up
      old <- use id
      pure $ Just old
        { _gsUpdateContext = False
        }

  else do
    drawFrame shadow scenes

    Options{optionsFrameGC, optionsFrameDelay} <- view appOptions
    when optionsFrameGC $
      liftIO performGC
    when optionsFrameDelay do
      endTime <- liftIO getMonotonicTimeNSec
      let frameTime = fromIntegral (endTime - startTime) / 1_000_000 :: Double
      let budget = truncate $ 1_000_000 / 60 - frameTime
      -- logInfo $ "delay: " <> displayShow (frameTime, budget, delay)
      -- BUG: it is kinda wrong to insert extra space between draws
      -- TODO: split world state update and frame preparation into separate threads
      threadDelay $ min 0 budget

    nextTicks <- liftIO getMonotonicTimeNSec
    mainLoop startTime nextTicks

makeFrame :: RIO AppGame (Draw, [Draw])
makeFrame = do
  (World.Toggles{..}, World.Time{..}) <- World.run World.globalGet

  shadow <- Shadow.makeScene

  solidDraw <-
    if _toggleSolids then
      Solids.makeScene
    else
      Solids.release

  world <-
    if _toggleWorld then
      World.makeScene
    else
      mempty

  ui <-
    if _toggleUI then
      UI.makeUI
    else
      mempty

  pure
    ( shadow
    , [ solidDraw
      , world
      , ui
      ]
    )
