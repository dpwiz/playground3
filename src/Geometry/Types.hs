{-# LANGUAGE TemplateHaskell #-}

module Geometry.Types where

import RIO

import Control.Lens.TH (makeLenses)
import Geomancy (Vec4, vec4)
import Geomancy.Transform (Transform)
import Linear (V2(..))
import Vulkan.Zero (Zero(..))

import qualified Foreign
import qualified RIO.Vector.Storable as Storable
import qualified RIO.Vector.Storable.Unsafe as Storable (unsafeWith)

class VertexAttribs a where
  vertexAttribs :: [a] -> [Float]
  vertexPositions :: [a] -> [Float]

  default vertexPositions :: HasPosition a => [a] -> [Float]
  vertexPositions = concatMap getPosition

class HasPosition a where
  getPosition :: a -> [Float]

data Indexed a = Indexed
  { iItems   :: [a]
  , iIndices :: [Word32]
  }
  deriving (Show, Functor, Foldable, Traversable)

instance Semigroup (Indexed a) where
  -- XXX: half-assed, but should work
  a <> b = Indexed
    { iItems   = iItems a <> iItems b
    , iIndices = iIndices a <> map ((+) ixOffset) (iIndices b)
    }
    where
      ixOffset = fromIntegral $ length (iItems a)

instance Monoid (Indexed a) where
  mempty = Indexed mempty mempty

data SceneView = SceneView
  { _svView        :: Transform
  , _svProjection  :: Transform
  , _svViewPosTime :: Vec4
  , _svDirection   :: Vec4
  , _svNumLights   :: Word32
  , _svEnvCube     :: Word32
  } deriving (Show)

instance Zero SceneView where
  zero = SceneView
    { _svView        = mempty
    , _svProjection  = mempty
    , _svViewPosTime = vec4 0 0 0 0
    , _svDirection   = vec4 0 0 (-1) 0
    , _svNumLights   = 0
    , _svEnvCube     = 0
    }

instance Storable SceneView where
  sizeOf ~SceneView{..} = sum
    [ Foreign.sizeOf _svView
    , Foreign.sizeOf _svProjection
    , Foreign.sizeOf _svViewPosTime
    , Foreign.sizeOf _svDirection
    , Foreign.sizeOf _svNumLights
    , Foreign.sizeOf _svEnvCube
    ]

  alignment = const 16

  poke ptr SceneView{..} = do
    Foreign.poke        (Foreign.castPtr ptr)                 _svView
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64)            _svProjection
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64)         _svViewPosTime
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64+16)      _svDirection
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64+16+16)   _svNumLights
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+64+16+16+4) _svEnvCube

  peek ptr = SceneView
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 64
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64+16)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64+16+16)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+64+16+16+4)

pattern MAX_LIGHTS :: (Eq a, Num a) => a
pattern MAX_LIGHTS = 128

newtype SceneLights = SceneLights (Storable.Vector SpotLight)
  deriving (Show)

instance Zero SceneLights where
  zero = SceneLights mempty

instance Storable SceneLights where
  alignment = const 16

  sizeOf = const $ Foreign.sizeOf (undefined :: SpotLight) * MAX_LIGHTS

  poke dst (SceneLights lights) =
    if size == 0 then
      pure ()
    else
      Storable.unsafeWith lights \src ->
        Foreign.copyArray (Foreign.castPtr dst) src size
    where
      size = min MAX_LIGHTS $ Storable.length lights

  peek = error "SceneLights are write-only"

data SpotLight = SpotLight
  { _slVP          :: Transform -- ^ to bring models into light-space
  , _slColor       :: Vec4      -- ^ a is intensity
  , _slPosition    :: Vec4      -- ^ a is attenuation power 0..2
  , _slDirection   :: Vec4      -- ^ w is ambient spill
  , _slCutoff      :: V2 Float  -- ^ inner/outer cosine
  , _slShadowRange :: V2 Float  -- ^ near/far
  } deriving (Show)

instance Zero SpotLight where
  zero = SpotLight
    { _slVP          = mempty
    , _slColor       = 0
    , _slPosition    = 0
    , _slDirection   = 0
    , _slCutoff      = 0
    , _slShadowRange = 0
    }

instance Storable SpotLight where
  alignment = const 16

  sizeOf ~SpotLight{} = 64 + 16 + 16 + 16 + 8 + 8

  poke ptr SpotLight{..} = do
    Foreign.poke        (Foreign.castPtr ptr)                 _slVP
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64)            _slColor
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+16)         _slPosition
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+16+16)      _slDirection
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+16+16+16)   _slCutoff
    Foreign.pokeByteOff (Foreign.castPtr ptr) (64+16+16+16+8) _slShadowRange

  peek ptr = SpotLight
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+16)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+16+16)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+16+16+16)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (64+16+16+16+8)

data TextureCombo = TextureCombo
  { _tSampler :: Word32
  , _tTexture :: Word32
  } deriving (Eq, Ord, Show, Generic)

instance Zero TextureCombo where
  zero = TextureCombo
    { _tSampler = 0
    , _tTexture = 0
    }

instance Hashable TextureCombo

instance Storable TextureCombo where
  alignment = const 4

  sizeOf ~TextureCombo{} = 8

  peek ptr = TextureCombo
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 4

  poke ptr TextureCombo{..} = do
    Foreign.poke        (Foreign.castPtr ptr)   _tSampler
    Foreign.pokeByteOff (Foreign.castPtr ptr) 4 _tTexture

data Bitmap = Bitmap
  { _tBase      :: Vec4
  , _tOffset    :: V2 Float
  , _tScale     :: V2 Float
  , _tSamplerId :: Word32
  , _tImageId   :: Word32
  } deriving (Eq, Show)

instance Zero Bitmap where
  zero = Bitmap
    { _tBase      = 0
    , _tOffset    = 0
    , _tScale     = 0
    , _tSamplerId = 0
    , _tImageId   = 0
    }

instance Storable Bitmap where
  sizeOf ~Bitmap{..} = sum
    [ Foreign.sizeOf _tBase      -- 16
    , Foreign.sizeOf _tOffset    -- 8
    , Foreign.sizeOf _tScale     -- 8
    , Foreign.sizeOf _tSamplerId -- 4
    , Foreign.sizeOf _tImageId   -- 4
    ]

  alignment = const 16

  poke ptr Bitmap{..} = do
    Foreign.poke        (Foreign.castPtr ptr)            _tBase
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16)       _tOffset
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8)     _tScale
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8+8)   _tSamplerId
    Foreign.pokeByteOff (Foreign.castPtr ptr) (16+8+8+4) _tImageId

  peek ptr = Bitmap
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 16
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (16+8+8+4)

data Origin
  = Begin
  | Middle
  | End
  deriving (Eq, Ord, Show, Enum, Bounded)

data CubeSides a = CubeSides
  { _cubeRight  :: a -- ^ X+
  , _cubeLeft   :: a -- ^ X-
  , _cubeTop    :: a -- ^ Y-
  , _cubeBottom :: a -- ^ Y+
  , _cubeFront  :: a -- ^ Z+
  , _cubeBack   :: a -- ^ Z-
  } deriving (Eq, Ord, Show, Functor, Foldable, Traversable, Generic)

makeLenses ''SceneView
makeLenses ''Bitmap
makeLenses ''CubeSides
