module Geometry.Layout3D
  ( Scene
  , node, node_
  , leaf, leaf_

  , unfoldTree
  , unfoldForest

  , flatGroups
  , flatten
  ) where

import Import

import Data.Tree (Tree(..), unfoldForest, unfoldTree)
import Geomancy.Transform (Transform)

type Scene a = Tree (Transform, Maybe a)

-- * Construction

node :: Transform -> a -> [Scene a] -> Scene a
node tr mat children =
  Node (tr, Just mat) children

node_ :: Transform -> [Scene a] -> Scene a
node_ tr children =
  Node (tr, Nothing) children

leaf :: Transform -> a -> Scene a
leaf tr mat =
  node tr mat []

leaf_ :: a -> Scene a
leaf_ = leaf mempty

-- * Consumption

flatGroups :: Functor group => [group [Scene a]] -> [group [(Transform, a)]]
flatGroups = map (fmap $ concatMap flatten)

flatten :: Scene a -> [(Transform, a)]
flatten = fold . unfoldTree \Node{rootLabel=(curTrans, curMat), subForest} ->
  ( case curMat of
      Nothing ->
        []
      Just mat ->
        [(curTrans, mat)]
  , do
      Node (subTrans, subNode) subChildren <- subForest
      pure $ Node (subTrans <> curTrans, subNode) subChildren
  )
