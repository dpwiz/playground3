module Geometry.Layout2D where

import Import

import Geomancy.Transform (Transform(..))

import qualified Geomancy.Mat4 as Mat4
import qualified Vulkan.Core10 as Vk

type Container = V4 Float

{-# INLINE screen #-}
screen :: Vk.Extent2D -> Container
screen Vk.Extent2D{width, height} =
  V4 (fromIntegral width) (fromIntegral height) 0 0

{-# INLINE fitSized #-}
fitSized :: Container -> V2 Float -> Transform
fitSized container size =
  fit container size \(V4 cw ch cx cy) ->
    Mat4.rowMajor
      cw 0  0 0
      0  ch 0 0
      0  0  1 0
      cx cy 0 1

{-# INLINE fillSized #-}
fillSized :: V4 Float -> Transform
fillSized container@(V4 cw ch _cx _cy) =
  fitSized container (V2 cw ch)

{-# INLINE pad #-}
pad :: Container -> Float -> (Container -> r) -> r
pad (V4 w h x y) v f = f $ V4 (w - 2*v) (h - 2*v) x y

{-# INLINE fit #-}
fit :: Container -> V2 Float -> (Container -> r) -> r
fit (V4 cw ch cx cy) (V2 ow oh) f = f $ V4 (ow * scale) (oh * scale) cx cy
  where
    scale =
      if cw / ch >= ow / oh then
        ch / oh
      else
        cw / ow

type Splitter r = Container -> Either Float Float -> (Container -> Container -> r) -> r

{-# INLINE split2Hat #-}
split2Hat :: Splitter r
split2Hat container@(V4 cw ch cx cy) leftSize f =
  case leftSize of
    Left cut ->
      f
        (V4 cut        ch (cx + cut / 2 - cw / 2) cy)
        (V4 (cw - cut) ch (cx + cut / 2)          cy)

    Right alpha ->
      split2Hat container (Left $ cw * alpha) f

{-# INLINE split2H #-}
split2H :: Container -> (Container -> Container -> r) -> r
split2H container = split2Hat container (Right $ 1/2)

{-# INLINE split2Vat #-}
split2Vat :: Splitter r
split2Vat container@(V4 cw ch cx cy) topSize f =
  case topSize of
    Left cut ->
      f
        (V4 cw cut        cx (cy + ch / 2 - cut / 2))
        (V4 cw (ch - cut) cx (cy - cut / 2))

    Right alpha ->
      split2Vat container (Left $ ch * alpha) f

{-# INLINE split2V #-}
split2V :: Container -> (Container -> Container -> r) -> r
split2V container = split2Vat container (Right $ 1/2)

{-# INLINE splitN #-}
splitN :: Container -> Splitter (Container, Container) -> [Container -> r] -> [r]
splitN initial splitter items = final
  where
    (_c, _n, final) = foldl' f (initial, length items, mempty) items

    f (container, n, acc) proc =
      let
        (cur, next) = splitter container (Right $ 1 / fromIntegral n) (,)
      in
        (next, n - 1, proc cur : acc)

{-# INLINE cutN #-}
cutN :: Container -> Splitter (Container, Container) -> Float -> [Container -> r] -> [r]
cutN initial splitter slice items = final
  where
    (_c, _n, final) = foldl' f (initial, length items, mempty) items

    f (container, n, acc) proc =
      let
        (cur, next) = splitter container (Left slice) (,)
      in
        (next, n - 1, proc cur : acc)
