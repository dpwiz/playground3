module Geometry.Solids where

import Import

tetrahedron :: Float -> Indexed Vec3
tetrahedron r = Indexed vertices indices
  where
    phiaa = -19.471220333
    phia = pi * phiaa / 180

    origin = V3 0 0 (-r)

    vertices = origin : do
      ix <- [1..3]
      let the = ix * 120 / 180 * pi
      pure $ V3
        (r * cos the * cos phia)
        (r * sin the * cos phia)
        (negate $ r * sin phia)

    indices =
      [ 0, 2, 1
      , 0, 3, 2
      , 0, 1, 3
      , 1, 2, 3
      ]
