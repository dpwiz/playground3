module Game.Resources
  ( setup
  , acquire
  ) where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified Geomancy.Transform as Transform
import qualified RIO.List as List
import qualified RIO.Map as Map
import qualified RIO.Vector as Vector
import qualified SDL
import qualified Vulkan.Core10 as Vk

import qualified Game.Static.CubeMaps as CubeMaps
import qualified Game.Static.Fonts as Fonts
import qualified Game.Static.Models as Models
import qualified Game.Static.Textures as Textures
import qualified Game.World as World
import qualified Render.Model as Model
import qualified Render.SDF.Context as SDF
import qualified Render.Shadow.Context as Shadow
import qualified Render.Simple.Context as Simple
import qualified Render.Simple.Vertex as Simple
import qualified Render.Solid.Context as Solid
import qualified Render.Sprite.Context as Sprite
import qualified Render.Texture as Texture
import qualified Resource.Font.Evanw as FontB
import qualified Resource.Font.Proxima as FontS
import qualified Vulkan.Context as Vulkan

setup :: VulkanDevice -> Acquire GameState
setup vd = do
  world <- World.setup

  fontB <- liftIO $ FontB.load Fonts.UBUNTU_24_JSON
  fontS <- liftIO $ FontS.load Fonts.ROBOTO_REGULAR_JSON

  square <- Model.acquireIndexed vd Simple.square
  cube   <- Model.acquireObj vd Models.CUBE_OBJ mempty
  sphere <- Model.acquireObj vd Models.SPHERE_OBJ $
    Transform.rotateX pi <> Transform.scale 0.05

  room    <- Model.acquireGltf vd Models.VIKING_ROOM_GLTF $ Transform.rotateX pi -- XXX: vulkan is -Y up
  roomObj <- Model.acquireObj vd Models.VIKING_ROOM_OBJ $ Transform.rotateX (-pi/2) -- XXX: model lies on its side o_O

  sprite <- Sprite.acquireModel vd

  let fillerTexturePath = Textures.BLACK_PNG
  filler <- Texture.acquire vd fillerTexturePath
  let ixTextures = zip [1..] $ List.delete fillerTexturePath Textures.paths
  textures <- for ixTextures \(ix, fp) -> do
    imageView <- Texture.acquire vd fp
    pure (Map.singleton fp ix, imageView)
  let (textureIds, textureViews) = List.unzip textures

  let fillerCubePath = CubeMaps.BLACK_KTX
  fillerCube <- Texture.acquire vd fillerCubePath
  let ixCubes = zip [1..] $ List.delete fillerCubePath CubeMaps.paths
  cubemaps <- for ixCubes \(ix, fp) -> do
    imageView <- Texture.acquire vd fp
    pure (Map.singleton fp ix, imageView)
  let (cubeIds, cubeViews) = List.unzip cubemaps

  pure GameState
    { _gsQuit          = False
    , _gsUpdateContext = False
    , _gsInflight      = List.cycle $ toList (_vdInflightData vd)

    , _gsWorld = world

    , _gsFontB = fontB
    , _gsFontS = fontS

    , _gsSquare = square
    , _gsCube   = cube
    , _gsSphere = sphere

    , _gsRoom    = room
    , _gsRoomObj = roomObj

    , _gsSprite = sprite

    , _gsTextures   = Vector.fromList $ filler : textureViews
    , _gsTextureIds = Map.insert fillerTexturePath 0 $ mconcat textureIds

    , _gsCubeMaps = Vector.fromList $ fillerCube : cubeViews
    , _gsCubeIds  = Map.insert fillerCubePath 0 $ mconcat cubeIds
    }

acquire :: SDL.Window -> VulkanDevice -> GameState -> Acquire GameContext
acquire window vd GameState{..} = do
  _gcVulkan <- Vulkan.acquire window vd

  (_gcSimple, _gcSimpleBlend, _gcSkybox, _gcSimpleRender) <- Simple.acquire
    vd
    _gcVulkan
    _gsTextures
    _gsCubeMaps

  (_gcSolid, _gcSolidRender)   <- Solid.acquire vd _gcVulkan
  (_gcSprite, _gcSpriteRender) <- Sprite.acquire vd _gcVulkan _gsTextures
  (_gcSdf, _gcSdfRender)       <- SDF.acquire vd _gcVulkan _gsTextures

  (_gcShadow, _gcShadowRender) <- Shadow.acquire vd _gcVulkan

  -- XXX: guard against uniforms still in use somewhere
  mkAcquire (pure ()) (\() -> Vk.deviceWaitIdle $ _vdLogical vd)

  pure GameContext{..}
