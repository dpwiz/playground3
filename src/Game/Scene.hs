module Game.Scene (initial) where

import Import

import qualified Apecs
import qualified Geomancy.Quaternion as Quaternion
import qualified Geomancy.Transform as Transform

import qualified Game.Static.CubeMaps as CubeMaps
import qualified Game.Static.Materials as Materials
import qualified Game.World as World
import qualified Game.World.Components.Draw as Draw
import qualified Game.World.Systems.Drawable as Drawable

initial :: MonadIO m => GameState -> m ()
initial gs = do
  stateRef <- newSomeRef gs
  runRIO stateRef do
    room <- Drawable.spawnMesh gsRoom Materials.vikingRoom

    ball <- Drawable.spawnMesh gsSphere Materials.mirror

    halo <- Drawable.spawnBillboard Materials.flare 0

    target <- Drawable.spawnMesh gsCube Materials.hgj

    -- TODO: add toggle
    -- Drawable.setEnvCube CubeMaps.TEST_KTX
    Drawable.setEnvCube CubeMaps.GALAXY_KTX

    World.run do
      World.Camera{..} <- World.globalGet

      Apecs.set target
        ( World.CameraBound
            { _cbDirection = Quaternion.lookAtUp 0 (vec3 0 0 1) _cameraUp
            , _cbPosition  = vec3 1 1 1 -- TODO: shift only for 1st person camera
            , _cbOrient    = True
            }
        , Draw.Light
            { _lightColor       = vec4 1 1 1 32
            , _lightAttenuation = 1
            , _lightAmbient     = 0
            , _lightCutoff      = cos $ V2 (pi/6) (pi/4)
            }
        , Draw.ShadowMapped
        , World.Emitter
        , World.HeadLights
        )

      _roomLight <- Apecs.newEntity
        ( Draw.Light
            { _lightColor       = vec4 1 1 1 4
            , _lightAttenuation = 0.5
            , _lightAmbient     = 0.0
            , _lightCutoff      = cos $ V2 pi pi
            }
        , World.Position $ vec3 0 0 0
        , World.Direction $ Quaternion.rotationBetween (vec3 0 0 1) (vec3 0 (1) 0)
        , World.RoomLights
        )

      Apecs.set room
        ( World.Position $ vec3 0 10 0
        , Draw.ShadowCaster
        )
      Apecs.modify room Drawable.bakeInstance

      Apecs.set ball
        ( World.Position 0
        , Draw.ShadowCaster
        )
      Apecs.modify ball $
        Draw.modelTransform <>~ Transform.scale 1.1
      Apecs.modify ball Drawable.bakeInstance

      Apecs.modify halo $
        Draw.modelTransform <>~ Transform.scale 5
      Apecs.modify halo Drawable.bakeInstance

      _ribbon <- Apecs.newEntity
        ( Draw.SolidRibbon
        , World.Position $ vec3 0 0 0
        , World.Direction $ quaternion 1 0 0 1
        , Draw.ShadowCaster
        )

      Drawable.updateBillboards
      Drawable.updateFlashlight
