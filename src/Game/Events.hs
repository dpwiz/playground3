module Game.Events where

import Import hiding (Vec3)

import Data.Acquire (allocateAcquire)

import qualified Apecs
import qualified Apecs.Experimental.Reactive as Apecs
import qualified Geomancy.Transform as Transform
import qualified Geomancy.Quaternion as Quaternion
import qualified Geomancy.Vec3 as Vec3
import qualified SDL
import qualified VulkanMemoryAllocator as VMA

import qualified Game.Draw.Solids as Solids
import qualified Game.Static.CubeMaps as CubeMaps
import qualified Game.World as World
import qualified Game.World.Components.Draw as Draw
import qualified Game.World.Systems.Drawable as Drawable
import qualified Render.Model as Model

handle :: SDL.Event -> RIO AppGame ()
handle event = do
  -- traceShowM ("Game.Events.handle", event)
  case SDL.eventPayload event of
    SDL.KeyboardEvent kb ->
      handleKeyboard kb
    SDL.MouseButtonEvent mb ->
      handleMouseButton mb
    SDL.MouseMotionEvent mm ->
      handleMouseMotion mm
    SDL.QuitEvent ->
      gsQuit .= True
    _ ->
      pure ()

handleKeyboard :: SDL.KeyboardEventData -> RIO AppGame ()
handleKeyboard SDL.KeyboardEventData{..} =
  case SDL.keysymKeycode keyboardEventKeysym of
    _ignore | keyboardEventRepeat ->
      pure ()

    SDL.KeycodeEscape ->
      -- XXX: move to World?
      gsQuit .= True

    SDL.Keycode1 | pressed ->
      toggle World.toggleSolids

    SDL.Keycode2 | pressed ->
      toggle World.toggleWorld

    SDL.Keycode3 | pressed ->
      toggle World.toggleUI

    SDL.KeycodeF1 | pressed -> do
      toggle World.toggleEnv
      World.Toggles{_toggleEnv} <- World.globalGet
      if _toggleEnv then
        Drawable.setEnvCube CubeMaps.TEST_KTX
      else
        Drawable.setEnvCube CubeMaps.GALAXY_KTX

    SDL.KeycodeF2 | pressed -> do
      toggle World.toggleHeadlight
      World.run do
        World.Toggles{_toggleHeadlight} <- World.globalGet
        headlights <- Apecs.withReactive $ Apecs.enumLookup World.HeadLights
        for_ headlights \light ->
          Apecs.modify light $
            Draw.lightColor .~
              if _toggleHeadlight then
                vec4 1 1 1 32
              else
                vec4 1 1 1 0

    SDL.KeycodeF3 | pressed -> do
      toggle World.toggleRoomlight
      World.run do
        World.Toggles{_toggleRoomlight} <- World.globalGet
        roomlights <- Apecs.withReactive $ Apecs.enumLookup World.RoomLights
        for_ roomlights \light ->
          Apecs.modify light $
            Draw.lightColor .~
              if _toggleRoomlight then
                vec4 1 1 1 4
              else
                vec4 1 1 1 0

    SDL.KeycodeW ->
      if pressed then
        walk 5
      else
        walkStop

    SDL.KeycodeS ->
      if pressed then
        walk (-3)
      else
        walkStop

    SDL.KeycodeD ->
      if pressed then
        strafe 4
      else
        strafeStop

    SDL.KeycodeA ->
      if pressed then
        strafe (-4)
      else
        strafeStop

    SDL.KeycodeE ->
      if pressed then
        fly 2
      else
        flyStop

    SDL.KeycodeQ ->
      if pressed then
        fly (-2)
      else
        flyStop

    SDL.KeycodeLeft ->
      if pressed then
        yaw 1
      else
        yawStop

    SDL.KeycodeRight ->
      if pressed then
        yaw (-1)
      else
        yawStop

    SDL.KeycodeUp ->
      if pressed then
        pitch 1
      else
        pitchStop

    SDL.KeycodeDown ->
      if pressed then
        pitch (-1)
      else
        pitchStop

    SDL.KeycodeSpace | pressed ->
      Drawable.spawnSample1

    SDL.KeycodeBackspace | pressed ->
      Drawable.wipeSpawned

    SDL.KeycodeF12 | pressed ->
      dumpVMA

    _ ->
      pure ()
  where
    pressed = keyboardEventKeyMotion == SDL.Pressed

handleMouseButton :: SDL.MouseButtonEventData -> RIO AppGame ()
handleMouseButton SDL.MouseButtonEventData{..} =
  case mouseButtonEventButton of
    SDL.ButtonLeft | pressed ->
      Drawable.spawnSample2

    SDL.ButtonRight | pressed -> do
      toggle World.toggleMouse
      World.Toggles{_toggleMouse} <- World.run World.globalGet
      void . SDL.setMouseLocationMode $
        if _toggleMouse then
          SDL.RelativeLocation
        else
          SDL.AbsoluteLocation
    _ ->
      pure ()
  where
    pressed = mouseButtonEventMotion == SDL.Pressed

handleMouseMotion :: SDL.MouseMotionEventData -> RIO AppGame ()
handleMouseMotion SDL.MouseMotionEventData{..} = do
  (World.Toggles{_toggleMouse}, World.Camera{..}) <- World.run World.globalGet
  when _toggleMouse do
    let
      SDL.V2 mx my = mouseMotionEventRelMotion
      rx = fromIntegral (-mx) / _cameraFov / tau
      ry = fromIntegral my / _cameraFov / tau

    World.globalModify \World.Camera{..} ->
      World.Camera
        { World._cameraTarget =
            Quaternion.rotatePoint
              (Quaternion.axisAngle _cameraUp rx)
              _cameraOrigin
              _cameraTarget
        , ..
        }
    let cameraRight = Vec3.cross _cameraUp (_cameraTarget - _cameraOrigin)
    World.globalModify \World.Camera{..} ->
      World.Camera
        { World._cameraTarget =
            Quaternion.rotatePoint
              (Quaternion.axisAngle cameraRight ry)
              _cameraOrigin
              _cameraTarget
        , ..
        }
    World.run Drawable.updateFlashlight

toggle :: Lens' World.Toggles Bool -> RIO AppGame ()
toggle f = World.run $
  World.globalModify (f %~ not)

walk :: Float -> RIO AppGame ()
walk speed = World.run $
  World.globalSet (World.Walk speed)

walkStop :: RIO AppGame ()
walkStop = World.run $
  World.globalSet (Apecs.Not @World.Walk)

fly :: Float -> RIO AppGame ()
fly speed = World.run $
  World.globalSet (World.Fly speed)

flyStop :: RIO AppGame ()
flyStop = World.run $
  World.globalSet (Apecs.Not @World.Fly)

strafe :: Float -> RIO AppGame ()
strafe speed = World.run $
  World.globalSet (World.Strafe speed)

strafeStop :: RIO AppGame ()
strafeStop = World.run $
  World.globalSet (Apecs.Not @World.Strafe)

yaw :: Float -> RIO AppGame ()
yaw speed = World.run $
  World.globalSet (World.Yaw speed)

yawStop :: RIO AppGame ()
yawStop = World.run $
  World.globalSet (Apecs.Not @World.Yaw)

pitch :: Float -> RIO AppGame ()
pitch speed = World.run $
  World.globalSet (World.Pitch speed)

pitchStop :: RIO AppGame ()
pitchStop = World.run $
  World.globalSet (Apecs.Not @World.Pitch)

handleTime :: Word64 -> Word64 -> RIO AppGame ()
handleTime prevTime startTime = do
  World.run do
    World.globalModify
      $ (World.timeAbsolute +~ deltaSeconds)
      . (World.timeDelta .~ deltaSeconds)

    handleCameraMotion deltaSeconds do
      Drawable.updateFlashlight
      Drawable.updateBillboards

    handleProjectiles deltaSeconds

  handleSolids deltaSeconds

  where
    deltaSeconds :: Float
    deltaSeconds = fromIntegral (startTime - prevTime) * 1e-9

maybeUpdate
  :: ( Apecs.Get World IO force
     , Apecs.Get World IO object
     , Apecs.Set World IO object
     )
  => (force -> object -> object) -> SystemW Bool
maybeUpdate f = do
  World.globalGet >>= \case
    Nothing ->
      pure False
    Just comp -> do
      World.globalModify (f comp)
      pure True

runUpdaters :: [SystemW Bool] -> SystemW () -> SystemW ()
runUpdaters updaters followup = do
  updated <- sequence updaters
  when (or updated) followup

handleCameraMotion :: Float -> SystemW () -> SystemW ()
handleCameraMotion dt = runUpdaters
  [ maybeUpdate \World.Walk{walkSpeed} camera@World.Camera{..} ->
      let
        dir  = Vec3.normalize (_cameraTarget - _cameraOrigin)
        step = dir Vec3.^* (walkSpeed * dt)
      in
        camera
          & World.cameraOrigin +~ step
          & World.cameraTarget +~ step

  , maybeUpdate \World.Strafe{strafeSpeed} camera@World.Camera{..} ->
      let
        dir  = Vec3.normalize $ Vec3.cross (_cameraTarget - _cameraOrigin) _cameraUp
        step = dir Vec3.^* (strafeSpeed * dt)
      in
        camera
          & World.cameraOrigin +~ step
          & World.cameraTarget +~ step
  , maybeUpdate \World.Fly{flySpeed} camera@World.Camera{..} ->
      let
        dir  = _cameraUp
        step = dir Vec3.^* (flySpeed * dt)
      in
        camera
          & World.cameraOrigin +~ step
          & World.cameraTarget +~ step
  , maybeUpdate \World.Yaw{yawSpeed} World.Camera{..} ->
      let
        dir = _cameraUp
      in
        World.Camera
          { World._cameraTarget =
              Quaternion.rotatePoint
                (Quaternion.axisAngle dir $ yawSpeed * dt)
                _cameraOrigin
                _cameraTarget
          , ..
          }

  , maybeUpdate \World.Pitch{pitchSpeed} World.Camera{..} ->
      let
        dir = Vec3.normalize $ Vec3.cross (_cameraTarget - _cameraOrigin) _cameraUp
      in
        World.Camera
          { World._cameraTarget =
              Quaternion.rotatePoint
                (Quaternion.axisAngle dir $ pitchSpeed * dt)
                _cameraOrigin
                _cameraTarget
          , ..
          }
  ]

handleProjectiles :: Float -> SystemW ()
handleProjectiles dt =
  Apecs.cmap \(World.Projectile{..}, m, World.Position pos, d) ->
    let
      timer' = _projectileTimer - dt
      proj' = World.Projectile{_projectileTimer = timer', ..}
      pos' = World.Position $ pos + _projectileVelocity Vec3.^* dt
    in
      if timer' <= 0 then
        Left $ Not @(World.Projectile, Drawable.DrawableComponents, Draw.Light)
      else
        Right
          ( proj'
          , pos'
          , Drawable.bakeInstance (m, pos',d)
          )

{- | Inflate a Mesh/Instance from SolidRibbon generator.

XXX: Previous Resource is assumed to be collected via Draw.Solid command generator.
-}
handleSolids :: Float -> RIO AppGame ()
handleSolids _dt = do
  World.Time{..} <- World.run World.globalGet
  vd <- view appVulkanDevice
  ribbons <- World.getAll
  for_ ribbons \(Draw.SolidRibbon, ety) -> do
    let vertices = Solids.ribbon 2 32 _timeAbsolute
    (solidRelease, Model{..}) <- allocateAcquire $ Model.acquireIndexed vd vertices
    World.run $
      Apecs.set ety
        ( Draw.Resource solidRelease -- XXX: collect before the next frame, but after rendering
        , Draw.Mesh
            { _meshPositions  = _modelPositions
            , _meshVertices   = _modelVertices
            , _meshIndices    = _modelIndices
            , _meshIndexCount = _modelIndexCount
            }
        , Draw.Instance $
            Transform.rotateY (_timeAbsolute / pi)
        )

dumpVMA :: RIO AppGame ()
dumpVMA = do
  vma <- views appVulkanDevice _vdAllocator

  VMA.Stats{total} <- VMA.calculateStats vma
  logInfo $ displayShow total

  budgets <- VMA.getBudget vma
  logInfo $ displayShow do
    budget <- budgets
    guard $ VMA.blockBytes budget > 0
    pure budget
