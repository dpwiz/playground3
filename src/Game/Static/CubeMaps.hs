{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fforce-recomp #-}

module Game.Static.CubeMaps
  ( module Game.Static.CubeMaps
  ) where

import qualified Resource.TH as TH

TH.filePaths TH.Files "resources/cubemaps"
TH.filePatterns TH.Files "resources/cubemaps"
