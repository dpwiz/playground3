module Game.Static.Materials where

import Import

import Render.Material

import qualified Game.Static.Textures as Textures

-- * Indexed

staticMaterials :: [(Integer, Material FilePath)]
staticMaterials = zip [0..]
  [ box
  , hgj
  , mirror
  , pureWhite
  , vikingRoom
  ]

-- * Materials

box :: Material FilePath
box = blank
  { _diffuse      = Textures.BOX_DIFFUSE_KTX
  , _specular     = Textures.BOX_SPECULAR_KTX
  , _reflectivity = Textures.GREY128_PNG
  }

hgj :: Material FilePath
hgj = blank
  { _diffuse      = Textures.HGJ_KTX
  , _emissive     = Textures.HGJ_KTX
  , _reflectivity = Textures.GREY128_PNG
  }

mirror :: Material FilePath
mirror = pureBlack
  { _reflectivity = Textures.WHITE_PNG
  }

vikingRoom :: Material FilePath
vikingRoom = blank
  { _diffuse = Textures.VIKING_ROOM_KTX
  }

-- * Sprites

cd :: Material FilePath
cd = pureBlack
  { _emissive = Textures.CD_PNG
  }

flare :: Material FilePath
flare = pureBlack
  { _emissive = Textures.FLARE_PNG
  }

-- * Baselines

blank :: Material FilePath
blank = Material
  { _diffuse      = Textures.BLACK_PNG
  , _specular     = Textures.GREY32_PNG
  , _emissive     = Textures.BLACK_PNG
  , _occlusion    = Textures.WHITE_PNG
  , _reflectivity = Textures.BLACK_PNG
  , _normals      = Textures.FLAT_PNG
  }

pureBlack :: Material FilePath
pureBlack = Material
  { _diffuse      = Textures.BLACK_PNG
  , _specular     = Textures.BLACK_PNG
  , _emissive     = Textures.BLACK_PNG
  , _occlusion    = Textures.BLACK_PNG
  , _reflectivity = Textures.BLACK_PNG
  , _normals      = Textures.FLAT_PNG
  }

pureWhite :: Material FilePath
pureWhite = Material
  { _diffuse      = Textures.BLACK_PNG
  , _specular     = Textures.BLACK_PNG
  , _emissive     = Textures.WHITE_PNG
  , _occlusion    = Textures.BLACK_PNG
  , _reflectivity = Textures.BLACK_PNG
  , _normals      = Textures.FLAT_PNG
  }
