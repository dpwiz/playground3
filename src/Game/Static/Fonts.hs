{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fforce-recomp #-}

module Game.Static.Fonts
  ( module Game.Static.Fonts
  ) where

import qualified Resource.TH as TH

TH.filePaths TH.Files "resources/fonts"
TH.filePatterns TH.Files "resources/fonts"
