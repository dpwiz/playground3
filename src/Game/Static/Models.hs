{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fforce-recomp #-}

module Game.Static.Models
  ( module Game.Static.Models
  ) where

import qualified Resource.TH as TH

TH.filePaths TH.Files "resources/models"
TH.filePatterns TH.Files "resources/models"
