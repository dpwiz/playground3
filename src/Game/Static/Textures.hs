{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -fforce-recomp #-}

module Game.Static.Textures
  ( module Game.Static.Textures
  ) where

import qualified Resource.TH as TH

TH.filePaths TH.Files "resources/textures"
TH.filePatterns TH.Files "resources/textures"
