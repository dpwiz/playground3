{-# LANGUAGE TemplateHaskell #-}

module Game.World.Components where

import RIO

import Apecs
import Apecs.Experimental.Reactive
import Control.Lens.TH (makeLenses)
import Geomancy (Vec3, Quaternion, vec3)

data Camera = Camera
  { _cameraOrigin :: Vec3
  , _cameraTarget :: Vec3
  , _cameraUp     :: Vec3
  , _cameraFov    :: Float
  } deriving (Eq, Ord, Show)

instance Semigroup Camera where
  _a <> b = b

instance Monoid Camera where
  mempty = Camera
    { _cameraOrigin = vec3 6 (-4) (-4)
    , _cameraTarget = vec3 0 0 0
    , _cameraUp     = vec3 0 (-1) 0
    , _cameraFov    = 60
    }

instance Component Camera where
  type Storage Camera = Global Camera

-- TODO: extract to generic joint node
data CameraBound = CameraBound
  { _cbDirection   :: Quaternion
  , _cbPosition    :: Vec3
  , _cbOrient      :: Bool
  }
  deriving (Eq, Ord, Show)

instance Component CameraBound where
  type Storage CameraBound = Apecs.Map CameraBound

data Time = Time
  { _timeAbsolute :: Float
  , _timeDelta    :: Float
  } deriving (Eq, Ord, Show)

instance Semigroup Time where
  _a <> b = b

instance Monoid Time where
  mempty = Time
    { _timeAbsolute = 0
    , _timeDelta    = 0
    }

instance Component Time where
  type Storage Time = Global Time

data Toggles = Toggles
  { _toggleSolids    :: Bool
  , _toggleWorld     :: Bool
  , _toggleUI        :: Bool
  , _toggleMouse     :: Bool
  , _togglePause     :: Bool
  , _toggleEnv       :: Bool
  , _toggleHeadlight :: Bool
  , _toggleRoomlight :: Bool
  }

instance Semigroup Toggles where
  _a <> b = b

instance Monoid Toggles where
  mempty = Toggles
    { _toggleSolids    = True
    , _toggleWorld     = True
    , _toggleUI        = True -- False
    , _toggleMouse     = False
    , _togglePause     = False
    , _toggleEnv       = False
    , _toggleHeadlight = True
    , _toggleRoomlight = True
    }

instance Component Toggles where
  type Storage Toggles = Global Toggles

data Group
  = HeadLights
  | RoomLights
  | SpawnedStuff
  deriving (Eq, Ord, Show, Enum, Bounded)

instance Component Group where
  type Storage Group = Reactive (EnumMap Group) (Apecs.Map Group)

newtype Walk = Walk { walkSpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component Walk where
  type Storage Walk = Apecs.Map Walk

newtype Strafe = Strafe { strafeSpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component Strafe where
  type Storage Strafe = Apecs.Map Strafe

newtype Fly = Fly { flySpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component Fly where
  type Storage Fly = Apecs.Map Fly

newtype Yaw = Yaw { yawSpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component Yaw where
  type Storage Yaw = Apecs.Map Yaw

newtype Pitch = Pitch { pitchSpeed :: Float }
  deriving (Eq, Ord, Show)

instance Component Pitch where
  type Storage Pitch = Apecs.Map Pitch

newtype Position = Position { unPosition :: Vec3 }
  deriving (Eq, Ord, Show)

instance Component Position where
  type Storage Position = Apecs.Map Position

newtype Direction = Direction { unDirection :: Quaternion }
  deriving (Eq, Ord, Show)

instance Component Direction where
  type Storage Direction = Apecs.Map Direction

data Projectile = Projectile
  { _projectileTimer    :: Float
  , _projectileVelocity :: Vec3
  } deriving (Eq, Ord, Show)

instance Component Projectile where
  type Storage Projectile = Apecs.Cache 100 (Apecs.Map Projectile)

data Emitter = Emitter
  deriving (Eq, Ord, Show)

instance Component Emitter where
  type Storage Emitter = Apecs.Map Emitter

makeLenses ''Camera
makeLenses ''CameraBound
makeLenses ''Time
makeLenses ''Toggles
