{-# LANGUAGE TemplateHaskell #-}

module Game.World.Types
  ( World
  , initWorld
  , SystemW
  ) where

import RIO

import Apecs

import Game.World.Components
import qualified Game.World.Components.Draw as Draw

makeWorld "World"
  [ ''Camera
  , ''CameraBound
  , ''Time
  , ''Toggles

  , ''Walk
  , ''Strafe
  , ''Fly
  , ''Yaw
  , ''Pitch

  , ''Position
  , ''Direction

  , ''Projectile
  , ''Emitter

  , ''Group

  , ''Draw.Env

  , ''Draw.Mesh
  , ''Draw.Model
  , ''Draw.Instance
  , ''Draw.Material
  , ''Draw.Resource

  , ''Draw.Light
  , ''Draw.ShadowMapped
  , ''Draw.ShadowCaster

  , ''Draw.Billboard
  , ''Draw.SolidRibbon
  ]

type SystemW a = SystemT World IO a
