module Game.World.Systems.Drawable where

import Import hiding (Vec3)

import Data.Hashable (hash)
import Geomancy (Vec3)
import RIO.List.Partial ((!!))

import qualified Apecs
import qualified Geomancy.Quaternion as Quaternion
import qualified Geomancy.Vec3 as Vec3
import qualified Geomancy.Transform as Transform
import qualified RIO.Map as Map

import qualified Game.Static.Materials as Materials
import qualified Game.World as World
import qualified Game.World.Components.Draw as Draw
import qualified Render.Material as Render (Material(..))

spawnSample1 :: (HasStateRef GameState env, World.Run (RIO env)) => RIO env ()
spawnSample1 = do
  World.Camera{..} <- World.globalGet

  static <- spawnMesh gsCube Materials.box

  -- TODO: add selectors:

  -- static <- spawnMesh gsCube Materials.brickWall
  --   gsCube
  --   Textures.BRICKWALL_JPG
  --   Nothing
  --   Nothing
  --   Nothing
  --   (Just Textures.GREY128_PNG)
  --   (Just Textures.BRICKWALL_NORMAL_JPG)

  -- static <- spawnMesh gsCube Materials.dpWiz
  --   gsCube
  --   Textures.DPWIZ_JPG
  --   Nothing
  --   Nothing
  --   Nothing
  --   (Just Textures.GREY128_PNG)
  --   (Just Textures.DPWIZ_NORMALS_PNG)

  bills <- World.getAll @Draw.Billboard
  dynamic <- spawnBillboard
    ( [ Materials.cd
      , Materials.flare
      ] !! (length bills `mod` 2)
    )
    _cameraTarget

  World.run do
    Apecs.set static
      ( World.SpawnedStuff
      , World.Position _cameraTarget
      , Draw.ShadowCaster
      )
    Apecs.modify static $
      Draw.modelTransform <>~ Transform.translate 0 0.5 0 <> Transform.scale 2
    Apecs.modify static bakeInstance

    Apecs.set dynamic World.SpawnedStuff
    Apecs.modify dynamic $
      Draw.modelTransform <>~ Transform.translate 0 (-0.5) 0
    Apecs.modify dynamic bakeInstance

spawnSample2 :: (HasStateRef GameState env, World.Run (RIO env)) => RIO env ()
spawnSample2 = do
  World.Camera{..} <- World.globalGet

  numLights <- fmap length . World.run $ World.getAll @Draw.Light

  when (numLights < MAX_LIGHTS) do
    World.run World.getAll >>= traverse_ \(World.CameraBound{}, World.Emitter, pos, dir) -> do
      projectile <- spawnMesh gsSphere Materials.pureWhite

      World.run do
        let World.Direction towards = dir
        Apecs.set projectile
          ( pos :: World.Position
          , dir
          , World.Projectile
              { _projectileTimer    = 5.0
              , _projectileVelocity = Quaternion.rotate towards (vec3 0 0 10)
              }
          , Draw.Light
              { _lightColor       = vec4 1 1 1 10
              , _lightAttenuation = 2
              , _lightAmbient     = 0
              , _lightCutoff      = cos $ V2 pi pi
              }
          )
        Apecs.modify projectile $
          Draw.modelTransform <>~ Transform.scale 0.2
        Apecs.modify projectile bakeInstance

setEnvCube
  :: (HasStateRef GameState env, World.Run (RIO env))
  => FilePath -> RIO env ()
setEnvCube cubeKey =
  uses gsCubeIds (Map.lookup cubeKey) >>= \case
    Nothing ->
      error "static resource got no stuff"
    Just cid ->
      World.globalModify $ Draw.envCube .~ cid

getMaterial
  :: HasStateRef GameState env
  => Render.Material FilePath
  -> RIO env Draw.Material
getMaterial matPaths = do
  ids <- for matPaths \key ->
    uses gsTextureIds (Map.lookup key) >>= \case
      Nothing ->
        error "static resource got no stuff"
      Just tid ->
        pure $ fromIntegral tid

  pure Draw.Material
    { _materialId     = fromIntegral $ hash ids
    , _materialParams = fmap (TextureCombo 0) ids
    }

spawnMesh
  :: (HasStateRef GameState env, World.Run (RIO env))
  => Lens' GameState Import.Model
  -> Render.Material FilePath
  -> RIO env Entity
spawnMesh getModel matPaths = do
  Import.Model{..} <- use getModel

  material <- getMaterial matPaths

  World.run do
    e <- Apecs.newEntity
      ( Draw.Mesh
          { _meshPositions  = _modelPositions
          , _meshVertices   = _modelVertices
          , _meshIndices    = _modelIndices
          , _meshIndexCount = _modelIndexCount
          }
      , Draw.Model
          { _modelTransform = _modelTransform
          }
      , material
      , World.Position 0
      , World.Direction (quaternion 1 0 0 0)
      )

    Apecs.modify e bakeInstance

    pure e

spawnBillboard
  :: (HasStateRef GameState env, World.Run (RIO env))
  => Render.Material FilePath -> Vec3 -> RIO env Entity
spawnBillboard matPaths pos = do
  World.Camera{..} <- World.globalGet

  material <- getMaterial matPaths

  World.run do
    let
      mpd =
        ( Draw.Model
            { _modelTransform = mempty
            }
        , World.Position pos
        ,  World.Direction $ Quaternion.lookAtUp _cameraOrigin pos _cameraUp
        )
      distVec = pos - _cameraOrigin
    Apecs.newEntity
      ( Draw.Billboard $ Vec3.dot distVec distVec
      , mpd
      , material
      , bakeInstance mpd
      )

bakeInstance :: (Draw.Model, World.Position, World.Direction) -> Draw.Instance
bakeInstance (Draw.Model{_modelTransform=base}, World.Position pos, World.Direction dir) =
  Draw.Instance
    { _instanceTransform =
        base <> Transform.dirPos dir pos
        -- base <> Transform.rotateQ dir <> Transform.translateV pos
        -- Transform.translateV pos <> Transform.rotateQ dir <> base
    }

wipeSpawned :: RIO AppGame ()
wipeSpawned = World.run do
  stuff <- World.getGroup World.SpawnedStuff
  for_ stuff \spawned ->
    Apecs.destroy spawned $
      Proxy @(DrawableComponents, World.Group, Draw.Billboard)

-- XXX: billboards etc. not included
type DrawableComponents = (Draw.Mesh, Draw.Model, Draw.Instance, Draw.Material)

updateBillboards :: SystemW ()
updateBillboards = do
  World.Camera{..} <- World.globalGet

  Apecs.cmapM \(Draw.Billboard{}, (model, World.Position pos), entity) -> do
    let distVec = pos - _cameraOrigin
    let dir' = World.Direction $ Quaternion.lookAtUp _cameraOrigin pos _cameraUp

    -- TODO: try reactive store
    Apecs.set entity
      ( Draw.Billboard $ Vec3.dot distVec distVec
      , dir'
      , bakeInstance (model, World.Position pos, dir')
      )

updateFlashlight :: SystemW ()
updateFlashlight = do
  World.Camera{..} <- World.globalGet
  World.run $ Apecs.cmap \(World.CameraBound{..}, mInstance) ->
    let
      combined =
        if _cbOrient then
          cameraDirection * _cbDirection
        else
          _cbDirection
        where
          cameraDirection =
            Quaternion.lookAtUp _cameraOrigin _cameraTarget _cameraUp

      position =
        World.Position $
          _cameraOrigin +
          Quaternion.rotatePoint combined 0 _cbPosition

      direction =
        World.Direction combined
    in
      ( position
      , direction
      , case mInstance of
          Nothing ->
            Nothing
          Just (model, Draw.Instance{}) ->
            Just $ bakeInstance (model, position, direction)
      )
