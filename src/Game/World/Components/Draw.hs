{-# LANGUAGE TemplateHaskell #-}

module Game.World.Components.Draw where

import RIO

import Apecs
import Control.Lens.TH (makeLenses)
import Control.Monad.Trans.Resource (ReleaseKey)
import Geomancy (Vec4)
import Geomancy.Transform (Transform)
import Linear (V2)

import qualified Vulkan.Core10 as Vk

import Geometry.Types (TextureCombo)

import qualified Render.Material as Render

data Env = Env
  { _envCube :: Word32
  }
  deriving (Eq, Ord, Show)

instance Semigroup Env where
  _a <> b = b

instance Monoid Env where
  mempty = Env
    { _envCube = 0
    }

instance Component Env where
  type Storage Env = Apecs.Global Env

-- | Currently a clone of Types.Model sans transform.
data Mesh = Mesh
  { _meshPositions  :: Vk.Buffer
  , _meshVertices   :: Vk.Buffer
  , _meshIndices    :: Vk.Buffer
  , _meshIndexCount :: Word32
  } deriving (Eq, Ord, Show)

instance Component Mesh where
  type Storage Mesh = Apecs.Map Mesh

-- | Base mesh transform to pile position and direction upon.
data Model = Model
  { _modelTransform :: Transform
  -- TODO: _modelInverse   :: InvTransform
  } deriving (Show)

instance Component Model where
  type Storage Model = Apecs.Map Model

-- | Baked instance attributes. Do not recalculate that's not changed.
-- TODO: try reactive stores on Model+Position+Direction
data Instance = Instance
  { _instanceTransform :: Transform -- ^ 'Model' + 'Position' + 'Direction'
  -- TODO: _instanceInverse   :: InvTransform
  } deriving (Show)

instance Component Instance where
  type Storage Instance = Apecs.Map Instance

-- | Currently a clone of Simple.PushFragment with extra index for sorting/grouping
data Material = Material
  { _materialId     :: Word32
  , _materialParams :: Render.Material TextureCombo
  } deriving (Show)

instance Eq Material where
  a == b = _materialId a == _materialId b

instance Ord Material where
  compare a b = _materialId a `compare` _materialId b

instance Component Material where
  type Storage Material = Apecs.Map Material

data Light = Light
  { _lightColor       :: Vec4
  , _lightAttenuation :: Float -- ^ Stored with "Position"
  , _lightAmbient     :: Float -- ^ Stored with "Direction"
  , _lightCutoff      :: V2 Float
  } deriving (Eq, Ord, Show)

instance Component Light where
  type Storage Light = Apecs.Map Light

-- TODO: add shadowmap params like atlas extent
data ShadowMapped = ShadowMapped
  deriving (Eq, Ord, Show)

instance Component ShadowMapped where
  type Storage ShadowMapped = Apecs.Unique ShadowMapped

data ShadowCaster = ShadowCaster
  deriving (Eq, Ord, Show)

instance Component ShadowCaster where
  type Storage ShadowCaster = Apecs.Map ShadowCaster

-- | If the mesh was generated dynamically (e.g. Draw.Solids, texts) - use this to relesase buffers.
data Resource = Resource
  { _resourceKey :: ReleaseKey
  }

-- TODO: try reactive stores on Mesh
instance Component Resource where
  type Storage Resource = Apecs.Map Resource

-- | The entity is always facing the camera
newtype Billboard = Billboard Float
  deriving (Eq, Ord, Show)

instance Component Billboard where
  type Storage Billboard = Apecs.Map Billboard

-- | Demonstration component on how to generate and release a Mesh at runtime.
data SolidRibbon = SolidRibbon
  deriving (Eq, Ord, Show)

instance Component SolidRibbon where
  type Storage SolidRibbon = Apecs.Map SolidRibbon

makeLenses ''Env

makeLenses ''Mesh
makeLenses ''Model
makeLenses ''Instance
makeLenses ''Material
makeLenses ''Resource

makeLenses ''Light

makeLenses ''Billboard
makeLenses ''SolidRibbon
