{-# LANGUAGE OverloadedLists #-}

module Game.Draw.Solids where

import Import

import qualified Linear
import qualified RIO.List as List
import qualified Vulkan.Core10 as Vk

import Render.Draw (Draw(..))

import qualified Game.World as World
import qualified Game.World.Components.Draw as Draw
import qualified Geometry.Solids as Solids
import qualified Render.Setup.Context as Context
import qualified Render.Solid.Context as Solid
import qualified Render.Solid.Vertex as Solid

rgbPyramid :: Float -> Float -> Indexed Solid.Vertex
rgbPyramid size seconds = Indexed colored indices
  where
    Indexed vertices indices = Solids.tetrahedron size

    colored = zipWith Solid.Vertex vertices colors

    colors =
      [ V4 a a a 1
      , V4 1 0 0 1
      , V4 0 1 0 1
      , V4 0 0 1 1
      ]
    a = 0.5 + 0.5 * sin seconds

ribbon :: Int -> Int -> Float -> Indexed Solid.Vertex
ribbon turns size seconds = Indexed items (indices <> reverse indices)
  where
    items = do
      (ix, (color, pos), (_nextColor, nextPos)) <- List.zip3 [0..] path (drop 1 path)
      let axis = pos - nextPos
      let q = Linear.axisAngle axis (pi * ix / fromIntegral size / fromIntegral turns)

      id
        [ Solid.Vertex (pos + Linear.rotate q (V3 (-0.25) 0 0)) color
        , Solid.Vertex (pos + Linear.rotate q (V3 ( 0.25) 0 0)) color
        ]

    path = do
      ix <- [0 :: Int .. turns * size + 1]
      let
        x = fromIntegral turns * pi * fromIntegral ix / fromIntegral size
        fx = sin (x + 0.1 * seconds)
        gx = cos (x + 0.1 * seconds)

        color = V4 (0.5 + 0.5 * fx) (0.5 + 0.5 * gx) 0.5 1.0

      pure (color, V3 ( (x - 6) / fromIntegral turns) gx fx)

    indices = concatMap @[] strip [0 .. len - 2]
      where
        len = fromIntegral $ length items `div` 2

        strip i = map (+ 2*i)
          [ 0, 1, 3
          , 0, 3, 2
          ]

makeScene :: RIO AppGame Draw
makeScene = do
  -- XXX: Fetch game resources
  GameState{..} <- use id
  GameContext{..} <- view appContext

  let PipelineContext{..} = _gcSolid
  let RenderContext{..} = _gcSolidRender

  sv <- World.sceneView3d
  Context.setScene sv zero _gcSolidRender

  solids <- World.getAll
  let (resources, meshes) = List.unzip solids

  pure $ DrawCommands (map Draw._resourceKey resources) \cmd -> do
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcPipeline
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS _pcLayout 0 _rcDescSets mempty

    for_ meshes \(Draw.SolidRibbon, Draw.Mesh{..}, Draw.Instance{..}) -> do
      Vk.cmdBindVertexBuffers cmd 0 [_meshPositions, _meshVertices] [0, 0]
      Vk.cmdBindIndexBuffer cmd _meshIndices 0 Vk.INDEX_TYPE_UINT32

      Solid.pushVertex cmd _pcLayout _instanceTransform

      let
        instanceCount = 1
        firstIndex    = 0
        vertexOffset  = 0
        firstInstance = 0
      Vk.cmdDrawIndexed cmd _meshIndexCount instanceCount firstIndex vertexOffset firstInstance

release :: RIO AppGame Draw
release = do
  solids <- World.getAll @(Draw.SolidRibbon, Draw.Resource)
  let
    transient = do
      (Draw.SolidRibbon, Draw.Resource releaseKey) <- solids
      pure releaseKey

  pure $ DrawCommands transient (const mempty)
