{-# LANGUAGE OverloadedLists #-}

module Game.Draw.UI where

import Import

import Data.Acquire (Acquire, allocateAcquire)
import Geomancy.Transform (Transform)

import qualified Geomancy.Transform as Transform
import qualified RIO.Map as Map
import qualified RIO.Text as Text
import qualified Vulkan.Core10 as Vk

import Render.Draw (Draw(..))

import qualified Game.Static.Textures as Textures
import qualified Game.World as World
import qualified Render.Model as Model
import qualified Render.Setup.Context as Context
import qualified Render.SDF.Context as SDF
import qualified Render.SDF.Vertex as SDF
import qualified Render.Sprite.Context as Sprite
import qualified Resource.Font.Evanw as FontB
import qualified Resource.Font.Proxima as FontS
import qualified Geometry.Layout2D as L

makeUI :: RIO AppGame Draw
makeUI = do
  -- XXX: Fetch game resources
  GameContext{..} <- view appContext
  GameState{..} <- use id

  sv <- World.sceneViewUi
  Context.setScene sv zero _gcSpriteRender
  Context.setScene sv zero _gcSdfRender

  World.Time{_timeAbsolute=seconds} <- World.run World.globalGet

  let
    sprite key = case Map.lookup key _gsTextureIds of
      Nothing ->
        error "assert: all static textures are indexed"
      Just ix ->
        Bitmap
          { _tBase      = 1
          , _tOffset    = 0
          , _tScale     = 1
          , _tSamplerId = 0
          , _tImageId   = fromIntegral ix
          }

  extent <- view (appContext . gcVulkan . vcExtent)
  let
    fullscreen = L.screen extent

    layout =
      L.pad fullscreen 64 \padded ->
          L.split2Vat padded (Right 0.9) \_pad bottom -> concat @[]
            [ L.splitN bottom L.split2Hat $
              replicate
                (round $ 4 + 4 * sin seconds)
                ( \c ->
                  (L.fitSized c 512, sprite Textures.HGJ_KTX)
                )
            , FontB.putChars bottom (Middle, Middle) 24 _gsFontB (sprite Textures.UBUNTU_24_W_PNG) $
                "middle-aligned 24-sized bitmap font from " <> Textures.UBUNTU_24_W_PNG
            ]

  let
    transcluent = take 5 do
      x <- [-2 .. 2]
      y <- [-1, 0, 1]
      let w = 2 * 64
      let h = 2 * 64
      pure
        ( mconcat
            [ Transform.scaleXY w h
            , Transform.translate (w * x * 1.1) (h * y * 1.1) 0
            , Transform.rotateZ seconds
            ]
        , sprite Textures.CD_PNG
            & tBase .~ vec4
                (1.0 + cos seconds)
                (1.0 + sin seconds)
                (1.0 + sin (seconds * pi))
                1.0
        )

    coordinated = take 2 do
      i <- [1..]
      let tscale = (1.0 + 0.5 * sin seconds)
      pure
        ( mappend
            (Transform.translate i i 0)
            (Transform.scaleXY 128 128)
        , sprite Textures.DAVID_KTX
            & tBase   .~ vec4 1.0 1.0 1.0 (0.5 + 0.5 * cos seconds)
            & tOffset .~ 0.5
            & tScale  .~ V2 tscale tscale
        )

    animation = do
      ix <- [1 .. 10]
      let x = 10 / 2 - ix
      let y = sin $ seconds + ix / 10 * pi
      (size, phase) <- [(32, ix), (32, ix)]
      let frame = fromIntegral @Int $ truncate (seconds * 8 * 2 + phase)
      pure
        ( Transform.translate x y 0 <> Transform.scaleXY size size
        , sprite Textures.EXPLOSION_1_PNG
            & tOffset .~ V2 (frame / 8) 0
            & tScale  .~ V2 (1/8) 1
        )

    flipflops = id do
      x <- [-2, 2]
      y <- [-2, 2]
      let w = 96
      let h = 64
      pure
        ( mconcat
            [ Transform.scaleXY w h
            , Transform.rotateZ $ sin seconds
            , Transform.translate (w * x * 1.1) (h * y * 1.1) 0
            ]
        , sprite $
            if truncate seconds `mod` 2 == (0 :: Int) then
              Textures.FASHION_KTX
            else
              Textures.DAVID_KTX
        )

  let
    groups =
      [ layout
      , flipflops
      , transcluent
      , coordinated
      , animation
      ]

  vd <- view appVulkanDevice

  -- (b0, block0) <- allocateAcquire $ textBlock vd _gsFontS [[ident_ "яж цß"]]
  -- let transient = [b0]
  -- let blocks = [(block0, Transform.scale3 1 1 1)]

  (b1, block1) <- allocateAcquire $ textBlock vd _gsFontS
    [ [ syntax_ "let ", ident_ "seconds", syntax_ " = "
      , let
          t = tshow @Int (round seconds)
          ch = num_ t
          s = sqrt $ fract seconds
        in
          ch
            { chunkTransform = chunkTransform ch
                <> Transform.scale3 1 s 1
            }
      , syntax_ " :: ", type_ "MS "
      , let
          ch = comment_ "-- ЪуЪ"
          wiggle = 0.5 * sin (2 * pi * seconds) + 0.5 * cos (3 * pi * seconds)
        in
          ch
            { chunkTransform = Transform.rotateZ (wiggle / tau) <> chunkTransform ch
            }
      ]
    , [ syntax_ "in "
      , ident_ "show ", ident_ "seconds"
      ]
    ]

  (b2, block2) <- allocateAcquire $ textBlock vd _gsFontS
    [ [ ident_ "main", syntax_ " :: ", type_ "IO ()" ]
    , [ ident_ "main", syntax_ " = ", ident_ "putStrLn", str_ " \"Zhopa кита.\"" ]
    ]

  let
    size16 = Transform.scaleXY (16/64) (16/64)
    size32 = Transform.scaleXY (32/64) (32/64)
    size64 = mempty

    blocks :: [([Chunk Model], Transform)]
    blocks =
      [ ( block1
        , size16 <>
          Transform.translate (50 + 64 * sin seconds) 100 0.25
        )
      , ( block1
        , size32 <>
          Transform.translate 0 (-100) 0.25
        )
      , ( block2
        , Transform.rotateZ (-pi/2) <>
          size64 <>
          Transform.translate (-500) (400 - fract (seconds / 15) * 1400) 0.25
        )
      ]
  let transient = [b1, b2]

  pure $ DrawCommands transient \cmd -> do
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSprite)
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSprite) 0 (_rcDescSets _gcSpriteRender) mempty

    -- TODO: Set up separate sprite rendering pass and pipeline
    Vk.cmdBindVertexBuffers cmd 0 [_modelPositions _gsSprite, _modelVertices _gsSprite] [0, 0]
    Vk.cmdBindIndexBuffer cmd (_modelIndices _gsSprite) 0 Vk.INDEX_TYPE_UINT32

    for_ (zip [0.5, 0.5 - 0.0001 ..] groups) \(layer, groupItems) -> do
      for_ groupItems \(vert, frag) -> do
        Sprite.pushVertex cmd (_pcLayout _gcSprite) (Transform.translate 0 0 layer <> vert)
        Sprite.pushFragment cmd (_pcLayout _gcSprite) $ Sprite.FragmentPush frag

        let
          instanceCount = 1
          firstIndex    = 0
          vertexOffset  = 0
          firstInstance = 0
        Vk.cmdDrawIndexed cmd (_modelIndexCount _gsSprite) instanceCount firstIndex vertexOffset firstInstance

    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSdf)
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSdf) 0 (_rcDescSets _gcSdfRender) mempty
    for_ blocks $ \(blockChunks, blockTransform) ->
      for_ blockChunks \Chunk{chunkData=Model{..}, ..} -> do
        SDF.pushVertex cmd (_pcLayout _gcSdf) $
          chunkTransform <> blockTransform
        SDF.pushFragment cmd (_pcLayout _gcSdf) . SDF.FragmentPush $
          sprite Textures.ROBOTO_REGULAR_PNG
            & tSamplerId .~ 5 -- XXX : NEAREST/0/CLAMP_BORDER
            & chunkStyle

        Vk.cmdBindVertexBuffers cmd 0 [_modelPositions, _modelVertices] [0, 0]
        Vk.cmdBindIndexBuffer cmd _modelIndices 0 Vk.INDEX_TYPE_UINT32
        Vk.cmdDrawIndexed cmd _modelIndexCount 1 0 0 0

data Chunk a = Chunk
  { chunkData      :: a
  , chunkTransform :: Transform
  , chunkStyle     :: Bitmap -> Bitmap
  }

styled_ :: (Bitmap -> Bitmap) -> a -> Chunk a
styled_ style x = Chunk
  { chunkData      = x
  , chunkTransform = mempty
  , chunkStyle     = style
  }

syntax_ :: a -> Chunk a
syntax_ = styled_ $ tBase .~ vec4 0.5 0.5 0.5 1

ident_ :: a -> Chunk a
ident_ = styled_ $ tBase .~ vec4 1 1 1 1

type_ :: a -> Chunk a
type_ = styled_ $ tBase .~ vec4 0.5 0.5 1 1

comment_ :: a -> Chunk a
comment_ = styled_ $ tBase .~ vec4 1 0.5 0.5 1

num_ :: a -> Chunk a
num_ = styled_ $ tBase .~ vec4 1 0.5 1 1

str_ :: a -> Chunk a
str_ = styled_ $ tBase .~ vec4 0.5 1 0.5 1

textBlock :: VulkanDevice -> FontS.Container -> [[Chunk Text]] -> Acquire [Chunk Model]
textBlock vd font = fmap fst . foldl' foldLine (pure (mempty, 0))
  where
    foldLine before chunks = do
      (previous, offsetY) <- before
      (models, _width) <- foldl' (foldChunks offsetY) (pure (mempty, 0)) chunks
      pure
        ( models <> previous
        -- , offsetY - FontS.size font * 1.1
        , offsetY - 64 * 1.2
        )

    foldChunks offsetY before Chunk{..} = do
      (previous, offsetX) <- before
      (model, advanceX) <- textProxima vd font chunkData
      pure
        ( Chunk
            { chunkData      = model
            , chunkTransform = chunkTransform <> Transform.translate offsetX offsetY 0
            , chunkStyle
            } : previous
        , offsetX + advanceX
        )

textProxima :: VulkanDevice -> FontS.Container -> Text -> Acquire (Model, Float)
textProxima vd font line =
  (,)
    <$> Model.acquireIndexed vd indexed
    <*> pure (sumAdvance $ Text.length line)
  where
    indexed = mconcat do
      (ix, Right (_glyphAdvance, baseVertices)) <- zip [0..] glyphs
      let
        baseX = sumAdvance ix

        vertices = do
            V4 x y s t <- baseVertices
            pure SDF.Vertex
              { vPos      = V2 (baseX + x) y
              , vTexCoord = V2 s t
              }
      pure $ Indexed vertices FontS.indices

    glyphs = FontS.toGlyphs (Just ' ') font line

    sumAdvance ix =
      sum $ map (either id fst) $ take ix glyphs
