module Game.Draw.Shadow where

import Import

import Control.Monad.Trans.Resource (ReleaseKey)
import Data.Acquire (allocateAcquire, mkAcquire)
import Geomancy (Transform)
import Geomancy.Vulkan.Projection (infinitePerspective)
import Geomancy.Vulkan.View (lookAt)

import qualified Foreign
import qualified Geomancy.Quaternion as Quaternion
import qualified RIO.List as List
import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Game.World.Components.Draw
import Render.Draw (Draw(..))

import qualified Game.World as World
import qualified Render.Shadow.Context as Shadow

makeScene :: RIO AppGame Draw
makeScene = do
  World.getAll >>= \case
    [] ->
      pure mempty
    (ShadowMapped, light) : _unique -> do
      makeShadows light

makeShadows :: (Light, World.Position, World.Direction) -> RIO AppGame Draw
makeShadows (Light{..}, World.Position origin, World.Direction dir) = do
  GameState{..} <- use id
  GameContext{..} <- view appContext

  Vk.Extent2D{width, height} <- view (appContext . gcVulkan . vcShadowExtent)
  let
    target = origin + Quaternion.rotate dir (vec3 0 0 1)

    V2 _inner outerCos  = _lightCutoff
    fovRads = 2 * acos outerCos
    -- near = 0
    -- far = 64 -- TODO: get from _lightAttenuation and use clipped perspective

    projection = infinitePerspective fovRads width height
    view_      = lookAt origin target (vec3 0 (-1) 0)

  let
    sv = SceneView
      { _svProjection  = projection
      , _svView        = view_
      , _svViewPosTime = 0
      , _svDirection   = 0
      , _svNumLights   = 0
      , _svEnvCube     = 0
      }
  Shadow.setScene sv zero _gcShadowRender

  vma <- view (appVulkanDevice . vdAllocator)

  meshes <- World.getAll @(Mesh, (Instance, ShadowCaster))
  let
    meshGroups = do
      group@((mesh, _perInstance) : _rest) <- List.groupBy ((==) `on` fst) $ List.sortOn fst meshes
      pure (mesh, map snd group)
  allocatedMeshes <- for meshGroups \(mesh, group) ->
    fmap (mesh,) $
      allocateInstances
        vma
        (List.groupBy ((==) `on` fst))
        (List.sortOn snd group)
  let
    (transient, groups') = List.unzip do
      (model, (resources, items)) <- allocatedMeshes
      pure (resources, (model, items))

  -- billboards <- World.getAll @(Billboard, (Instance, Material))
  -- (bbRelease, (bbInstances, bbMaterials)) <- case billboards of
  --   [] ->
  --     allocateAcquire $
  --       pure (error "no buffer allocated for empty billboards", mempty)
  --   _some ->
  --     allocateInstances
  --       vma
  --       (map pure)
  --       (map snd $ List.sortBy (comparing $ Down . fst) billboards)

  -- TODO: make DrawCommands a monoid and combine separately-composed meshes and optional billboards
  pure $ DrawCommands transient \cmd -> do
    -- XXX: draw opaque meshes first
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcShadow)
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcShadow) 0 (_rcDescSets _gcShadowRender) mempty

    for_ groups' \(Mesh{..}, (perInstance, perDraw)) -> do
      Vk.cmdBindVertexBuffers cmd 0
        (Vector.fromList [_meshPositions, perInstance])
        (Vector.fromList [0, 0])
      Vk.cmdBindIndexBuffer cmd _meshIndices 0 Vk.INDEX_TYPE_UINT32

      for_ perDraw \(firstInstance, instanceCount, _material) -> do
        let
          firstIndex   = 0
          vertexOffset = 0
        Vk.cmdDrawIndexed cmd _meshIndexCount instanceCount firstIndex vertexOffset firstInstance

allocateInstances
  :: VMA.Allocator
  -> ([(material, Word32)] -> [[(material, Word32)]])
  -> [(Instance, material)]
  -> RIO AppGame (ReleaseKey, (Vk.Buffer, [(Word32, Word32, material)]))
allocateInstances vma batch group = do
  let
    numInstances = length group

    (perInstance, indexedDraws) = List.unzip do
      (ix, (instanced, material)) <- zip [0..] group
      pure
        ( instanced
        , (material, ix)
        )

    perDraw = do
      drawBatch@((material, firstInstance) : _rest) <- batch indexedDraws
      pure
        ( firstInstance
        , fromIntegral $ length drawBatch
        , material
        )

  (release, mappedBuf) <- allocateAcquire do
    let
      ci = zero
        { Vk.size        = fromIntegral $ numInstances * Foreign.sizeOf @Transform mempty
        , Vk.usage       = Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
        , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
        }

      ai = zero
        { VMA.flags =
            VMA.ALLOCATION_CREATE_MAPPED_BIT
        , VMA.usage =
            VMA.MEMORY_USAGE_GPU_ONLY
        , VMA.requiredFlags =
            Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
            Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT -- TODO: use aggregated flush
        }

    (buf, _allocation, VMA.AllocationInfo{mappedData}) <- VMA.withBuffer vma ci ai mkAcquire
    liftIO . Foreign.pokeArray (Foreign.castPtr mappedData) $
      map _instanceTransform perInstance

    pure buf

  pure
    ( release
    , ( mappedBuf
      , perDraw
      )
    )
