module Game.Draw.World where

import Import

import Control.Monad.Trans.Resource (ReleaseKey)
import Data.Acquire (allocateAcquire, mkAcquire)
import Geomancy (Transform)

import qualified Foreign
import qualified RIO.List as List
import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Render.Draw (Draw(..))
import Game.World.Components.Draw

import qualified Game.World as World
import qualified Render.Setup.Context as Context
import qualified Render.Simple.Context as Simple

makeScene :: RIO AppGame Draw
makeScene = do
  GameState{..} <- use id
  GameContext{..} <- view appContext

  sv <- World.sceneView3d
  lights@(SceneLights sl) <- World.sceneLights3d
  Context.setScene
    sv
      { _svNumLights =
          min MAX_LIGHTS $ fromIntegral $ Vector.length sl
      }
    lights
    _gcSimpleRender

  vma <- view (appVulkanDevice . vdAllocator)

  meshes <- World.getAll @(Mesh, (Instance, Material))
  let
    meshGroups = do
      group@((mesh, _perInstance) : _rest) <- List.groupBy ((==) `on` fst) $ List.sortOn fst meshes
      pure (mesh, map snd group)
  allocatedMeshes <- for meshGroups \(mesh, group) ->
    fmap (mesh,) $
      allocateInstances
        vma
        (List.groupBy ((==) `on` fst))
        (List.sortOn snd group)
  let
    (transient, groups') = List.unzip do
      (model, (resources, items)) <- allocatedMeshes
      pure (resources, (model, items))

  billboards <- World.getAll @(Billboard, (Instance, Material))
  (bbRelease, (bbInstances, bbMaterials)) <- case billboards of
    [] ->
      allocateAcquire $
        pure (error "no buffer allocated for empty billboards", mempty)
    _some ->
      allocateInstances
        vma
        (map pure)
        (map snd $ List.sortBy (comparing $ Down . fst) billboards)

  -- TODO: make DrawCommands a monoid and combine separately-composed meshes and optional billboards
  pure $ DrawCommands (bbRelease : transient) \cmd -> do
    -- XXX: draw opaque meshes first
    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSimple)
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSimple) 0 (_rcDescSets _gcSimpleRender) mempty

    for_ groups' \(Mesh{..}, (perInstance, perDraw)) -> do
      Vk.cmdBindVertexBuffers cmd 0
        (Vector.fromList [_meshPositions, _meshVertices, perInstance])
        (Vector.fromList [0, 0, 0])
      Vk.cmdBindIndexBuffer cmd _meshIndices 0 Vk.INDEX_TYPE_UINT32

      for_ perDraw \(firstInstance, instanceCount, Material{_materialParams}) -> do
        -- Simple.pushVertex cmd _pcLayout mempty
        Simple.pushFragment cmd (_pcLayout _gcSimple) $ Simple.FragmentPush _materialParams

        let
          firstIndex   = 0
          vertexOffset = 0
        Vk.cmdDrawIndexed cmd _meshIndexCount instanceCount firstIndex vertexOffset firstInstance

    Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSkybox)
    -- XXX: temporarily simplified skybox layout
    Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSkybox) 0 (_rcDescSets _gcSimpleRender) mempty
    -- XXX: ignore bound vertex buffer and draw shader-embedded triangle
    Vk.cmdDraw cmd 3 1 0 0

    unless (null billboards) do
      Vk.cmdBindPipeline cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcPipeline _gcSimpleBlend)
      Vk.cmdBindDescriptorSets cmd Vk.PIPELINE_BIND_POINT_GRAPHICS (_pcLayout _gcSimple) 0 (_rcDescSets _gcSimpleRender) mempty

      Vk.cmdBindVertexBuffers cmd 0
        (Vector.fromList [_modelPositions _gsSquare, _modelVertices _gsSquare, bbInstances])
        (Vector.fromList [0, 0, 0])
      Vk.cmdBindIndexBuffer cmd (_modelIndices _gsSquare) 0 Vk.INDEX_TYPE_UINT32
      for_ bbMaterials \(firstInstance, instanceCount, Material{_materialParams}) -> do
        Simple.pushFragment cmd (_pcLayout _gcSimpleBlend) $ Simple.FragmentPush _materialParams
        let
          firstIndex   = 0
          vertexOffset = 0
        Vk.cmdDrawIndexed cmd (_modelIndexCount _gsSquare) instanceCount firstIndex vertexOffset firstInstance

allocateInstances
  :: VMA.Allocator
  -> ([(material, Word32)] -> [[(material, Word32)]])
  -> [(Instance, material)]
  -> RIO AppGame (ReleaseKey, (Vk.Buffer, [(Word32, Word32, material)]))
allocateInstances vma batch group = do
  let
    numInstances = length group

    (perInstance, indexedDraws) = List.unzip do
      (ix, (instanced, material)) <- zip [0..] group
      pure
        ( instanced
        , (material, ix)
        )

    perDraw = do
      drawBatch@((material, firstInstance) : _rest) <- batch indexedDraws
      pure
        ( firstInstance
        , fromIntegral $ length drawBatch
        , material
        )

  (release, mappedBuf) <- allocateAcquire do
    let
      ci = zero
        { Vk.size        = fromIntegral $ numInstances * Foreign.sizeOf @Transform mempty
        , Vk.usage       = Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
        , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
        }

      ai = zero
        { VMA.flags =
            VMA.ALLOCATION_CREATE_MAPPED_BIT
        , VMA.usage =
            VMA.MEMORY_USAGE_GPU_ONLY
        , VMA.requiredFlags =
            Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
            Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT -- TODO: use aggregated flush
        }

    (buf, _allocation, VMA.AllocationInfo{mappedData}) <- VMA.withBuffer vma ci ai mkAcquire
    liftIO . Foreign.pokeArray (Foreign.castPtr mappedData) $
      map _instanceTransform perInstance

    pure buf

  pure
    ( release
    , ( mappedBuf
      , perDraw
      )
    )
