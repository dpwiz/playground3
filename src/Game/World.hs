{-# LANGUAGE AllowAmbiguousTypes #-}

module Game.World
  ( setup
  , Run(..)
  , globalGet
  , globalSet
  , globalModify
  , getGroup
  , getAll
  , wipeAll
  , module World
  , module Apecs
  , sceneViewUi
  , sceneView3d
  , sceneLights3d
  ) where

import Import hiding (set)

import Apecs (global, get, set, modify, destroy, ($=), ($~))
import Geomancy.Vulkan.Projection (infinitePerspective, orthoOffCenter)
import Geomancy.Vulkan.View (lookAt)

import qualified Apecs
import qualified Apecs.Experimental.Reactive as Apecs
import qualified Geomancy.Quaternion as Quaternion
import qualified Geomancy.Vec3 as Vec3
import qualified RIO.Vector.Storable as Storable
import qualified Vulkan.Core10 as Vk

import Game.World.Components as World

import qualified Game.World.Components.Draw as Draw

class Monad m => Run m where
  run :: SystemW a -> m a

instance Run (SystemT World IO) where
  {-# INLINE run #-}
  run = id

instance Run (RIO AppGame) where
  {-# INLINE run #-}
  run action = use gsWorld >>= liftIO . Apecs.runSystem action

instance (Run (RIO (SomeRef GameState))) where
  {-# INLINE run #-}
  run action = use gsWorld >>= liftIO . Apecs.runSystem action

setup :: MonadIO m => m World
setup = liftIO initWorld

{-# INLINE globalGet #-}
globalGet :: (Run m, Apecs.Get World IO a) => m a
globalGet = run $ Apecs.get Apecs.global

{-# INLINE globalSet #-}
globalSet :: (Run m, Apecs.Set World IO a) => a -> m ()
globalSet = run . Apecs.set Apecs.global

{-# INLINE globalModify #-}
globalModify
  :: ( Apecs.Get World IO a
     , Apecs.Set World IO a
     , Run m
     )
  => (a -> a)
  -> m ()
globalModify = run . Apecs.modify Apecs.global

{-# INLINE getGroup #-}
getGroup :: (Run m) => Group -> m [Entity]
getGroup = run . Apecs.withReactive . Apecs.enumLookup

{-# INLINE getAll #-}
getAll ::
  ( Apecs.Members World IO a
  , Apecs.Get World IO a
  , Run m
  ) => m [a]
getAll = run $ Apecs.cfold (flip (:)) mempty

{-# INLINE wipeAll #-}
wipeAll
  :: forall components m .
      ( Apecs.Members World IO components
      , Apecs.Get     World IO components
      , Apecs.Destroy World IO components
      , Run m
      )
  => m ()
wipeAll = run $ Apecs.cmap \(_ :: components) -> Apecs.Not @components

-- * Misc helpers

sceneViewUi :: RIO AppGame SceneView
sceneViewUi = do
  Vk.Extent2D{width, height} <- view (appContext . gcVulkan . vcExtent)
  World.Time{..} <- globalGet

  let proj = orthoOffCenter 0 1 width height

  pure SceneView
    { _svView        = mempty
    , _svProjection  = proj
    , _svViewPosTime = vec4 0 0 0 _timeAbsolute
    , _svDirection   = vec4 0 0 (1) 0
    , _svNumLights   = 0
    , _svEnvCube     = 0
    }

sceneView3d :: RIO AppGame SceneView
sceneView3d = do
  Vk.Extent2D{width, height} <- view (appContext . gcVulkan . vcExtent)
  (World.Camera{..}, World.Time{..}, Draw.Env{..}) <- globalGet

  -- TODO: add 3rd person static camera toggle
  -- let
  --   _cameraOrigin = vec3 9 (-7) (-5)
  --   _cameraTarget = vec3 0 0 0
  --   _cameraFov    = 45
  --   _cameraUp     = vec3 0 (-1) 0

  let
    viewDirection = Vec3.normalize (_cameraTarget - _cameraOrigin)
    viewPosition  = _cameraOrigin -- + viewDirection Vec3.^* 0.5

  let
    fovRads = _cameraFov / 180 * pi
    projection = infinitePerspective fovRads width height
    view_ = lookAt viewPosition _cameraTarget _cameraUp

    viewPosTime =
      withVec3 viewPosition \x y z ->
        vec4 x y z _timeAbsolute

    direction4 =
      withVec3 viewDirection \x y z ->
        vec4 x y z 0

  pure SceneView
    { _svProjection  = projection
    , _svView        = view_
    , _svViewPosTime = viewPosTime
    , _svDirection   = direction4
    , _svNumLights   = 0
    , _svEnvCube     = _envCube
    }

sceneLights3d :: RIO AppGame SceneLights
sceneLights3d = do
  lights <- getAll

  Vk.Extent2D{width, height} <- view (appContext . gcVulkan . vcShadowExtent)

  pure . SceneLights $ Storable.fromList do
    (Draw.Light{..}, World.Position origin, World.Direction dir, mshadow) <- lights

    let dir3 = Quaternion.rotate dir (vec3 0 0 1)

    -- TODO: move to component
    -- XXX: copypasta from Game.Draw.Shadow
    let
      target = origin + Quaternion.rotate dir (vec3 0 0 1)

      V2 _inner outerCos  = _lightCutoff
      fovRads = 2 * acos outerCos
      near = 1/128
      far = 64 -- TODO: get from _lightAttenuation and use clipped perspective

      -- projection = perspective fovRads near far width height
      projection = infinitePerspective fovRads width height
      view_      = lookAt origin target (vec3 0 (-1) 0)

    pure SpotLight
      { _slVP =
          case mshadow of
            Nothing ->
              mempty
            Just _sm ->
              -- TODO: move to component
              -- BUG: why the reverse order? 🤔
              view_ <> projection
      , _slColor =
          _lightColor
      , _slPosition =
          withVec3 origin \x y z ->
            vec4 x y z _lightAttenuation
      , _slDirection =
          withVec3 dir3 \x y z ->
            vec4 x y z _lightAmbient
      , _slCutoff =
          _lightCutoff
      , _slShadowRange =
          case mshadow of
            Nothing ->
              0
            Just Draw.ShadowMapped ->
              V2 near far
      }
