module Vulkan.Buffer.Uniform where

import Import

import Data.Acquire (Acquire, mkAcquire)
import Data.Maybe (fromJust)
-- import GHC.Stack (SrcLoc(..), callStack, getCallStack)

import qualified Foreign
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA
-- import qualified Vulkan.Utils.Debug as Debug

acquire
  :: (HasCallStack, Storable a)
  => VulkanDevice -> Maybe a -> Acquire (Allocated Vk.Buffer a)
acquire vd@VulkanDevice{..} initial = do
  (buf, allocation, info) <- VMA.withBuffer _vdAllocator uniformCI uniformAI mkAcquire
  -- case getCallStack callStack of
  --   [] ->
  --     pure ()
  --   (_acquire, SrcLoc{..}) : _rest ->
  --     Debug.nameObject _vdLogical buf $
  --       Text.encodeUtf8 . Text.pack $
  --         srcLocFile <> ":" <> show srcLocStartLine

  let
    allocated = Allocated
      { _allocated      = buf
      , _allocation     = allocation
      , _allocationInfo = info
      }

  case initial of
    Nothing ->
      pure ()
    Just value ->
      liftIO $ update vd allocated value

  pure allocated

  where
    uniformCI = zero
      { Vk.size        = fromIntegral $ Foreign.sizeOf (fromJust initial)
      , Vk.usage       = Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    uniformAI = zero
      { VMA.flags = VMA.ALLOCATION_CREATE_MAPPED_BIT
      , VMA.usage = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags =
          Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT .|.
          Vk.MEMORY_PROPERTY_HOST_COHERENT_BIT
      }

-- TODO: wrap mapped memory in Acquire
update :: (Storable a, MonadUnliftIO m) => VulkanDevice -> Allocated buf a -> a -> m ()
update VulkanDevice{..} Allocated{..} value =
  liftIO $ Foreign.poke (Foreign.castPtr $ VMA.mappedData _allocationInfo) value
