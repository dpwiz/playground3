{-# LANGUAGE OverloadedLists #-}

module Vulkan.Buffer.Image.CubeMap where

import Import

import Data.Acquire (Acquire, mkAcquire)
import RIO.FilePath ((</>), (<.>))

import qualified Foreign
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Buffer.Command (oneshot)

import qualified Resource.Image as Image
import qualified Vulkan.Buffer.Image as Image

pattern IMAGE_FORMAT :: Vk.Format
pattern IMAGE_FORMAT = Vk.FORMAT_R8G8B8A8_SRGB

type CubePaths = CubeSides FilePath

sides :: FilePath -> String -> CubeSides FilePath
sides path ext = CubeSides
  { _cubeFront  = path </> "front"  <.> ext
  , _cubeBack   = path </> "back"   <.> ext
  , _cubeLeft   = path </> "left"   <.> ext
  , _cubeRight  = path </> "right"  <.> ext
  , _cubeTop    = path </> "top"    <.> ext
  , _cubeBottom = path </> "bottom" <.> ext
  }

-- * Image view

acquireImageView
  :: VulkanDevice
  -> Vk.Image
  -> "mipLevels" ::: Word32
  -> Acquire Vk.ImageView
acquireImageView VulkanDevice{..} image mipLevels =
  Vk.withImageView _vdLogical imageViewCI Nothing mkAcquire
  where
    imageViewCI = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_CUBE
      , Vk.format           = IMAGE_FORMAT
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = Vk.ImageSubresourceRange
      { aspectMask  = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = 0
      , levelCount     = mipLevels -- XXX: including base
      , baseArrayLayer = 0
      , layerCount     = 6
      }

-- * Image buffer

type AllocatedImage = (Vk.Image, "mipLevels" ::: Word32, VMA.Allocation)

acquireImage :: VulkanDevice -> CubeSides FilePath -> Acquire (Vk.Image, "mipLevels" ::: Word32)
acquireImage vd cube = do
  (image, mipLevels, _alloc) <- mkAcquire (createImage vd cube) (destroyImage vd)
  pure (image, mipLevels)

createImage :: VulkanDevice -> CubeSides FilePath -> IO AllocatedImage
createImage vd@VulkanDevice{_vdAllocator} cube = do
  V2 sideWidth sideHeight <- fmap fromIntegral <$> Image.loadDimensions (_cubeFront cube)
  let extent = Vk.Extent3D sideWidth sideHeight 1
  let pixelBytes = fromIntegral $ sideWidth * sideHeight * 4
  let mipLevels = 1 -- log2MipLevels extent

  (image, allocation, _info) <- VMA.createImage
    _vdAllocator
    (imageCI extent mipLevels)
    imageAllocationCI

  Image.transitionImageLayout
    vd
    image
    mipLevels
    6
    IMAGE_FORMAT
    Vk.IMAGE_LAYOUT_UNDEFINED
    Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL

  VMA.withBuffer _vdAllocator (stageBufferCI pixelBytes) stageAllocationCI bracket \(staging, stage, stageInfo) -> do
    for_ (zip [0..5] $ toList cube) \(ix, path) ->
      Image.loadWith path \width height srcPixels -> do
        unless (width == sideWidth && height == sideHeight) $
          fail $ unwords
            [ "Cube side dimensions don't match front at", path
            , show (sideWidth, sideHeight)
            , show (width, height)
            ]

        let dstBuffer = VMA.mappedData stageInfo `Foreign.plusPtr` (pixelBytes * ix)
        liftIO $ Foreign.copyBytes dstBuffer srcPixels pixelBytes

    VMA.flushAllocation _vdAllocator stage 0 Vk.WHOLE_SIZE

    Image.copyBufferToImage vd staging image extent 6

    oneshot vd \buf ->
      cmdBarrier buf Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT (barrierBase image)
        { Vk.subresourceRange = subr
        , Vk.oldLayout        = Vk.IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        , Vk.newLayout        = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        , Vk.srcAccessMask    = Vk.ACCESS_TRANSFER_WRITE_BIT
        , Vk.dstAccessMask    = Vk.ACCESS_SHADER_READ_BIT
        }

  -- https://github.com/SaschaWillems/Vulkan/blob/master/examples/texturecubemap/texturecubemap.cpp#L255
  -- TODO: create sampler

  pure (image, mipLevels, allocation)

  where
    usage =
      Vk.IMAGE_USAGE_SAMPLED_BIT .|.      -- Sampler
      Vk.IMAGE_USAGE_TRANSFER_DST_BIT .|. -- Staging
      Vk.IMAGE_USAGE_TRANSFER_SRC_BIT     -- Mip generation

    imageCI :: Vk.Extent3D -> Word32 -> Vk.ImageCreateInfo '[]
    imageCI extent mipLevels = zero
      { Vk.flags         = Vk.IMAGE_CREATE_CUBE_COMPATIBLE_BIT
      , Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = IMAGE_FORMAT
      , Vk.extent        = extent
      , Vk.mipLevels     = mipLevels
      , Vk.arrayLayers   = 6
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = usage
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = Vk.SAMPLE_COUNT_1_BIT -- XXX: no multisampling here
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageBufferCI :: Int -> Vk.BufferCreateInfo '[]
    stageBufferCI pixelBytes = zero
      { Vk.size        = fromIntegral pixelBytes * 6
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    stageAllocationCI :: VMA.AllocationCreateInfo
    stageAllocationCI = zero
      { VMA.flags         = VMA.ALLOCATION_CREATE_MAPPED_BIT
      , VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }


    cmdBarrier buf dstStage barrier = Vk.cmdPipelineBarrier
      buf
      Vk.PIPELINE_STAGE_TRANSFER_BIT
      dstStage
      zero
      mempty
      mempty
      [SomeStruct barrier]

    barrierBase image = zero
      { Vk.srcQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.dstQueueFamilyIndex = Vk.QUEUE_FAMILY_IGNORED
      , Vk.image               = image
      }

    subr = Vk.ImageSubresourceRange
      { aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , baseMipLevel   = 0
      , baseArrayLayer = 0
      , levelCount     = 1
      , layerCount     = 6
      }

destroyImage :: VulkanDevice -> AllocatedImage -> IO ()
destroyImage vw (image, _mipLevels, allocation) = VMA.destroyImage (_vdAllocator vw) image allocation
