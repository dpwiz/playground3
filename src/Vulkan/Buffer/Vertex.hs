module Vulkan.Buffer.Vertex where

import Import

import Data.Acquire (Acquire, mkAcquire)
import RIO.List.Partial (head)

import qualified Foreign
import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

import Vulkan.Buffer.Command (oneshot)

acquireIndexed
  :: VertexAttribs items
  => VulkanDevice
  -> Indexed items
  -> Acquire
    ( "positions"  ::: Vk.Buffer
    , "vertices"   ::: Vk.Buffer
    , "indices"    ::: Vk.Buffer
    , "numIndices" ::: Word32
    )
acquireIndexed vd Indexed{..} = (,,,)
  <$> acquire vd Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT (vertexPositions iItems)
  <*> acquire vd Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT (vertexAttribs iItems)
  <*> acquire vd Vk.BUFFER_USAGE_INDEX_BUFFER_BIT iIndices
  <*> pure (fromIntegral $ length iIndices)

acquire :: Storable a => VulkanDevice -> Vk.BufferUsageFlagBits -> [a] -> Acquire Vk.Buffer
acquire vd@VulkanDevice{..} usage values = do
  (final, _allocation, _info) <- VMA.withBuffer _vdAllocator bufferCI bufferAI mkAcquire

  liftIO $ VMA.withBuffer _vdAllocator stageCI stageAI bracket \(staging, stage, _stageInfo) -> do
    VMA.withMappedMemory _vdAllocator stage bracket \ptr -> do
      liftIO $ Foreign.pokeArray (Foreign.castPtr ptr) values
      VMA.flushAllocation _vdAllocator stage 0 Vk.WHOLE_SIZE
    copyBuffer vd staging final itemsSize

  pure final
  where
    itemsSize = fromIntegral $ Foreign.sizeOf (head values) * length values

    bufferCI = zero
      { Vk.size        = itemsSize
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_DST_BIT .|. usage
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    bufferAI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    stageCI = zero
      { Vk.size        = itemsSize
      , Vk.usage       = Vk.BUFFER_USAGE_TRANSFER_SRC_BIT
      , Vk.sharingMode = Vk.SHARING_MODE_EXCLUSIVE
      }

    stageAI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_CPU_TO_GPU
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_HOST_VISIBLE_BIT
      }

copyBuffer
  :: MonadUnliftIO m
  => VulkanDevice
  -> ("src" ::: Vk.Buffer)
  -> ("dst" ::: Vk.Buffer)
  -> Vk.DeviceSize
  -> m ()
copyBuffer vd src dst size =
  oneshot vd \buf ->
    Vk.cmdCopyBuffer buf src dst $
      Vector.singleton (Vk.BufferCopy 0 0 size)
