module Vulkan.Buffer.Command where

import Import

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

-- | Scratch command buffer for transfer operations.
-- The simple fence makes it unusable for rendering.
oneshot :: MonadUnliftIO m => VulkanDevice -> (Vk.CommandBuffer -> m ()) -> m ()
oneshot VulkanDevice{..} action = do
  Vk.withCommandBuffers _vdLogical commandBufferAllocateInfo bracket $ \case
    (toList -> [buf]) -> do
      let
        oneTime :: Vk.CommandBufferBeginInfo '[]
        oneTime = zero
          { Vk.flags = Vk.COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
          }

      Vk.useCommandBuffer buf oneTime $ action buf

      Vk.withFence _vdLogical zero Nothing bracket $ \fence -> do
        Vk.queueSubmit _vdGraphicsQ (x buf) fence
        Vk.waitForFences _vdLogical (pure fence) True maxBound >>= \case
          Vk.SUCCESS ->
            pure ()
          err ->
            error $ "copyBuffer failed: " <> show err
    _ ->
      error "assert: exactly the requested buffer was given"
  where
    commandBufferAllocateInfo :: Vk.CommandBufferAllocateInfo
    commandBufferAllocateInfo = zero
      { Vk.commandPool        = _vdCommandPool
      , Vk.level              = Vk.COMMAND_BUFFER_LEVEL_PRIMARY
      , Vk.commandBufferCount = 1
      }

    x buf = Vector.singleton $ SomeStruct zero
      { Vk.commandBuffers =
          Vector.singleton (Vk.commandBufferHandle buf)
      }
