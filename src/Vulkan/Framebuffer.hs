module Vulkan.Framebuffer where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk
import qualified VulkanMemoryAllocator as VMA

type Framebuffers = Vector Vk.Framebuffer

acquireRender
  :: VulkanDevice
  -> Vk.Extent2D
  -> Vk.RenderPass
  -> Vector Vk.ImageView
  -> Acquire Framebuffers
acquireRender vd extent renderPass swapViews = do
  colorView <- acquireColorResource vd extent
  (_depthImage, depthView) <- acquireDepthResource vd extent False

  for swapViews \colorResolve ->
    Vk.withFramebuffer
      (_vdLogical vd)
      (framebufferCI colorView depthView colorResolve)
      Nothing
      mkAcquire

  where
    Vk.Extent2D{width, height} = extent

    framebufferCI colorView depthView colorResolve = zero
      { Vk.renderPass  = renderPass
      , Vk.width       = width
      , Vk.height      = height
      , Vk.layers      = 1
      , Vk.attachments = Vector.fromList [ colorView, depthView, colorResolve ]
      }

acquireShadow
  :: VulkanDevice
  -> Vk.Extent2D
  -> Vk.RenderPass
  -> Acquire (Vk.Image, Vk.ImageView, Vk.Framebuffer)
acquireShadow vd extent shadowPass = do
  (depthImage, depthView) <- acquireDepthResource vd extent True

  fb <- Vk.withFramebuffer
    (_vdLogical vd)
    (framebufferCI depthView)
    Nothing
    mkAcquire
  pure (depthImage, depthView, fb)

  where
    Vk.Extent2D{width, height} = extent

    framebufferCI depthView = zero
      { Vk.renderPass  = shadowPass
      , Vk.width       = width
      , Vk.height      = height
      , Vk.layers      = 1
      , Vk.attachments = Vector.singleton depthView
      }

-- * Attached resources

acquireColorResource :: VulkanDevice -> Vk.Extent2D -> Acquire Vk.ImageView
acquireColorResource VulkanDevice{..} Vk.Extent2D{width, height} = do
  (image, _allocation, _info) <- VMA.withImage _vdAllocator imageCI imageAllocationCI mkAcquire
  Vk.withImageView _vdLogical (imageViewCI image) Nothing mkAcquire
  where
    PhysicalDevice{..} = _vdPhysical

    imageCI = zero
      { Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = _vdSurfaceFormat
      , Vk.extent        = Vk.Extent3D width height 1
      , Vk.mipLevels     = 1
      , Vk.arrayLayers   = 1
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = Vk.IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT .|. Vk.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = _pdMsaaSamples
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    imageViewCI image = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = _vdSurfaceFormat
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = zero
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }

acquireDepthResource :: VulkanDevice -> Vk.Extent2D -> "sampled" ::: Bool -> Acquire (Vk.Image, Vk.ImageView)
acquireDepthResource VulkanDevice{..} Vk.Extent2D{width, height} sampled = do
  (image, _allocation, _info) <- VMA.withImage _vdAllocator imageCI imageAllocationCI mkAcquire
  imageView <- Vk.withImageView _vdLogical (imageViewCI image) Nothing mkAcquire
  pure (image, imageView)
  where
    PhysicalDevice{..} = _vdPhysical

    usage =
      if sampled then
        Vk.IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT .|.
        Vk.IMAGE_USAGE_SAMPLED_BIT
      else
        Vk.IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT

    samples =
      if sampled then
        Vk.SAMPLE_COUNT_1_BIT
      else
        _pdMsaaSamples

    imageCI = zero
      { Vk.imageType     = Vk.IMAGE_TYPE_2D
      , Vk.format        = _vdDepthFormat
      , Vk.extent        = Vk.Extent3D width height 1
      , Vk.mipLevels     = 1
      , Vk.arrayLayers   = 1
      , Vk.tiling        = Vk.IMAGE_TILING_OPTIMAL
      , Vk.initialLayout = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.usage         = usage
      , Vk.sharingMode   = Vk.SHARING_MODE_EXCLUSIVE
      , Vk.samples       = samples
      }

    imageAllocationCI = zero
      { VMA.usage         = VMA.MEMORY_USAGE_GPU_ONLY
      , VMA.requiredFlags = Vk.MEMORY_PROPERTY_DEVICE_LOCAL_BIT
      }

    imageViewCI image = zero
      { Vk.image            = image
      , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
      , Vk.format           = _vdDepthFormat
      , Vk.components       = zero
      , Vk.subresourceRange = subr
      }

    subr = zero
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_DEPTH_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }
