module Vulkan.Context where

import Import

import Data.Acquire (Acquire, mkAcquire)
import SDL.Video.Vulkan (vkGetDrawableSize)

import qualified RIO.Vector as Vector
import qualified SDL
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Extensions.VK_KHR_surface as Khr
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr

import qualified Vulkan.Framebuffer as Framebuffer
import qualified Vulkan.RenderPass as RenderPass
import qualified Vulkan.RenderPass.Shadow as ShadowPass

pattern MAX_SETS :: (Eq a, Num a) => a
pattern MAX_SETS = 20

pattern MAX_SHADER_UBOS :: (Eq a, Num a) => a
pattern MAX_SHADER_UBOS = 2

pattern MAX_SHADER_SAMPLERS :: (Eq a, Num a) => a
pattern MAX_SHADER_SAMPLERS = 1

pattern MAX_MODELS :: (Eq a, Num a) => a
pattern MAX_MODELS = 10

acquire :: SDL.Window -> VulkanDevice -> Acquire VulkanContext
acquire window vd = do
  V2 w h <- vkGetDrawableSize window
  let extent = Vk.Extent2D (fromIntegral w) (fromIntegral h)

  (swapChain, swapViews, swapLength) <- acquireSwapchain vd extent

  let shadowExtent = Vk.Extent2D 2048 2048
  shadowPass <- ShadowPass.acquire vd
  shadowBuffer <- Framebuffer.acquireShadow vd shadowExtent shadowPass

  renderPass <- RenderPass.acquire vd
  framebuffers <- Framebuffer.acquireRender vd extent renderPass swapViews

  descPool <- Vk.withDescriptorPool (_vdLogical vd) (descPoolCI swapLength) Nothing mkAcquire

  pure VulkanContext
    { _vcExtent         = extent
    , _vcSwapChain      = swapChain
    , _vcSwapLength     = swapLength
    , _vcSwapViews      = swapViews
    , _vcRenderPass     = renderPass
    , _vcShadowPass     = shadowPass
    , _vcShadowExtent   = shadowExtent
    , _vcShadowBuffer   = shadowBuffer
    , _vcFramebuffers   = framebuffers
    , _vcDescriptorPool = descPool
    }

acquireSwapchain :: VulkanDevice -> Vk.Extent2D -> Acquire (Khr.SwapchainKHR, Vector Vk.ImageView, Int)
acquireSwapchain vd@VulkanDevice{..} extent = do
  swapChain <- Khr.withSwapchainKHR _vdLogical (swapchainCI vd extent) Nothing mkAcquire

  (_res, images) <- Khr.getSwapchainImagesKHR _vdLogical swapChain
  swapViews <- for images \image ->
    Vk.withImageView _vdLogical (imageViewCI image _vdSurfaceFormat) Nothing mkAcquire

  pure
    ( swapChain
    , swapViews
    , Vector.length swapViews
    )

swapchainCI :: VulkanDevice -> Vk.Extent2D -> Khr.SwapchainCreateInfoKHR '[]
swapchainCI VulkanDevice{..} extent = zero
  { Khr.surface            = _vdSurface
  , Khr.minImageCount      = minImageCount + 1
  , Khr.imageFormat        = _vdSurfaceFormat
  , Khr.imageColorSpace    = Khr.colorSpace _pdSurfaceFormat
  , Khr.imageExtent        = extent
  , Khr.imageArrayLayers   = 1
  , Khr.imageSharingMode   = sharingMode
  , Khr.imageUsage         = Vk.IMAGE_USAGE_COLOR_ATTACHMENT_BIT
  , Khr.queueFamilyIndices = Vector.fromList queueFamilyIndices
  , Khr.preTransform       = Khr.currentTransform _pdSurfaceCaps
  , Khr.compositeAlpha     = Khr.COMPOSITE_ALPHA_OPAQUE_BIT_KHR
  , Khr.presentMode        = _pdPresentMode
  , Khr.clipped            = True
  }
  where
    PhysicalDevice{..} = _vdPhysical

    minImageCount =
      Khr.minImageCount (_pdSurfaceCaps :: Khr.SurfaceCapabilitiesKHR)

    (sharingMode, queueFamilyIndices) =
      if _vdGraphicsQ == _vdPresentQ then
        ( Vk.SHARING_MODE_EXCLUSIVE
        , []
        )
      else
        ( Vk.SHARING_MODE_CONCURRENT
        , [_pdGraphicsQueueIx, _pdPresentQueueIx]
        )

imageViewCI :: Vk.Image -> Vk.Format -> Vk.ImageViewCreateInfo '[]
imageViewCI image format = zero
  { Vk.image            = image
  , Vk.viewType         = Vk.IMAGE_VIEW_TYPE_2D
  , Vk.format           = format
  , Vk.components       = zero
  , Vk.subresourceRange = subr
  }
  where
    subr = Vk.ImageSubresourceRange
      { Vk.aspectMask     = Vk.IMAGE_ASPECT_COLOR_BIT
      , Vk.baseMipLevel   = 0
      , Vk.levelCount     = 1
      , Vk.baseArrayLayer = 0
      , Vk.layerCount     = 1
      }

descPoolCI :: Int -> Vk.DescriptorPoolCreateInfo '[]
descPoolCI swapLength = zero
  { Vk.maxSets = MAX_SETS
  , Vk.poolSizes = Vector.fromList
      [ Vk.DescriptorPoolSize
          Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
          (fromIntegral swapLength * MAX_SHADER_UBOS * MAX_MODELS)
      , Vk.DescriptorPoolSize
          Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
          (fromIntegral swapLength * MAX_SHADER_UBOS * MAX_MODELS)
      , Vk.DescriptorPoolSize
          Vk.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
          1024
      ]
  }

commandPoolCI :: VulkanDevice -> Vk.CommandPoolCreateInfo
commandPoolCI VulkanDevice{_vdPhysical}= Vk.CommandPoolCreateInfo
  { Vk.flags            = zero
  , Vk.queueFamilyIndex = _pdGraphicsQueueIx _vdPhysical
  }