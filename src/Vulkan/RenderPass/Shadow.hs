module Vulkan.RenderPass.Shadow where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

acquire :: VulkanDevice -> Acquire Vk.RenderPass
acquire VulkanDevice{..} = Vk.withRenderPass _vdLogical createInfo Nothing mkAcquire
  where
    PhysicalDevice{..} = _vdPhysical

    createInfo :: Vk.RenderPassCreateInfo '[]
    createInfo = zero
      { Vk.attachments  = Vector.fromList [depth]
      , Vk.subpasses    = Vector.fromList [subpass]
      , Vk.dependencies = Vector.fromList [pre, post]
      }

    depth = zero
      { Vk.format         = _vdDepthFormat
      , Vk.samples        = Vk.SAMPLE_COUNT_1_BIT
      , Vk.loadOp         = Vk.ATTACHMENT_LOAD_OP_CLEAR
      , Vk.storeOp        = Vk.ATTACHMENT_STORE_OP_STORE
      , Vk.stencilLoadOp  = Vk.ATTACHMENT_LOAD_OP_DONT_CARE
      , Vk.stencilStoreOp = Vk.ATTACHMENT_STORE_OP_DONT_CARE
      , Vk.initialLayout  = Vk.IMAGE_LAYOUT_UNDEFINED
      , Vk.finalLayout    = Vk.IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
      }

    subpass = zero
      { Vk.pipelineBindPoint = Vk.PIPELINE_BIND_POINT_GRAPHICS
      , Vk.depthStencilAttachment = Just zero
          { Vk.attachment = 0
          , Vk.layout     = Vk.IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
          }
      }

    pre = zero
      { Vk.srcSubpass      = Vk.SUBPASS_EXTERNAL
      , Vk.dstSubpass      = 0
      , Vk.srcStageMask    = Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT
      , Vk.dstStageMask    = Vk.PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT
      , Vk.srcAccessMask   = Vk.ACCESS_SHADER_READ_BIT
      , Vk.dstAccessMask   = Vk.ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
      , Vk.dependencyFlags = Vk.DEPENDENCY_BY_REGION_BIT
      }

    post = zero
      { Vk.srcSubpass      = 0
      , Vk.dstSubpass      = Vk.SUBPASS_EXTERNAL
      , Vk.srcStageMask    = Vk.PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT
      , Vk.dstStageMask    = Vk.PIPELINE_STAGE_FRAGMENT_SHADER_BIT
      , Vk.srcAccessMask   = Vk.ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT
      , Vk.dstAccessMask   = Vk.ACCESS_SHADER_READ_BIT
      , Vk.dependencyFlags = Vk.DEPENDENCY_BY_REGION_BIT
      }
