module Vulkan.Inflight where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

-- | Intel says:
--
--     Having two sets of frame resources is a must.
--     Increasing the number of frame resources to three
--     doesn't give us much more performance (if any at all).
--
-- https://software.intel.com/en-us/articles/practical-approach-to-vulkan-part-1
pattern INFLIGHT_FRAMES :: Int
pattern INFLIGHT_FRAMES = 2

acquire :: Vk.Device -> Word32 -> Acquire (Vector InflightData)
acquire device queue = Vector.replicateM INFLIGHT_FRAMES (acquire1 device queue)

acquire1 :: Vk.Device -> Word32 -> Acquire InflightData
acquire1 device queue = do
  _idCommandPool    <- Vk.withCommandPool device poolCI Nothing mkAcquire
  _idBusy           <- Vk.withFence device fenceCI Nothing mkAcquire
  _idImageAvailable <- Vk.withSemaphore device zero Nothing mkAcquire
  _idRenderFinished <- Vk.withSemaphore device zero Nothing mkAcquire
  pure InflightData{..}
  where
    poolCI = Vk.CommandPoolCreateInfo
      { Vk.flags            = zero
      , Vk.queueFamilyIndex = queue
      }

    fenceCI = zero
      { Vk.flags = Vk.FENCE_CREATE_SIGNALED_BIT
      } :: Vk.FenceCreateInfo '[]
