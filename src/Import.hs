module Import
  ( module RIO

  , module Game.World.Types
  , module Geometry.Types
  , module Types

  , module RE

  , (.&&.)
  , sizeOfFloats
  , tau
  , fract
  ) where

-- The prelude here
import RIO

-- Base stuff
import Data.Bits as RE (Bits(..))
import Data.List as RE (sort, sortOn)
import Foreign.C.Types as RE (CInt)
import Foreign.Ptr as RE (castPtr)
import Foreign.Storable as RE (sizeOf)
import GHC.Float as RE (double2Float, float2Double)
import Text.Printf as RE (printf)

-- Hackage deps
import Apecs as RE (Entity, Not(..), SystemT)
import Control.Lens as RE
  ( _1, _2, _3
  , use, uses, views
  , (+~), (-~), (*~), (<>~)
  , (.=), (%=), (+=), (-=), (*=), (<>=)
  , (<%=)
  )
import Geomancy as RE
  ( Quaternion
  , Vec4
  , quaternion
  , vec3
  , vec4
  , withQuaternion
  , withVec3
  , withVec4
  )
import Linear as RE (V2(..), V3(..), V4(..))

-- Generic Vulkan utilities
import Vulkan.CStruct.Extends as RE (SomeStruct(..))
import Vulkan.NamedType as RE ((:::))
import Vulkan.Zero as RE (Zero(..))

-- Project stuff
import Game.World.Types
import Geometry.Types
import Types

(.&&.) :: Bits a => a -> a -> Bool
x .&&. y = (/= zeroBits) (x .&. y)

sizeOfFloats :: Integral a => Int -> a
sizeOfFloats num = fromIntegral $ sizeOf @Float 0 * num

tau :: Floating a => a
tau = 2 * pi

fract :: Float -> Float
fract f = f - fromIntegral @Int (truncate f)
