{-# LANGUAGE QuasiQuotes #-}

module Render.SDF.Shaders
  ( vertex
  , fragment

  , vertexInput
  , vertexConstants
  , fragmentConstants
  ) where

import Import

import Geomancy (Transform)
import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

import qualified Render.Sprite.Shaders as Sprite

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=0) mat4 model;
      // TODO: modelInv;
    } obj;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
    } scene;

    // TODO: instance uniforms

    layout(location = 0) in vec2 inPosition;
    layout(location = 1) in vec2 inTexCoord;

    layout(location = 0) out vec2 fragTexCoord;

    void main() {
      gl_Position = scene.projection * scene.view * obj.model * vec4(inPosition, 0.0, 1.0);
      fragTexCoord = inTexCoord;
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    struct Texture {
      vec4 base;
      vec4 texOffsetScale;
      uint samplerId;
      uint textureId;
    };

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=64) Texture primary;
    } obj;

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[128];

    layout(location = 0) in vec2 fragTexCoord;

    layout(location = 0) out vec4 outColor;

    float median(float r, float g, float b)
    {
      return max(min(r, g), min(max(r, g), b));
    }

    float pxRange = 12.0;

    void main() {
      vec2 pos = obj.primary.texOffsetScale.xy + obj.primary.texOffsetScale.zw * fragTexCoord;

      vec3 texel = texture(
        sampler2D(
          textures[obj.primary.textureId],
          samplers[obj.primary.samplerId]
        ),
        pos
      ).rgb;

      float sigDist = median(texel.r, texel.g, texel.b);
      float w = fwidth(sigDist);
      float opacity = smoothstep(0.5 - w, 0.5 + w, sigDist + 1/(pxRange * 0.5) + 0.015);
      outColor = mix(vec4(0), obj.primary.base, opacity);
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.fromList
      [ Vk.VertexInputBindingDescription
          { binding   = 0
          , stride    = sum $ map formatSize Sprite.vertexPos
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 1
          , stride    = sum $ map formatSize vertexAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      ]
  , Vk.vertexAttributeDescriptions =
      attrBindings [Sprite.vertexPos, vertexAttrs]
  }

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.offset     = 0
  , Vk.size       = fromIntegral $ sizeOf @Transform mempty
  }

fragmentConstants :: Vk.PushConstantRange
fragmentConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
  , Vk.offset     = offset
  , Vk.size       = fromIntegral $ sum pushFields
  }
  where
    Vk.PushConstantRange{size=offset} = vertexConstants

    pushFields =
      [ sizeOf @Bitmap zero
      ]
