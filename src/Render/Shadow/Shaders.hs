{-# LANGUAGE QuasiQuotes #-}

module Render.Shadow.Shaders
  ( vertex

  , vertexInput
  ) where

import Import

import Vulkan.Utils.ShaderQQ (vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

import qualified Render.Simple.Shaders as Simple

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
    } scene;

    layout(location = 0) in vec3 inPosition;

    layout(location = 1) in vec4 model_l0;
    layout(location = 2) in vec4 model_l1;
    layout(location = 3) in vec4 model_l2;
    layout(location = 4) in vec4 model_l3;

    void main() {
      // XXX: reassemble mat4 from instance attributes
      mat4 model = mat4(model_l0, model_l1, model_l2, model_l3);

      gl_Position = scene.projection * scene.view * model * vec4(inPosition, 1.0);
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.fromList
      [ Vk.VertexInputBindingDescription
          { binding   = 0
          , stride    = sum $ map formatSize Simple.vertexPos
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 1
          , stride    = sum $ map formatSize Simple.instanceAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_INSTANCE
          }
      ]
  , Vk.vertexAttributeDescriptions =
      attrBindings
        [ Simple.vertexPos
        , Simple.instanceAttrs
        ]
  }
