module Render.Shadow.Context
  ( acquire
  , Context.setScene
  ) where

import Import

import Data.Acquire (Acquire)

import qualified Render.Setup.Acquire as Acquire
import qualified Render.Setup.Context as Context
import qualified Render.Shadow.Shaders as Shadow

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Acquire (PipelineContext, RenderContext)
acquire vd vc = do
  layouts <- Acquire.layouts vd (Context.layoutBindings $ _vdSamplers vd)

  pc <- Acquire.pipeline
    vd vc
    layouts
    Nothing
    Shadow.vertex Nothing
    Shadow.vertexInput
    []

  rc <- Context.acquire vd vc layouts $ const [] -- XXX: shadows are vertex-only

  pure (pc, rc)
