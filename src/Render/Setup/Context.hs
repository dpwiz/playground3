module Render.Setup.Context where

import Import

import Data.Acquire (Acquire, mkAcquire)
import Geomancy.Transform (Transform)

import qualified Foreign
import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector ((!))
import qualified Vulkan.Buffer.Uniform as Uniform
import qualified Vulkan.Core10 as Vk

import qualified Render.Setup.Acquire as Acquire

pattern MAX_TEXTURES :: (Eq a, Num a) => a
pattern MAX_TEXTURES = 128

pattern MAX_CUBEMAPS :: (Eq a, Num a) => a
pattern MAX_CUBEMAPS = 10

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.DescriptorSetLayout
  -> (Vk.DescriptorSet -> [SomeStruct Vk.WriteDescriptorSet])
  -> Acquire RenderContext
acquire vd@VulkanDevice{..} vc layout writers = do
  scene  <- Uniform.acquire vd (Just zero)
  lights <- Uniform.acquire vd (Just zero)

  descSets <- Acquire.descriptorSets vd vc layout

  set0 <- case descSets Vector.!? 0 of
    Nothing ->
      error "Empty descSets"
    Just s ->
      pure s

  let
    writes = Vector.fromList $
      [ sceneWriter set0 0 scene
      , lightsWriter set0 4 lights
      ] ++ writers set0
  Vk.updateDescriptorSets _vdLogical writes mempty

  pure RenderContext
    { _rcDescSets = descSets
    , _rcScene    = scene
    , _rcLights   = lights
    }

-- | Common context setup
acquire_
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.DescriptorSetLayout
  -> "textures" ::: Vector Vk.ImageView
  -> "cubemaps" ::: Vector Vk.ImageView
  -> Acquire RenderContext
acquire_ vd vc layout textures cubemaps = do
  shadowSampler <- Vk.withSampler (_vdLogical vd) shadowCI Nothing mkAcquire
  acquire vd vc layout \set0 -> catMaybes
    [ Just $ texturesWriter set0 2 textures MAX_TEXTURES
    , if Vector.null cubemaps then
        Nothing
      else
        Just $ cubemapsWriter set0 3 cubemaps MAX_CUBEMAPS
    , Just $ shadowWriter set0 5 shadowSampler shadowView
    ]
  where
    (_image, shadowView, _fb) = _vcShadowBuffer vc

    shadowCI = zero
      { Vk.addressModeU = Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE
      , Vk.addressModeV = Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE
      , Vk.addressModeW = Vk.SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE
      }

layoutBindings :: Vector Vk.Sampler -> [[Vk.DescriptorSetLayoutBinding]]
layoutBindings samplers = [ set0 ]
  where
    set0 = [ binding0, binding1, binding2, binding3, binding4, binding5 ]
      where
        binding0 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 0
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
          , Vk.stageFlags        = Vk.SHADER_STAGE_VERTEX_BIT .|. Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorCount   = 1
          , Vk.immutableSamplers = mempty
          }

        binding1 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 1
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLER
          , Vk.descriptorCount   = fromIntegral $ Vector.length samplers
          , Vk.immutableSamplers = samplers
          }

        binding2 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 2
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
          , Vk.descriptorCount   = MAX_TEXTURES
          , Vk.immutableSamplers = mempty
          }

        binding3 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 3
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
          , Vk.descriptorCount   = MAX_CUBEMAPS
          , Vk.immutableSamplers = mempty
          }

        binding4 = Vk.DescriptorSetLayoutBinding
          { Vk.binding           = 4
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER -- TODO: _DYNAMIC
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorCount   = 1 -- XXX: one block to hold whatever number of arrays
          , Vk.immutableSamplers = mempty
          }

        binding5 = Vk.DescriptorSetLayoutBinding
          { Vk.binding = 5
          , Vk.descriptorType    = Vk.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
          , Vk.stageFlags        = Vk.SHADER_STAGE_FRAGMENT_BIT
          , Vk.descriptorCount   = 1
          , Vk.immutableSamplers = mempty -- TODO: static shadow sampler?
          }

setScene
  :: (MonadReader (App context state) m, MonadUnliftIO m)
  => SceneView -> SceneLights -> RenderContext -> m ()
setScene sv lights rc = do
  vd <- view appVulkanDevice
  Uniform.update vd (_rcScene rc) sv
  Uniform.update vd (_rcLights rc) lights

sceneWriter :: Vk.DescriptorSet -> Word32 -> Allocated Vk.Buffer SceneView -> SomeStruct Vk.WriteDescriptorSet
sceneWriter descSet binding ubo = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER
  , Vk.descriptorCount = 1
  , Vk.bufferInfo      = Vector.singleton bi
  }
  where
    bi = Vk.DescriptorBufferInfo
      { Vk.buffer = _allocated ubo
      , Vk.offset = 0
      , Vk.range  = Vk.WHOLE_SIZE
      }

samplersWriter :: Vk.DescriptorSet -> Word32 -> Vector Vk.Sampler -> SomeStruct Vk.WriteDescriptorSet
samplersWriter descSet binding samplers = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLER
  , Vk.descriptorCount = fromIntegral $ Vector.length samplers
  , Vk.imageInfo       = samplersInfo
  }
  where
    samplersInfo = do
      sampler <- samplers
      pure Vk.DescriptorImageInfo
        { sampler     = sampler
        , imageView   = zero
        , imageLayout = zero
        }

texturesWriter :: Vk.DescriptorSet -> Word32 -> Vector Vk.ImageView -> Int -> SomeStruct Vk.WriteDescriptorSet
texturesWriter descSet binding textures maxTextures = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
  , Vk.descriptorCount = fromIntegral $ Vector.length textureInfos
  , Vk.imageInfo       = textureInfos
  }
  where
    textureInfos = do
      texture <- textures <> fillers
      pure Vk.DescriptorImageInfo
        { sampler     = zero
        , imageView   = texture
        , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        }

    fillers =
      Vector.replicate
        (maxTextures - Vector.length textures)
        (textures Vector.! 0)

cubemapsWriter :: Vk.DescriptorSet -> Word32 -> Vector Vk.ImageView -> Int -> SomeStruct Vk.WriteDescriptorSet
cubemapsWriter descSet binding textures maxTextures = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_SAMPLED_IMAGE
  , Vk.descriptorCount = fromIntegral $ Vector.length textureInfos
  , Vk.imageInfo       = textureInfos
  }
  where
    textureInfos = do
      texture <- textures <> fillers
      pure Vk.DescriptorImageInfo
        { sampler     = zero
        , imageView   = texture
        , imageLayout = Vk.IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        }

    fillers =
      Vector.replicate
        (maxTextures - Vector.length textures)
        (textures Vector.! 0)

lightsWriter :: Vk.DescriptorSet -> Word32 -> Allocated Vk.Buffer SceneLights -> SomeStruct Vk.WriteDescriptorSet
lightsWriter descSet binding ubo = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_UNIFORM_BUFFER -- TODO: _DYNAMIC?
  , Vk.descriptorCount = 1 -- MAX_LIGHTS
  , Vk.bufferInfo      = Vector.singleton updateRange
  }
  where
    updateRange = Vk.DescriptorBufferInfo
      { Vk.buffer = _allocated ubo
      , Vk.offset = 0
      , Vk.range  = fromIntegral $ Foreign.sizeOf (undefined :: SceneLights)
      }

shadowWriter :: Vk.DescriptorSet -> Word32 -> Vk.Sampler -> Vk.ImageView -> SomeStruct Vk.WriteDescriptorSet
shadowWriter descSet binding sampler imageView = SomeStruct zero
  { Vk.dstSet          = descSet
  , Vk.dstBinding      = binding
  , Vk.dstArrayElement = 0
  , Vk.descriptorType  = Vk.DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER
  , Vk.descriptorCount = 1
  , Vk.imageInfo       = Vector.singleton sampledImageInfo
  }
  where
    sampledImageInfo = Vk.DescriptorImageInfo
      { sampler     = sampler
      , imageView   = imageView
      , imageLayout = Vk.IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL
      }

pushVertex :: MonadIO m => Vk.CommandBuffer -> Vk.PipelineLayout -> Transform -> m ()
pushVertex commandBuffer layout model =
  liftIO $ Foreign.with model \ptr ->
    Vk.cmdPushConstants
      commandBuffer
      layout
      Vk.SHADER_STAGE_VERTEX_BIT
      0
      (fromIntegral $ Foreign.sizeOf model)
      (Foreign.castPtr ptr)
  where
    -- TODO: modelInv
