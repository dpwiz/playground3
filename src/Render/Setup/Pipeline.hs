module Render.Setup.Pipeline
  ( acquire
  , Config(..)
  , PipelineError(..)

  , attrBindings
  , formatSize
  ) where

import Import

import Data.Acquire (Acquire, mkAcquire)

import qualified RIO.List as List
import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

data PipelineError = PipelineError Text
  deriving (Eq, Ord, Show)

instance Exception PipelineError

data Config = Config
  { depthTest  :: Bool
  , depthWrite :: Bool
  , blend      :: Bool
  , cull       :: Vk.CullModeFlagBits
  } deriving (Eq, Ord, Show)

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Maybe Config
  -> ShaderStages
  -> Vk.PipelineLayout
  -> SomeStruct Vk.PipelineVertexInputStateCreateInfo
  -> Acquire Vk.Pipeline
acquire VulkanDevice{..} VulkanContext{..} config ShaderStages{..} layout vertexInput = do
  (_result, pipelines) <- Vk.withGraphicsPipelines
    _vdLogical
    zero
    (Vector.singleton pipelineCI)
    Nothing
    mkAcquire

  case toList pipelines of
    [one] ->
      pure one
    [] ->
      throwIO $ PipelineError "No pipelines were created"
    _tooMany ->
      throwIO $ PipelineError "Too many pipelines were created"

  where
    PhysicalDevice{..} = _vdPhysical

    (Config{..}, msaa, extent, rasterizationState) = case config of
      Just conf ->
        (conf, _pdMsaaSamples, _vcExtent, rasterizationBase)
      Nothing ->
        ( Config
            { depthTest  = True
            , depthWrite = True
            , blend      = False
            , cull       = Vk.CULL_MODE_NONE
            }
        , Vk.SAMPLE_COUNT_1_BIT
        , _vcShadowExtent
        , rasterizationBase
            { Vk.depthBiasEnable         = True
            , Vk.depthBiasConstantFactor = 0.5
            , Vk.depthBiasSlopeFactor    = 1.5
            }
        )

    renderPass =
      case config of
        Nothing      -> _vcShadowPass
        Just _render -> _vcRenderPass

    pipelineCI = SomeStruct zero
      { Vk.stages             = Vector.fromList stages
      , Vk.vertexInputState   = Just $ vertexInput
      , Vk.inputAssemblyState = Just inputAsembly
      , Vk.viewportState      = Just $ SomeStruct viewportState
      , Vk.rasterizationState = SomeStruct rasterizationState
      , Vk.multisampleState   = Just $ SomeStruct multisampleState
      , Vk.depthStencilState  = Just depthStencilState
      , Vk.colorBlendState    = Just $ SomeStruct colorBlendState
      , Vk.dynamicState       = Nothing -- TODO: how to do this??
      , Vk.layout             = layout
      , Vk.renderPass         = renderPass
      , Vk.subpass            = 0
      , Vk.basePipelineHandle = zero
      }

    stages = mconcat
      [ [ SomeStruct zero
            { Vk.stage = Vk.SHADER_STAGE_VERTEX_BIT
            , Vk.name     = "main"
            , Vk.module'  = _ssVertex
            }
        ]
      , case _ssFragment of
          Nothing ->
            mzero
          Just spirv ->
            [ SomeStruct zero
                { Vk.stage = Vk.SHADER_STAGE_FRAGMENT_BIT
                , Vk.name     = "main"
                , Vk.module'  = spirv
                }
            ]
      ]

    inputAsembly = zero
      { Vk.topology               = Vk.PRIMITIVE_TOPOLOGY_TRIANGLE_LIST
      , Vk.primitiveRestartEnable = False
      }

    viewportState = zero
      { Vk.viewports = Vector.singleton Vk.Viewport
          { Vk.x        = 0
          , Vk.y        = 0
          , Vk.width    = realToFrac width
          , Vk.height   = realToFrac height
          , Vk.minDepth = 0
          , Vk.maxDepth = 1
          }
      , Vk.scissors = Vector.singleton Vk.Rect2D
          { Vk.offset = Vk.Offset2D 0 0
          , Vk.extent = extent
          }
      }
      where
        Vk.Extent2D{width, height} = extent

    rasterizationBase = zero
      { Vk.lineWidth               = 1
      , Vk.polygonMode             = Vk.POLYGON_MODE_FILL
      , Vk.cullMode                = cull
      , Vk.frontFace               = Vk.FRONT_FACE_COUNTER_CLOCKWISE
      }

    multisampleState = zero
      { Vk.rasterizationSamples = msaa
      , Vk.sampleShadingEnable  = enable
      , Vk.minSampleShading     = if enable then 0.2 else 1.0
      , Vk.sampleMask           = Vector.singleton maxBound
      }
      where
        enable = msaa /= Vk.SAMPLE_COUNT_1_BIT

    depthStencilState = zero
      { Vk.depthTestEnable       = depthTest
      , Vk.depthWriteEnable      = depthWrite
      , Vk.depthCompareOp        = Vk.COMPARE_OP_LESS
      , Vk.depthBoundsTestEnable = False
      , Vk.minDepthBounds        = 0.0 -- Optional
      , Vk.maxDepthBounds        = 1.0 -- Optional
      , Vk.stencilTestEnable     = False
      , Vk.front                 = zero -- Optional
      , Vk.back                  = zero -- Optional
      }

    colorBlendState = zero
      { Vk.logicOpEnable =
          False
      , Vk.attachments = Vector.singleton zero
          { Vk.blendEnable         = blend
          , Vk.srcColorBlendFactor = Vk.BLEND_FACTOR_ONE
          , Vk.dstColorBlendFactor = Vk.BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
          , Vk.colorBlendOp        = Vk.BLEND_OP_ADD
          , Vk.srcAlphaBlendFactor = Vk.BLEND_FACTOR_ONE
          , Vk.dstAlphaBlendFactor = Vk.BLEND_FACTOR_ONE_MINUS_SRC_ALPHA
          , Vk.alphaBlendOp        = Vk.BLEND_OP_ADD
          , Vk.colorWriteMask      = colorRgba
          }
      }

    colorRgba =
      Vk.COLOR_COMPONENT_R_BIT .|.
      Vk.COLOR_COMPONENT_G_BIT .|.
      Vk.COLOR_COMPONENT_B_BIT .|.
      Vk.COLOR_COMPONENT_A_BIT

-- * Utils

attrBindings :: [[Vk.Format]] -> Vector Vk.VertexInputAttributeDescription
attrBindings bindings = mconcat $ List.unfoldr shiftLocations (0, 0, bindings)
  where
    shiftLocations = \case
      (_binding, _lastLoc, [])           -> Nothing
      (binding, lastLoc, formats : rest) -> Just (bound, next)
        where
          bound = Vector.fromList do
            (ix, format) <- zip [0..] formats
            let offset = sum . map formatSize $ take ix formats
            pure zero
              { Vk.binding  = binding
              , Vk.location = fromIntegral $ lastLoc + ix
              , Vk.format   = format
              , Vk.offset   = offset
              }

          next =
            ( binding + 1
            , lastLoc + Vector.length bound
            , rest
            )

formatSize :: Integral a => Vk.Format -> a
formatSize = \case
  Vk.FORMAT_R32G32B32A32_SFLOAT -> 16
  Vk.FORMAT_R32G32B32_SFLOAT    -> 12
  Vk.FORMAT_R32G32_SFLOAT       -> 8
  Vk.FORMAT_R32_SFLOAT          -> 4
  format                        -> error $ "Format size unknown: " <> show format
