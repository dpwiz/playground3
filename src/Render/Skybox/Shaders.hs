{-# LANGUAGE QuasiQuotes #-}

module Render.Skybox.Shaders
  ( vertex
  , fragment
  ) where

import Import

import Vulkan.Utils.ShaderQQ (frag, vert)

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
      uint envCube;
    } scene;

    layout(location = 0) out vec3 fragUVW;

    mat3 invView = transpose(mat3(scene.view));
    mat4 invProj = inverse(scene.projection);
    float farZ = 1 - 1e-7;

    void main() {
      vec4 pos = vec4(0.0);
      switch(gl_VertexIndex) {
          case 0: pos = vec4(-1.0, 3.0, farZ, 1.0); break;
          case 1: pos = vec4(3.0, -1.0, farZ, 1.0); break;
          case 2: pos = vec4(-1.0, -1.0, farZ, 1.0); break;
      }

      vec4 unProjected = invProj * pos;
      unProjected.xyz *= -1;
      fragUVW = invView * unProjected.xyz;

      gl_Position = pos;
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
      uint envCube;
    } scene;

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[128]; // MAX_TEXTURES
    layout(set=0, binding=3) uniform textureCube cubes[10]; // MAX_CUBEMAPS

    layout(location = 0) in vec3 fragUVW;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = texture(
        samplerCube(
          cubes[scene.envCube],
          samplers[3]
        ),
        fragUVW
      );
    }
  |]
