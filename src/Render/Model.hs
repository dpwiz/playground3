module Render.Model where

import Import

import Data.Acquire (Acquire)
import Geomancy.Transform (Transform)

import qualified Resource.GlTF as GlTF
import qualified Resource.Object as Object

import qualified Vulkan.Buffer.Vertex as Vertex

acquireIndexed :: VertexAttribs mesh => VulkanDevice -> Indexed mesh -> Acquire Model
acquireIndexed vd mesh = do
  (positions, vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelPositions  = positions
    , _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = mempty
    }

acquireObj :: VulkanDevice -> FilePath -> Transform -> Acquire Model
acquireObj vd object transform = do
  mesh <- liftIO $ Object.load object
  (positions, vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelPositions  = positions
    , _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = transform
    }

acquireGltf :: VulkanDevice -> FilePath -> Transform -> Acquire Model
acquireGltf vd gltf transform = do
  mesh <- liftIO $ GlTF.load gltf
  (positions, vertices, indices, size) <- Vertex.acquireIndexed vd mesh

  pure Model
    { _modelPositions  = positions
    , _modelVertices   = vertices
    , _modelIndices    = indices
    , _modelIndexCount = size
    , _modelTransform  = transform
    }
