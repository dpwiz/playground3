{-# LANGUAGE TemplateHaskell #-}

module Render.Material
  ( Material(..)
  , diffuse
  , specular
  , emissive
  , occlusion
  , reflectivity
  , normals
  ) where

import RIO

import Control.Lens.TH (makeLenses)
import Vulkan.Zero (Zero(..))

import qualified Foreign

data Material a = Material
  { _diffuse      :: a
  , _specular     :: a
  , _emissive     :: a
  , _occlusion    :: a
  , _reflectivity :: a
  , _normals      :: a
  } deriving (Eq, Show, Generic, Functor, Foldable, Traversable)

instance Zero a => Zero (Material a) where
  zero = Material
    { _diffuse      = zero
    , _specular     = zero
    , _emissive     = zero
    , _occlusion    = zero
    , _reflectivity = zero
    , _normals      = zero
    }

instance (Hashable a) => Hashable (Material a)

instance (Storable a) => Storable (Material a) where
  alignment = const 16

  sizeOf ~Material{} = Foreign.sizeOf (undefined :: a) * 6

  peek ptr = Material
    <$> Foreign.peek        (Foreign.castPtr ptr)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) 8
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (8+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (8+8+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (8+8+8+8)
    <*> Foreign.peekByteOff (Foreign.castPtr ptr) (8+8+8+8+8)

  poke ptr Material{..} = do
    Foreign.poke        (Foreign.castPtr ptr)             _diffuse
    Foreign.pokeByteOff (Foreign.castPtr ptr) 8           _specular
    Foreign.pokeByteOff (Foreign.castPtr ptr) (8+8)       _emissive
    Foreign.pokeByteOff (Foreign.castPtr ptr) (8+8+8)     _occlusion
    Foreign.pokeByteOff (Foreign.castPtr ptr) (8+8+8+8)   _reflectivity
    Foreign.pokeByteOff (Foreign.castPtr ptr) (8+8+8+8+8) _normals

makeLenses ''Material
