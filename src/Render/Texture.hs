module Render.Texture where

import Import

import Data.Acquire (Acquire)
import RIO.FilePath (takeExtension)

import qualified Vulkan.Core10 as Vk

import qualified Resource.Ktx as Ktx1
import qualified Vulkan.Buffer.Image as Image

acquire :: VulkanDevice -> FilePath -> Acquire Vk.ImageView
acquire vd file = do
  traceShowM file
  case takeExtension file of
    ".ktx" -> do
      (textureImage, format, mips, faces) <- Ktx1.acquireImage vd file
      Ktx1.acquireImageView vd textureImage format mips faces

    ".ktx2" ->
      error "KTX 2 is unsupported"

    ext | ext `elem` [".jpg", ".png"] -> do
      (textureImage, mips) <- Image.acquireImage vd file
      Image.acquireImageView vd textureImage mips

    _ext ->
      error $ "Unexpected file: " <> file
