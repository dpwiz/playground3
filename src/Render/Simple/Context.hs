module Render.Simple.Context
  ( acquire
  , Context.pushVertex
  , Context.setScene

  , pushFragment
  , FragmentPush(..)
  ) where

import Import

import Data.Acquire (Acquire)

import qualified Foreign
import qualified Vulkan.Core10 as Vk

import qualified Render.Material as Render (Material)
import qualified Render.Setup.Acquire as Acquire
import qualified Render.Setup.Context as Context
import qualified Render.Setup.Pipeline as Pipeline
import qualified Render.Simple.Shaders as Simple
import qualified Render.Skybox.Shaders as Skybox

type Pipelines3d =
  ( "phong"       ::: PipelineContext
  , "phong+blend" ::: PipelineContext
  , "skybox"      ::: PipelineContext
  , "shared"      ::: RenderContext
  )

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Vector Vk.ImageView
  -> Vector Vk.ImageView
  -> Acquire Pipelines3d
acquire vd vc textures cubemaps = do
  layouts <- Acquire.layouts vd (Context.layoutBindings $ _vdSamplers vd)

  pc <- Acquire.pipeline
    vd vc
    layouts
    ( Just Pipeline.Config
        { depthTest  = True
        , depthWrite = True
        , blend      = False
        , cull       = Vk.CULL_MODE_BACK_BIT
        }
    )
    Simple.vertex (Just Simple.fragment)
    Simple.vertexInput
    [Simple.vertexConstants, Simple.fragmentConstants]

  pcBlend <- Acquire.pipeline
    vd vc
    layouts
    ( Just Pipeline.Config
        { depthTest  = True
        , depthWrite = False -- XXX: prevent "blend-with-background" artifacts when billboards overlap
        , blend      = True
        , cull       = Vk.CULL_MODE_NONE -- XXX: only billboards are getting blended for now
        }
    )
    Simple.vertex (Just Simple.fragment)
    Simple.vertexInput
    [Simple.vertexConstants, Simple.fragmentConstants]

  pcSkybox <- Acquire.pipeline
    vd vc
    layouts
    ( Just Pipeline.Config
        { depthTest  = True
        , depthWrite = True
        , blend      = False
        , cull       = Vk.CULL_MODE_BACK_BIT
        }
    )
    Skybox.vertex (Just Skybox.fragment)
    zero
    mempty

  rc <- Context.acquire_ vd vc layouts textures cubemaps

  pure (pc, pcBlend, pcSkybox, rc)

pushFragment :: MonadIO m => Vk.CommandBuffer -> Vk.PipelineLayout -> FragmentPush -> m ()
pushFragment commandBuffer layout value =
  liftIO $ Foreign.with value \ptr ->
    Vk.cmdPushConstants
      commandBuffer
      layout
      Vk.SHADER_STAGE_FRAGMENT_BIT
      off
      (fromIntegral $ sizeOf value)
      (Foreign.castPtr ptr)
  where
    Vk.PushConstantRange{size=off} = Simple.vertexConstants

newtype FragmentPush = FragmentPush (Render.Material TextureCombo)
  deriving (Eq, Storable, Zero)
