module Render.Simple.Vertex
  ( Vertex(..)
  , square
  ) where

import RIO

import Linear (V3(..), V2(..))

import Geometry.Types (HasPosition(..), Indexed(..), VertexAttribs(..))

data Vertex = Vertex
  { vPos      :: V3 Float
  , vTexCoord :: V2 Float
  , vNormal   :: V3 Float
  , vTangent  :: V3 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

instance VertexAttribs Vertex where
  vertexAttribs vertices = do
    Vertex{..} <- vertices
    let V2 u v      = vTexCoord
    let V3 nx ny nz = vNormal
    let V3 tx ty tz = vTangent
    [ u, v, nx, ny, nz, tx, ty, tz ]

instance HasPosition Vertex where
  {-# INLINE getPosition #-}
  getPosition Vertex{vPos=V3 x y z} = [x, y, z]

square :: Indexed Vertex
square = Indexed vertices indices
  where
    -- TODO: remove vertex color
    vertices =
      [ Vertex rightBottom (V2 1 1) faceNormal faceTangent
      , Vertex rightTop    (V2 1 0) faceNormal faceTangent
      , Vertex leftTop     (V2 0 0) faceNormal faceTangent
      , Vertex leftBottom  (V2 0 1) faceNormal faceTangent
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5) (-0.5) 0
    rightTop    = V3 ( 0.5) (-0.5) 0
    rightBottom = V3 ( 0.5) ( 0.5) 0
    leftBottom  = V3 (-0.5) ( 0.5) 0

    faceNormal    = V3 0 0 1
    faceTangent   = V3 1 0 0
