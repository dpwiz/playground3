{-# LANGUAGE QuasiQuotes #-}

module Render.Simple.Shaders
  ( vertex
  , fragment

  , vertexInput
  , vertexConstants
  , fragmentConstants

  , vertexPos
  , vertexAttrs
  , instanceAttrs
  ) where

import Import

import Geomancy (Transform)
import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Material (Material)
import Render.Setup.Pipeline (attrBindings, formatSize)

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    // layout(push_constant) uniform MAX_SIZE_128_CRY {
    //   mat4 model;
    // } unused_;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
    } scene;

    layout(location = 0) in vec3 inPosition;

    layout(location = 1) in vec2 inTexCoord;
    layout(location = 2) in vec3 inNormal;
    layout(location = 3) in vec3 inTangent;

    layout(location = 4) in vec4 model_l0;
    layout(location = 5) in vec4 model_l1;
    layout(location = 6) in vec4 model_l2;
    layout(location = 7) in vec4 model_l3;

    layout(location = 0) out vec4 fragPosition;
    layout(location = 1) out vec2 fragTexCoord;
    layout(location = 2) out mat3 tbn;
    // layout(location = 3) out vec3 fragNormal;

    void main() {
      // XXX: reassemble mat4 from instance attributes
      mat4 model = mat4(model_l0, model_l1, model_l2, model_l3);

      fragTexCoord = inTexCoord;
      fragPosition = model * vec4(inPosition, 1.0);

      // XXX: shouldn't be needed now, but the TBN normals are slightly off on test cubemap,
      //      leaving until the correct course is determined.
      // fragNormal = transpose(mat3(inverse(model))) * inNormal; // TODO: use modelInv

      vec3 t = normalize(vec3(model * vec4(inTangent, 0.0)));
      // vec3 n = fragNormal;
      vec3 n = normalize(vec3(model * vec4(inNormal, 0.0)));
      t = normalize(t - dot(t, n) * n); // re-orthogonalize T with respect to N
      tbn = mat3(t, cross(n, t), n);

      gl_Position = scene.projection * scene.view * fragPosition; // TODO: use scene.VP
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    // TODO: move to materials
    float matShiny = 16;

    struct TextureCombo {
      uint samplerId;
      uint textureId;
    };

    struct Light {
      mat4 VP;          // bring model positions into light-space
      vec4 color;       // a is intensity
      vec4 position;    // w is attenuation power 0/1/2
      vec4 direction;   // a is ambient spill
      vec2 cutoff;      // inner / outer
      vec2 shadowRange; // near / far
    };

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      layout(offset=64) TextureCombo diffuse;
                        TextureCombo specular;
                        TextureCombo emissive;
                        TextureCombo occlusion;
                        TextureCombo reflectivity;
                        TextureCombo normals;
    } material;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
      uint envCube;
    } scene;

    layout(set=0, binding=1) uniform sampler samplers[8];
    layout(set=0, binding=2) uniform texture2D textures[128]; // MAX_TEXTURES
    layout(set=0, binding=3) uniform textureCube cubes[10]; // MAX_CUBEMAPS

    layout(set=0, binding=4, std140) uniform Lights {
      Light lights[128]; // MAX_LIGHTS
    };

    layout(set=0, binding=5) uniform sampler2D shadowmap;

    layout(location = 0) in vec4 fragPosition;
    layout(location = 1) in vec2 fragTexCoord;
    layout(location = 2) in mat3 tbn;
    // layout(location = 3) in vec3 fragNormal;

    layout(location = 0) out vec4 outColor;

    float when_gt(float x, float y) {
      return max(sign(x - y), 0.0);
    }

    float linearizeDepth(float depth) {
      float near = 1.0;
      float far = 64.0;
      return (2.0 * near) / (far + near - depth * (far - near));
    }

    float shadow_factor(mat4 VP) {
      vec4 light_space_pos = VP * fragPosition;
      vec3 light_space_ndc = light_space_pos.xyz /= light_space_pos.w;

      if (abs(light_space_ndc.x) > 1.0 ||
          abs(light_space_ndc.y) > 1.0 ||
          abs(light_space_ndc.z) > 1.0)
            return 0.0;

      vec2 shadow_map_coord = light_space_ndc.xy * 0.5 + 0.5;
      if (light_space_ndc.z > texture(shadowmap, shadow_map_coord.xy).x)
        return 0.0;

      return 1.0;
    }

    void main() {
      vec4 diffTex = texture(
        sampler2D(
          textures[material.diffuse.textureId],
          samplers[material.diffuse.samplerId]
        ),
        fragTexCoord
      );

      vec4 specTex = texture(
        sampler2D(
          textures[material.specular.textureId],
          samplers[material.specular.samplerId]
        ),
        fragTexCoord
      );

      vec4 emissiveTex = texture(
        sampler2D(
          textures[material.emissive.textureId],
          samplers[material.emissive.samplerId]
        ),
        fragTexCoord
      );

      float occlusion = texture(
        sampler2D(
          textures[material.occlusion.textureId],
          samplers[material.occlusion.samplerId]
        ),
        fragTexCoord
      ).r;

      float reflectivity = texture(
        sampler2D(
          textures[material.reflectivity.textureId],
          samplers[material.reflectivity.samplerId]
        ),
        fragTexCoord
      ).r;

      vec3 normalsColor = texture(
        sampler2D(
          textures[material.normals.textureId],
          samplers[material.normals.samplerId]
        ),
        fragTexCoord
      ).rgb;
      // XXX: convert normal non-colors to linear values from sRGB texture colorspace
      vec3 normals = pow(normalsColor, vec3(1.0/2.2)) * 2.0 - 1.0;

      // XXX: can't do per-vertex due to interpolation artifacts
      vec3 normal = normalize(tbn * normals);
      vec3 viewDir = normalize(scene.viewPosTime.xyz - fragPosition.xyz);

      vec4 cubeTex = texture(
        samplerCube(
          cubes[scene.envCube],
          samplers[3]
        ),
        reflect(viewDir, normal)
      );

      vec3 light = emissiveTex.rgb;

      for (int i=0; i < scene.numLights; i++) {
        vec3 beam = lights[i].position.xyz - fragPosition.xyz;
        float attenuation = 1 / (1 + lights[i].position.a * dot(beam, beam));
        vec3 energy = vec3(lights[i].color.a * attenuation);

        vec3 lightDir = normalize(beam);

        // Flat ambient spill
        light += energy
          * lights[i].direction.a
          * occlusion;

        // Spot light
        float theta = dot(-lightDir, normalize(lights[i].direction.xyz));
        float epsilon = lights[i].cutoff.x - lights[i].cutoff.y;
        float spot = smoothstep(
          0.0, 1.0,
          (theta - lights[i].cutoff.y) / epsilon
        );
        energy *= spot;

        // Shadow
        if (lights[i].shadowRange.y > 0) {
          energy *= shadow_factor(lights[i].VP);
        }

        // Diffuse lighting

        float diff = max(dot(normal, lightDir), 0.0);

        light += energy
          * diff
          * diffTex.rgb;

        // Specular highlights
        float spec;

        // // Phong specs:
        // vec3 reflectDir = reflect(-lightDir, normal);
        // spec = pow(max(dot(viewDir, reflectDir), 0.0), matShiny) * attenuation;

        // Blinn specs:
        vec3 halfwayDir = normalize(lightDir + viewDir);
        spec = pow(max(dot(normal, halfwayDir), 0.0), matShiny);

        light += energy
          * when_gt(diff, 0.0f)
          * spec
          * specTex.rgb;
      }

      outColor = vec4(mix(light, cubeTex.rgb, reflectivity), diffTex.a * emissiveTex.a);
      // outColor = vec4((fragNormal - tbn[2]) * 0.5 + 0.5, 1.0);
      // outColor = vec4((tbn * vec3(0, 0, 1) - fragNormal) * 0.5 + 0.5, 1.0);
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.fromList
      [ Vk.VertexInputBindingDescription
          { binding   = 0
          , stride    = sum $ map formatSize vertexPos
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 1
          , stride    = sum $ map formatSize vertexAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 2
          , stride    = sum $ map formatSize instanceAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_INSTANCE
          }
      ]
  , Vk.vertexAttributeDescriptions =
      attrBindings [vertexPos, vertexAttrs, instanceAttrs]
  }

vertexPos :: [Vk.Format]
vertexPos =
  [ Vk.FORMAT_R32G32B32_SFLOAT
  ]

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32_SFLOAT    -- UV
  , Vk.FORMAT_R32G32B32_SFLOAT -- Normal
  , Vk.FORMAT_R32G32B32_SFLOAT -- Tangent
  ]

instanceAttrs :: [Vk.Format]
instanceAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  , Vk.FORMAT_R32G32B32A32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.size       = fromIntegral $ sizeOf @Transform mempty -- XXX: reserved
  , Vk.offset     = 0
  }

fragmentConstants :: Vk.PushConstantRange
fragmentConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_FRAGMENT_BIT
  , Vk.offset     = offset
  , Vk.size       = fromIntegral $ sum pushFields
  }
  where
    Vk.PushConstantRange{size=offset} = vertexConstants

    pushFields =
      [ sizeOf @(Material TextureCombo) zero
      ]
