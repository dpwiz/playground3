{-# LANGUAGE QuasiQuotes #-}

module Render.Solid.Shaders where

import Import

import Geomancy (Transform)
import Vulkan.Utils.ShaderQQ (frag, vert)

import qualified RIO.Vector as Vector
import qualified Vulkan.Core10 as Vk

import Render.Setup.Pipeline (attrBindings, formatSize)

import qualified Render.Simple.Shaders as Simple

vertex :: SpirV
vertex = SpirV
  [vert|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(push_constant) uniform MAX_SIZE_128_CRY {
      mat4 model;
      // TODO: modelInv;
    } obj;

    layout(set=0, binding=0) uniform PER_FRAME {
      mat4 view;
      mat4 projection;
      vec4 viewPosTime;
      vec4 viewDirection;
      uint numLights;
    } scene;

    layout(location = 0) in vec3 inPosition;
    layout(location = 1) in vec4 inColor;

    layout(location = 0) out vec4 fragColor;

    void main() {
      gl_Position = scene.projection * scene.view * obj.model * vec4(inPosition.xyz, 1.0);
      fragColor = inColor;
    }
  |]

fragment :: SpirV
fragment = SpirV
  [frag|
    #version 450
    #extension GL_ARB_separate_shader_objects : enable

    layout(location = 0) in vec4 fragColor;

    layout(location = 0) out vec4 outColor;

    void main() {
      outColor = fragColor;
    }
  |]

vertexInput :: SomeStruct Vk.PipelineVertexInputStateCreateInfo
vertexInput = SomeStruct zero
  { Vk.vertexBindingDescriptions = Vector.fromList
      [ Vk.VertexInputBindingDescription
          { binding   = 0
          , stride    = sum $ map formatSize Simple.vertexPos
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      , Vk.VertexInputBindingDescription
          { binding   = 1
          , stride    = sum $ map formatSize vertexAttrs
          , inputRate = Vk.VERTEX_INPUT_RATE_VERTEX
          }
      ]
  , Vk.vertexAttributeDescriptions =
      attrBindings [Simple.vertexPos, vertexAttrs]
  }

vertexAttrs :: [Vk.Format]
vertexAttrs =
  [ Vk.FORMAT_R32G32B32A32_SFLOAT
  ]

vertexConstants :: Vk.PushConstantRange
vertexConstants = Vk.PushConstantRange
  { Vk.stageFlags = Vk.SHADER_STAGE_VERTEX_BIT
  , Vk.size       = fromIntegral $ sizeOf @Transform mempty
  , Vk.offset     = 0
  }
