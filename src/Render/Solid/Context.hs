module Render.Solid.Context
  ( acquire
  , Context.pushVertex
  , Context.setScene
  ) where

import Import

import Data.Acquire (Acquire)

import qualified Vulkan.Core10 as Vk

import qualified Render.Setup.Acquire as Acquire
import qualified Render.Setup.Pipeline as Pipeline
import qualified Render.Setup.Context as Context
import qualified Render.Solid.Shaders as Shaders

acquire
  :: VulkanDevice
  -> VulkanContext
  -> Acquire (PipelineContext, RenderContext)
acquire vd vc = do
  layouts <- Acquire.layouts vd (Context.layoutBindings $ _vdSamplers vd)

  pc <- Acquire.pipeline
    vd vc
    layouts
    ( Just Pipeline.Config
        { depthTest = True
        , depthWrite = True
        , blend = False
        , cull = Vk.CULL_MODE_BACK_BIT
        }
    )
    Shaders.vertex (Just Shaders.fragment)
    Shaders.vertexInput
    [Shaders.vertexConstants]

  rc <- Context.acquire vd vc layouts $ const [] -- XXX: solid uses no textures/samplers

  pure (pc, rc)
