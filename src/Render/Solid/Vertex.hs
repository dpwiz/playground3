module Render.Solid.Vertex
  ( Vertex(..)
  , square
  , square2
  , rectXY
  ) where

import RIO

import Linear (V4(..), V3(..))

import Geometry.Types (HasPosition(..), Indexed(..), VertexAttribs(..))

-- | Vertex attributes for Render.Solid.Shaders
data Vertex = Vertex
  { solidPosition :: V3 Float
  , solidColor    :: V4 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

instance VertexAttribs Vertex where
  vertexAttribs vertices = do
    Vertex{..} <- vertices
    let V4 r g b a = solidColor
    [r, g, b, a]

instance HasPosition Vertex where
  {-# INLINE getPosition #-}
  getPosition Vertex{solidPosition=V3 x y z} = [x, y, z]

square :: Indexed Vertex
square = Indexed vertices indices
  where
    vertices =
      [ Vertex rightBottom (V4 0 1 0 1)
      , Vertex rightTop    (V4 0 0 1 1)
      , Vertex leftTop     (V4 1 0 0 1)
      , Vertex leftBottom  (V4 1 1 1 1)
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5) (-0.5) 0
    rightTop    = V3 ( 0.5) (-0.5) 0
    rightBottom = V3 ( 0.5) ( 0.5) 0
    leftBottom  = V3 (-0.5) ( 0.5) 0

-- | Double-sided square
square2 :: Indexed Vertex
square2 = Indexed
  { iItems   = iItems
  , iIndices = iIndices <> reverse iIndices
  }
  where
    Indexed{..} = square

-- | XY-plane rectangle from two triangles.
rectXY :: V4 Float -> Float -> Float -> (V3 Float -> V3 Float) -> Indexed Vertex
rectXY color w h tr = Indexed vertices (indices <> reverse indices)
  where
    vertices =
      [ Vertex (tr rightBottom) color
      , Vertex (tr rightTop)    color
      , Vertex (tr leftTop)     color
      , Vertex (tr leftBottom)  color
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V3 (-0.5 * w) (-0.5 * h) 0
    rightTop    = V3 ( 0.5 * w) (-0.5 * h) 0
    rightBottom = V3 ( 0.5 * w) ( 0.5 * h) 0
    leftBottom  = V3 (-0.5 * w) ( 0.5 * h) 0
