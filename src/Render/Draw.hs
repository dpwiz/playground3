module Render.Draw
  ( Draw(..)
  , drawFrame
  ) where

import Import

import Control.Monad.Trans.Resource (ReleaseKey, release)
import RIO.List.Partial (tail)

import qualified RIO.Vector as Vector
import qualified RIO.Vector.Partial as Vector
import qualified Vulkan.Core10 as Vk
import qualified Vulkan.Exception as Vk
import qualified Vulkan.Extensions.VK_KHR_swapchain as Khr

data Draw = DrawCommands [ReleaseKey] (Vk.CommandBuffer -> RIO AppGame ())

instance Semigroup Draw where
  DrawCommands ares acmd <> DrawCommands bres bcmd =
    DrawCommands (ares <> bres) \buf -> do
      acmd buf
      bcmd buf

instance Monoid Draw where
  mempty = DrawCommands mempty \_buf -> pure ()

drawFrame :: Draw -> [Draw] -> RIO AppGame ()
drawFrame shadow draws = do
  vd@VulkanDevice{..} <- view appVulkanDevice
  vc@VulkanContext{..} <- view (appContext . gcVulkan)

  inflightData@InflightData{..} <- nextInflight
  let inflight = Vector.singleton _idBusy

  Vk.waitForFences _vdLogical inflight True maxBound >>= \case
    Vk.SUCCESS ->
      pure ()
    err ->
      logError $ displayShow err

  releaseRef <- newIORef Nothing

  let nextImage = Khr.acquireNextImageKHR _vdLogical _vcSwapChain maxBound _idImageAvailable zero
  try nextImage >>= \case
    Right (Vk.SUCCESS, imageIndex) -> do
      let
        fb = _vcFramebuffers Vector.! fromIntegral imageIndex
        presentInfo = zero
          { Khr.waitSemaphores = Vector.singleton _idRenderFinished
          , Khr.swapchains     = Vector.singleton _vcSwapChain
          , Khr.imageIndices   = Vector.singleton imageIndex
          }

        cbAI = zero
          { Vk.commandPool        = _idCommandPool
          , Vk.level              = Vk.COMMAND_BUFFER_LEVEL_PRIMARY
          , Vk.commandBufferCount = 1
          }

      Vk.resetCommandPool _vdLogical _idCommandPool Vk.COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT
      bufs <- Vk.allocateCommandBuffers _vdLogical cbAI
      atomicWriteIORef releaseRef . Just $
        Vk.freeCommandBuffers _vdLogical _idCommandPool bufs

      case Vector.toList bufs of
        [cb] ->
          drawNextImage vd vc shadow draws inflightData cb fb presentInfo
        _ ->
          error "withCommandBuffers allocates a single CB"

    Right (err, _ii) ->
      logError $ displayShow err

    Left err ->
      case Vk.vulkanExceptionResult err of
        Vk.ERROR_OUT_OF_DATE_KHR ->
          gsUpdateContext .= True
        _ -> do
          logError $ displayShow err
          -- throwM err

  void $ Vk.waitForFences _vdLogical inflight True maxBound
  traverse_ release transient
  readIORef releaseRef >>= fromMaybe mempty

  where
    transient =
      (shadow : draws) >>= \case
      -- DrawScene{} ->
      --   mempty
      DrawCommands releaseKeys _commands -> do
        releaseKeys

drawNextImage
  :: VulkanDevice
  -> VulkanContext
  -> Draw
  -> [Draw]
  -> InflightData
  -> Vk.CommandBuffer
  -> Vk.Framebuffer
  -> Khr.PresentInfoKHR '[]
  -> RIO AppGame ()
drawNextImage vd vc shadow draws InflightData{..} commandBuffer renderBuffer presentInfo = do
  Vk.useCommandBuffer commandBuffer commandBufferBI do
    Vk.cmdUseRenderPass commandBuffer shadowPassBI Vk.SUBPASS_CONTENTS_INLINE $
      case shadow of
        DrawCommands _release drawCommands ->
          drawCommands commandBuffer

    Vk.cmdUseRenderPass commandBuffer renderPassBI Vk.SUBPASS_CONTENTS_INLINE $
      for_ draws \case
        -- DrawScene scene ->
        --   drawScene commandBuffer scene
        DrawCommands _release drawCommands ->
          drawCommands commandBuffer

  Vk.resetFences (_vdLogical vd) $ Vector.singleton _idBusy

  -- error "WAIT! I haven't yet done my validation checks!"
  Vk.queueSubmit (_vdGraphicsQ vd) submitInfo _idBusy

  try (Khr.queuePresentKHR (_vdPresentQ vd) presentInfo) >>= \case
    Right Vk.SUCCESS ->
      pure ()
    Right err ->
      logError $ displayShow err
    Left err ->
      case Vk.vulkanExceptionResult err of
        Vk.ERROR_OUT_OF_DATE_KHR ->
          gsUpdateContext .= True
        _ -> do
          logError $ displayShow err
  where
    commandBufferBI = zero
      { Vk.flags = Vk.COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
      } :: Vk.CommandBufferBeginInfo '[]

    (_shadowImage, _shadowView, shadowBuffer) = _vcShadowBuffer vc

    shadowPassBI = zero
      { Vk.renderPass  = _vcShadowPass vc
      , Vk.framebuffer = shadowBuffer
      , Vk.renderArea  = rect
      , Vk.clearValues = Vector.fromList clear
      }
      where
        rect = Vk.Rect2D
          { Vk.offset = zero
          , Vk.extent = _vcShadowExtent vc
          }
        clear =
          [ Vk.DepthStencil (Vk.ClearDepthStencilValue 1.0 0)
          ]

    renderPassBI = zero
      { Vk.renderPass  = _vcRenderPass vc
      , Vk.framebuffer = renderBuffer
      , Vk.renderArea  = rect
      , Vk.clearValues = Vector.fromList clear
      }
      where
        rect = Vk.Rect2D
          { Vk.offset = zero
          , Vk.extent = _vcExtent vc
          }
        clear =
          [ color
          , Vk.DepthStencil (Vk.ClearDepthStencilValue 1.0 0)
          , color
          ]
        color =
          Vk.Color (Vk.Float32 (0, 0, 0, 0))

    submitInfo = Vector.singleton $ SomeStruct zero
      { Vk.waitSemaphores   = Vector.singleton _idImageAvailable
      , Vk.waitDstStageMask = Vector.singleton Vk.PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
      , Vk.commandBuffers   = Vector.singleton (Vk.commandBufferHandle commandBuffer)
      , Vk.signalSemaphores = Vector.singleton _idRenderFinished
      }

{-# INLINE nextInflight #-}
nextInflight :: RIO AppGame InflightData
nextInflight =
  gsInflight <%= tail >>= \case
    so : _rest ->
      pure so
    _ ->
      error "gsInflight must be derived from a non-empty vector and cycled"
