module Render.Sprite.Vertex
  ( Vertex(..)
  , square
  ) where

import RIO

import Linear (V2(..))

import Geometry.Types (HasPosition(..), Indexed(..), VertexAttribs(..))

data Vertex = Vertex
  { vPos      :: V2 Float
  , vTexCoord :: V2 Float
  }
  deriving (Eq, Ord, Show, Generic)

instance Hashable Vertex

instance VertexAttribs Vertex where
  vertexAttribs vertices = do
    Vertex{..} <- vertices
    -- let V2 x y = vPos
    let V2 u v = vTexCoord
    [ u, v ]

instance HasPosition Vertex where
  {-# INLINE getPosition #-}
  getPosition Vertex{vPos=V2 x y} = [x, y]

square :: Indexed Vertex
square = Indexed vertices indices
  where
    vertices =
      [ Vertex leftTop     (V2 0 1)
      , Vertex rightTop    (V2 1 1)
      , Vertex rightBottom (V2 1 0)
      , Vertex leftBottom  (V2 0 0)
      ]

    indices =
      [ 0, 1, 2
      , 2, 3, 0
      ]

    leftTop     = V2 (-0.5) (-0.5)
    rightTop    = V2 ( 0.5) (-0.5)
    rightBottom = V2 ( 0.5) ( 0.5)
    leftBottom  = V2 (-0.5) ( 0.5)
